//
//  UserInfo.swift
//  Money_flow
//
//  Created by Vibhuti Sharma on 09/09/18.
//  Copyright © 2018 Nile Technologies. All rights reserved.
//

import UIKit
class UserInfo: NSObject {
   
    var keySkills = ""
    var keySkillsID = ""
    var language = ""
    var languageValueID = ""
    var isCheck = "0"
    //Get Job
    var activePlan = ""
    var url = ""
    var totalUsedblnce = ""
    var planType = ""
    var viewLimit = ""
    var startDate = ""
    var createdDate = ""
    var jobID  = ""
    var jobType = ""
    var jobTitle = ""
    var eID = ""
    var companyLogo = ""
    var  companyName = ""
    var sName = ""
    var cName = ""
    var myJob = 0
    
    var address = ""
    var empId = ""
    var appliedStatus = ""
    var appliedDate = ""
    var apply = 0

    var University = ""
    var startYear = ""
    var educationName = ""
    var desp = ""
    var designation = ""
    var lastDate = ""
    var profSkill = ""
     var employeeID = ""
    var countryName = ""
    var postedDate = ""
    var id = ""
    var candidate_id = ""
    var education = ""
    var courses = ""
    var educationID = ""
    var proSkill = ""
    var name = ""
    var company = ""
    var isPresent = ""
    var present = ""
    var startMonth = ""
    var lastYear = ""
    var lastMonth = ""
    var title = ""
    var price = ""
    var content = ""
    var subService = ""
    var bought = ""
    var resume_id = ""
    var status = ""
    var totalView = ""
    var usedBlnce = ""
    var leftBlnce = ""
    var leftBlnceVal = ""
    var planExp = ""
    var planStatus = ""
    var profilePic = ""
    
    var state = ""
    var city = ""
    var date = ""
    var amount = ""
    var package = ""
    
    
}
