//
//  MediaInfo.swift
//  Job Portal
//
//  Created by nile on 30/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class MediaInfo: NSObject {

    var title = ""
    var desc = ""
    var video_file = ""
    var data = ""

    class func getMediaArayList(responseArray: Array<Dictionary<String, Any>>) -> Array<MediaInfo> {
        var resultArray = [MediaInfo]()
        for item in responseArray {
            
            let modalObj = MediaInfo()
            modalObj.title = item.validatedValue("title", expected: "" as AnyObject) as! String
            modalObj.desc = item.validatedValue("desc", expected: "" as AnyObject) as! String
            modalObj.video_file = item.validatedValue("video_file", expected: "" as AnyObject) as! String
            modalObj.data = item.validatedValue("data", expected: "" as AnyObject) as! String
            resultArray.append(modalObj)
        }
        return resultArray
    }
}
