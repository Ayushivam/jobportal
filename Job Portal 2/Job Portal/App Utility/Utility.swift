

//  Utility.swift
//  Sage
//  Created by nile on 05/06/18.
//  Copyright © 2018 Nile. All rights reserved.


import Foundation
import UIKit
let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
let kWindowWidth = UIScreen.main.bounds.size.width
let kWindowHeight = UIScreen.main.bounds.size.height

func imageToNSString(image: UIImage) -> String {


    let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
    let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    return (imageStr.length != 0) ? imageStr : ""
}

extension String {
    func deletingPrefix(_ prefix: String) -> String {

        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))

    }
}

extension String{
    var length : Int {
        return self.count
    }
}

func base64Convert(base64String: String?) -> UIImage{
    if (base64String?.isEmpty)! {
        return #imageLiteral(resourceName: "no_image_found")
    }else {
        // !!! Separation part is optional, depends on your Base64String !!!
        let temp = base64String?.components(separatedBy: ",")

        let dataDecoded : Data = Data(base64Encoded: temp![0], options: .ignoreUnknownCharacters)!

        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage!
    }
}
