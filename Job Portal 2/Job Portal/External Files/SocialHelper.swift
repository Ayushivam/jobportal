//
//
//  Created by Chandan Mishra on 9/29/17.
//  Copyright © 2018 . All rights reserved.
//
//
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
class SocialHelper: NSObject {
typealias LoginCompletionBlock = (Dictionary<String, AnyObject>?, NSError?) -> Void
  
    class var facebookManager: SocialHelper {
        struct Static {
            static let instance: SocialHelper = SocialHelper()
        }
        return Static.instance
    }

    //MARK:- Public functions
        func getFacebookInfoWithCompletionHandler(fromViewController:AnyObject, onCompletion: @escaping  LoginCompletionBlock) -> Void {
        
//        if (kAppDelegate.isReachable() == false) {
//            
//            AlertViewController.alert("Connection Error!", message: "Internet connection appears to be offline. Please check your internet connection.")
//            
//            return
//        }

        self.getFBInfoWithCompletionHandler(fromViewController: fromViewController) { (dataDictionary:Dictionary<String, AnyObject>?, error: NSError?) -> Void in
            onCompletion(dataDictionary, error)
        }
    }

    func logoutFromFacebook() {
        
        FBSDKLoginManager().logOut()
        FBSDKAccessToken.setCurrent(nil)
        FBSDKProfile.setCurrent(nil)
    }

    //MARK:- Private functions
    private func getFBInfoWithCompletionHandler(fromViewController:AnyObject, onCompletion: @escaping LoginCompletionBlock) -> Void {
        let permissionDictionary = [
            "fields" : "id,name,first_name,last_name,gender,email,birthday,picture.type(large),about",
            //"locale" : "en_US"
        ]

        if FBSDKAccessToken.current() != nil {
            print(FBSDKAccessToken.current())
            let pictureRequest = FBSDKGraphRequest(graphPath: "me", parameters: permissionDictionary)
             _ = pictureRequest?.start(completionHandler: { (connection, result, error) in
                if error == nil {
                    onCompletion(result as? Dictionary<String, AnyObject>, nil)
                } else {
                    onCompletion(nil, error as NSError?)
                }
                connection?.cancel()
            })

        } else {
            FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile","user_friends"], from: fromViewController as! UIViewController, handler: { (result, error) in
                if error != nil {
                    FBSDKLoginManager().logOut()
                    let errorDetails = [NSLocalizedDescriptionKey : "Processing Error. Please try again!"]
                    let customError = NSError(domain: "Error!", code: (error?._code)!, userInfo: errorDetails)
                    onCompletion(nil, customError)
                    
                } else if (result?.isCancelled)! {
                    FBSDKLoginManager().logOut()
                    let errorDetails = [NSLocalizedDescriptionKey : "Request cancelled!"]
                    let customError = NSError(domain: "Request cancelled!", code: 404, userInfo: errorDetails)
                    
                    onCompletion(nil, customError)
                } else {
                    let pictureRequest = FBSDKGraphRequest(graphPath: "me", parameters: permissionDictionary)
                  _ = pictureRequest?.start(completionHandler: { (connection, result, error) in
                        if error == nil {
                            onCompletion(result as? Dictionary<String, AnyObject>, nil)
                        } else {
                            onCompletion(nil, error as NSError?)
                        }
                    })
                }
            })
        }
    }
}
