//  FILinkedinLoginPage.swift
//  WorldFax
//
//  Created by shivam on 08/03/17.
//  Copyright © 2017 Probir Chakraborty. All rights reserved.
//

import UIKit

protocol FILinkedinLoginDelegate: class {
    func callApiDelegateMethod(response: NSDictionary)
}

class TLinkedinLoginViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var addressBar: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    
    let client_id  = "77d3kqwyuusutu"
    let client_secret = "5r8JD2nFSBuRXVP1"
    let authorization_base_url = "https://www.linkedin.com/uas/oauth2/authorization"
    let token_url = "https://www.linkedin.com/uas/oauth2/accessToken"
    
    weak var delegate: FILinkedinLoginDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startAuthorization()
        self.activityIndicator.stopAnimating()
        self.activityIndicator.hidesWhenStopped = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startAuthorization() {
        
        // Specify the response type which should always be "code".
        let responseType = "code"
        // Set the redirect URL. Adding the percent escape characthers is necessary.
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth"
        
        // Create a random string based on the time interval (it will be in the form linkedin12345679).
        let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"
        // Set preferred scope.
        let scope = "r_basicprofile,r_emailaddress"
        // Create the authorization URL string.
        var authorizationURL = "\(authorization_base_url)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(client_id)&"
        authorizationURL += "redirect_uri=\(redirectURL)&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "scope=\(scope)"
        let request = NSURLRequest(url: NSURL(string: authorizationURL)! as URL)
        webView.loadRequest(request as URLRequest)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.url!
      //  print(url)
        
        if url.host == "com.appcoda.linkedin.oauth" {
            
            if url.absoluteString.range(of: "error") != nil {
                self.dismiss(animated: true, completion: nil)
            }
            
            if url.absoluteString.range(of: "code") != nil {
                let urlParts = url.absoluteString.components(separatedBy:"?")
                let code = urlParts[1].components(separatedBy:"=")[1]
                requestForAccessToken(authorizationCode: code)
            }
        }
        return true
    }

    func requestForAccessToken(authorizationCode: String) {

        self.activityIndicator.stopAnimating()
        let grantType = "authorization_code"
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth"
        // Set the POST parameters.
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(redirectURL)&"
        postParams += "client_id=\(client_id)&"
        postParams += "client_secret=\(client_secret)"
        // Convert the POST parameters into a NSData object.
        let postData = postParams.data(using: String.Encoding.utf8)
        // Initialize a mutable URL request object using the access token endpoint URL string.
        let request = NSMutableURLRequest(url: NSURL(string: token_url)! as URL)
        // Indicate that we're about to make a POST request.
        request.httpMethod = "POST"
        // Set the HTTP body using the postData object created above.
        request.httpBody = postData
        // Add the required HTTP header field.
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        // Initialize a NSURLSession object.
        let session = URLSession(configuration: URLSessionConfiguration.default)
        // Make the request.
        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            // Get the HTTP status code of the request.
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    print(">>>>>>>>>>>>>>>>>>>>\(dataDictionary)")
                    
                    if let infoDict = dataDictionary as? Dictionary<String, AnyObject> {
                        if let accessToken = infoDict["access_token"] as? String {
                            UserDefaults.standard.set(accessToken, forKey: "LIAccessToken")
                        }
                    }
                    
                    self.getProfileInfo()

                    DispatchQueue.main.async(execute: { () -> Void in
                        self.dismiss(animated: true, completion: nil)
                    })
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            }
        }
        task.resume()
    }
    
    func getProfileInfo(){
        
        if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") as? String {
            // Specify the URL string that we'll get the profile info from.
            let targetURLString = "https://api.linkedin.com/v1/people/~:(id,num-connections,first-name,last-name,picture-url,formatted-name,email-address,industry,public-profile-url,primary-twitter-account,summary,phone-numbers,date-of-birth,main-address,positions:(title,company:(name)))?format=json"
            
            // Initialize a mutable URL request object.
            let request = NSMutableURLRequest(url: NSURL(string: targetURLString)! as URL)
            
            // Indicate that this is a GET request.
            request.httpMethod = "GET"
            
            // Add the access token as an HTTP header field.
            request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            
            // Initialize a NSURLSession object.
            let session = URLSession(configuration: URLSessionConfiguration.default)
            
            // Make the request.
            let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200 {
                    // Convert the received JSON data into a dictionary.
                    do {
                        let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        print(">>>>>>>>>>>>>>>>>>>>\(dataDictionary)")
                        let dict = NSMutableDictionary()
                        dict["id"] = (dataDictionary as! NSDictionary).object(forKey: "id" as AnyObject
                            ) as! String
                        
                        dict["first_name"] = (dataDictionary as! NSDictionary).object(forKey: "firstName" as AnyObject
                            ) as! String
                        
                        dict["last_name"] = (dataDictionary as! NSDictionary).object(forKey: "lastName" as AnyObject
                            ) as! String
                        
                        dict["email"] = (dataDictionary as! NSDictionary).object(forKey: "emailAddress" as AnyObject) as! String
                            print(dict)
                        self.delegate?.callApiDelegateMethod(response: dict)

                        self.dismiss(animated: true, completion: nil)

                    }
                        
                    catch {
                        print("Could not convert JSON data into a dictionary.")
                    }
                }
            }
            
            task.resume()
        }
    }
    
    @IBAction func backToLogin(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

