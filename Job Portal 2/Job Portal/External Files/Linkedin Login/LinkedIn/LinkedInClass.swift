//
//  LinkedInClass.swift
//  SocialLogin
//
//  Created by Ketan on 12/28/16.
//  Copyright © 2016 kETANpATEL. All rights reserved.
//

import UIKit

var AppClientId: String = "77d3kqwyuusutu"
var AppClientSecrat: String = "5r8JD2nFSBuRXVP1"
var AppId: String = "5249163"


class LinkedInClass: NSObject {

    typealias LISuccessHandler = (_ success:AnyObject) -> Void
    typealias LIFailHandler = (_ success:AnyObject) -> Void
    
    var vc: UIViewController!
    var loginFail: LIFailHandler?
    var loginSucess: LISuccessHandler?
    
    static var linkedInClass: LinkedInClass!

    
    class func sharedInstance() -> LinkedInClass {
        
        if(linkedInClass == nil) {
            linkedInClass = LinkedInClass()
        }
        return linkedInClass
    }

    func loginWithLinkedIn(viewController: UIViewController, successHandler: @escaping LISuccessHandler, failHandler: @escaping LIFailHandler) {
        
        vc = viewController
        loginFail = failHandler
        loginSucess = successHandler
        
        if(Reachabilitys.isNetworkAvailable()) {
            
            LISDKSessionManager.createSession(withAuth: [LISDK_BASIC_PROFILE_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: { (response) in
                
                LISDKAPIHelper.sharedInstance().getRequest("https://api.linkedin.com/v1/people/~", success: { (response) in

                    if let data = response?.data.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dictResponse = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            print("Authenticated  : \(dictResponse)")
                            self.loginSucess!(dictResponse as AnyObject)
                            
                        } catch {
                            print(error.localizedDescription)
                            print("this is error")
                            print(error)
                            self.loginFail!(error.localizedDescription as AnyObject)
                        }
                    }
                }, error: { (error) in
                    self.loginFail!(error?.localizedDescription as AnyObject)
                    print("this is error")
                    print(error)

                })
                
            }, errorBlock: { (error) in
                print("this is error")
                print(error)

                self.loginFail!(error?.localizedDescription as AnyObject)
            })
            
        }
        else {
            print("No internet Connection.")
            self.loginFail!("No internet Connection." as AnyObject)
        }
    }
    
    func logoutFromLinkedIn() {
        
        if(Reachabilitys.isNetworkAvailable()) {
            
            if(LISDKSessionManager.hasValidSession() == true) {
                LISDKSessionManager.clearSession()
            }
            
            self.loginFail!("Logout." as AnyObject)
        }
        else {
            print("No internet Connection.")
            self.loginFail!("No internet Connection." as AnyObject)
        }
    }
    
}
