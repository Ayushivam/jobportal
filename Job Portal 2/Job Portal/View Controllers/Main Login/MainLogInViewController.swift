//
//  MainLogInViewController.swift
//  Job Portal
//
//  Created by nile on 22/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class MainLogInViewController: UIViewController {

    @IBOutlet weak var empBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
       
       if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            let  storyboard:UIStoryboard =  UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(newViewController, animated: false)
        }
        
        if let _ = NSUSERDEFAULT.value(forKey: kEmpID)
        {
            let  storyboard:UIStoryboard =  UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "EmployerHomeViewController") as! EmployerHomeViewController
            self.navigationController?.pushViewController(newViewController, animated: false)
        }
        
        //empBtn.isUserInteractionEnabled = false
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func candidateLoginBtnActn(_ sender: UIButton) {
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        
        newViewController.logInType = loginCheckEnmType.Candidate
        navigationController?.pushViewController(newViewController, animated: true)
        }
    
    @IBAction func employerLoginBtnActn(_ sender: UIButton) {
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        newViewController.logInType = loginCheckEnmType.Employee

        navigationController?.pushViewController(newViewController, animated: true)
    }
}
