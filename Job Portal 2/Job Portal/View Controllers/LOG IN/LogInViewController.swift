//
//  LogInViewController.swift
//  Job Portal
//
//  Created by nile on 05/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

enum loginCheckEnmType {
    case Candidate,Employee
}

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn

class LogInViewController: UIViewController,UITextFieldDelegate,GIDSignInUIDelegate,GIDSignInDelegate {
    
    @IBOutlet weak var googleSignInBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var rememberBtn: UIButton!
    @IBOutlet weak var orLbl: UILabel!
    @IBOutlet weak var emailAddressTextField: PaddedTextField!
    @IBOutlet weak var passwordTextField: PaddedTextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var linkedInBtn: UIButton!
    var logInType : loginCheckEnmType?
    @IBOutlet weak var dontAcEmp: UILabel!
    @IBOutlet weak var dontAcCndidate: UILabel!
    @IBOutlet weak var signUpEmpBtn: UIButton!
    @IBOutlet weak var signUpCandidateBtn: UIButton!
    @IBOutlet weak var skipLoginBtn: UIButton!
    @IBOutlet weak var eyeBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        InitialSetUp()
    }

    //MARK:- Initial Setup
    func InitialSetUp() {
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        if(logInType == loginCheckEnmType.Employee)
        {
            facebookBtn.isHidden = true
            linkedInBtn.isHidden = true
            orLbl.isHidden = true
            dontAcCndidate.isHidden = true
            signUpCandidateBtn.isHidden = true
            skipLoginBtn.isHidden = true
            dontAcEmp.isHidden = true
            signUpEmpBtn.isHidden = true
            googleSignInBtn.isHidden = true
         //   googleSignInBtn.isHidden = true
        }
        else {
            dontAcEmp.isHidden = true
            signUpEmpBtn.isHidden = true
            skipLoginBtn.isHidden = false
           // googleSignInBtn.isHidden = false
            
        }
        
        self.navigationController?.navigationBar.isHidden = true
        skipLoginBtn.layer.cornerRadius = 5.0
        skipLoginBtn.layer.borderWidth = 1.0
        skipLoginBtn.layer.borderColor = UIColor.lightGray.cgColor
        emailAddressTextField.layer.borderWidth = 0.50
        emailAddressTextField.layer.borderColor = UIColor.black.cgColor
        passwordTextField.layer.borderWidth = 0.50
        passwordTextField.layer.borderColor = UIColor.black.cgColor
        emailAddressTextField.layer.cornerRadius = 2.0
        passwordTextField.layer.cornerRadius = 2.0
        emailAddressTextField.keyboardType = .emailAddress
        passwordTextField.isSecureTextEntry = true
        loginBtn.layer.cornerRadius = 2.0
        facebookBtn.layer.cornerRadius = 2.0
        googleSignInBtn.layer.cornerRadius = 2.0

       // googleSignInBtn.layer.cornerRadius = 2.0
        
        linkedInBtn.layer.cornerRadius = 2.0
        rememberBtn.isHidden = true
        self.hideKeyboardWhenTappedAround()
        emailAddressTextField.tag = 100
        passwordTextField.tag = 101
        emailAddressTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Google Signin Delegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
            let fullName = user.profile.name
            let socialId = user.authentication.idToken
            let email = user.profile.email
            let firstname = (fullName?.components(separatedBy: " ")[0])!
            let lastName = (fullName?.components(separatedBy: " ")[1])!
            let imageUrl : NSURL   = user.profile.imageURL(withDimension: 120)! as NSURL
            let url =  String(describing: imageUrl)
            callApiForSocialLogin(firstName: firstname, lastName: lastName, email: email!, pictureUrl: url)
        } else {
        }
    }

    //Google Signin delegate
    @nonobjc func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        
    }
    
    // Present a view that prompts the user to sign in with Google
    @nonobjc func signIn(signIn: GIDSignIn!,presentViewController viewController: UIViewController!) {
        
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    // Dismiss the "Sign in with Google" view
    @nonobjc func signIn(signIn: GIDSignIn!,dismissViewController viewController: UIViewController!) {
        GIDSignIn.sharedInstance().signOut()
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Action Button
    @IBAction func googleSignAction(_ sender: UIButton) {
        
        view.endEditing(true)
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    @IBAction func skipAction(_ sender: UIButton) {
        
        view.endEditing(true)
        
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
        NSUSERDEFAULT.set("0", forKey: kcheckLoginType)

    }
    
    @IBAction func forgotpasswordButtonAction(_ sender: UIButton) {
        
        if(logInType == loginCheckEnmType.Candidate )
        {
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "ForgotpasswordViewController") as! ForgotpasswordViewController
            newViewController.isFromEmployer = false
            navigationController?.pushViewController(newViewController, animated: true)
        }
        else {
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "ForgotpasswordViewController") as! ForgotpasswordViewController
            newViewController.isFromEmployer = true
            navigationController?.pushViewController(newViewController, animated: true)
        }
    }
    
    @IBAction func eyeButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            passwordTextField.isSecureTextEntry = false
        }
        else {
            passwordTextField.isSecureTextEntry = true
        }
    }
    
    @IBAction func logInButtonAction(_ sender: UIButton) {
        
        view.endEditing(true)
        if emailAddressTextField.text?.count == 0 {
            AlertController.alert(title: kWriteEmail)
        }
        else if !(emailAddressTextField.text?.isEmail)!{
            AlertController.alert(title: kValidEmail)
        }
        else if passwordTextField.text?.count == 0{
            AlertController.alert(title: kEmptyPassword)
        }
        else {
            
            let _ = (logInType == loginCheckEnmType.Candidate) ? (callApiForLogInCandidate()) : (callApiForLogInEMP())
        }
    }
    
    @IBAction func bckBtnAction(_ sender: UIButton) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpBtnAction(_ sender: UIButton) {
        
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @IBAction func signUpForEmployeeBtnAction(_ sender: UIButton) {
        
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "SignUpEmpViewController") as! SignUpEmpViewController
        navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @IBAction func fbLoginAction(_ sender: UIButton) {
        facebookLogin()
    }
    
    @IBAction func linkedinAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let webView =  TLinkedinLoginViewController()
        webView.delegate = self
        self.present(webView, animated: true, completion: nil)
        
        
        
//        LinkedInClass.sharedInstance().loginWithLinkedIn(viewController: self, successHandler: { (response) in
//            let dict : Dictionary = response as! Dictionary<String,AnyObject>
//            let firstName: String = dict.validatedValue("firstName", expected: "" as AnyObject) as! String
//            let lastName: String = dict.validatedValue("lastName", expected: "" as AnyObject) as! String
//            let id: String = dict.validatedValue("id", expected: "" as AnyObject) as! String
//            let email : String = id + "@linkedinuser.com"
//            self.callApiForSocialLogin(firstName: firstName, lastName: lastName, email: email, pictureUrl: "")
//        }, failHandler: { (response) in
//        })
    }
    
    // Facebook Login Function
    func facebookLogin() {
        self.view.endEditing(true)
        SocialHelper.facebookManager.getFacebookInfoWithCompletionHandler(fromViewController: self) { (dataDictionary:Dictionary<String, AnyObject>?, error:NSError?) -> Void in
            if let infoDict = dataDictionary {
                //   print(infoDict)
                let firstName : String = (infoDict.validatedValue("first_name", expected: "" as AnyObject) as! String)
                let lastName : String = (infoDict.validatedValue("last_name", expected: "" as AnyObject) as! String)
                
                
                let imageURL : String = (((infoDict["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String)!
                
                var email:String = infoDict.validatedValue("email", expected: "" as AnyObject) as! String
                
                let socialId : String = infoDict.validatedValue("id", expected: "" as AnyObject) as! String
                if email == ""
                {
                    email = socialId + "@facebookuser.com"
                }
                self.callApiForSocialLogin(firstName: firstName, lastName: lastName, email: email, pictureUrl: imageURL)
            } else {
            }
        }
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 50
            {
                return false
            }
            break
        case 101:
            if str.length > 15
            {
                return false
            }
        default:
            break
        }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
    }
    
    //MARK :- WEB API
    func callApiForLogInCandidate()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["email"] = emailAddressTextField.text
        paramDict["password"] = passwordTextField.text
        ServiceHelper.request(paramDict, method: .post, apiName: kLoginForCandidate, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Login Success") {
                                NSUSERDEFAULT.set("0", forKey: kcheckLoginType)

                                NSUSERDEFAULT.set(self.emailAddressTextField.text, forKey: kUserEmail)
                                let userID : String = response.validatedValue("user_id", expected: "" as AnyObject) as! String
                                NSUSERDEFAULT.set(userID, forKey: kUserID)
                                print(userID)
                                
                                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                let newViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.navigationController?.pushViewController(newViewController, animated: true)
                            }
                        }
                        else if status == false {
                            if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Sorry Your email is not verified")
                            {
                                self.callApiForResendOTP(apiname: kResendOTP)
                            }
                                
                            else  if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Email or password is incorrect!") {
                                AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            }
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiForResendOTP(apiname:String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["email"] = emailAddressTextField.text
        ServiceHelper.request(paramDict, method: .post, apiName: apiname, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            
                            let otpValue : String = response.validatedValue("verified_code", expected: "" as AnyObject)  as! String
                            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                            let otpVC = storyboard.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
                            otpVC.email = self.emailAddressTextField.text!
                            otpVC.otp = otpValue
                            self.navigationController?.pushViewController(otpVC, animated: true)
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    
    func callApiForLogInEMP()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["email"] = emailAddressTextField.text
        paramDict["password"] = passwordTextField.text
        ServiceHelper.request(paramDict, method: .post, apiName: kLoginForEmployee, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Login Success") {
                                let empID : String = response.validatedValue("emp_id", expected: "" as AnyObject) as! String
                                NSUSERDEFAULT.set("0", forKey: kcheckLoginType)
                                NSUSERDEFAULT.set(empID, forKey: kEmpID)
                                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                let newViewController = storyboard.instantiateViewController(withIdentifier: "EmployerHomeViewController") as! EmployerHomeViewController
                                self.navigationController?.pushViewController(newViewController, animated: true)
                            }
                        }
                        else if status == false {
                            if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Sorry Your email is not verified")
                            {
                                self.callApiForResendOTP(apiname: kEMPResendOTP)
                            }
                            else  if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Email or password is incorrect!") {
                                AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            }
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    //MARK :- Facebook Login
    
    func callApiForSocialLogin(firstName:String,lastName:String,email:String,pictureUrl:String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["first_name"] = firstName
        paramDict["last_name"] = lastName
        paramDict["email"] = email
        paramDict["thumbnail"] = pictureUrl
        
        ServiceHelper.request(paramDict, method: .post, apiName: kFacebookLogin, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            if (response.validatedValue("message", expected: "" as AnyObject) as! String == "You have registered successfully.") {
                                NSUSERDEFAULT.set("1", forKey: kcheckLoginType)

                                NSUSERDEFAULT.set(self.emailAddressTextField.text, forKey: kUserEmail)
                                let userID : String = response.validatedValue("user_id", expected: "" as AnyObject) as! String
                                NSUSERDEFAULT.set(userID, forKey: kUserID)
                                print(userID)
                                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                let newViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.navigationController?.pushViewController(newViewController, animated: true)
                            }
                            
                        }
                        else if status == false {
                            if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Sorry Your email is not verified")
                            {
                                self.callApiForResendOTP(apiname: kResendOTP)
                            }
                                
                            else  if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Email or password is incorrect!") {
                                AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            }
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
}

extension LogInViewController : FILinkedinLoginDelegate{

    func callApiDelegateMethod(response: NSDictionary) {
        print(response)

        let dict : Dictionary = response as! Dictionary<String,AnyObject>
        let firstName: String = dict.validatedValue("first_name", expected: "" as AnyObject) as! String
        let lastName: String = dict.validatedValue("last_name", expected: "" as AnyObject) as! String
        let id: String = dict.validatedValue("id", expected: "" as AnyObject) as! String
        let email : String = id + "@linkedinuser.com"
        self.callApiForSocialLogin(firstName: firstName, lastName: lastName, email: email, pictureUrl: "")
    }
}
