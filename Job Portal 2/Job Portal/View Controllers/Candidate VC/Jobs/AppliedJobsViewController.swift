//
//  AppliedJobsViewController.swift
//  Job Portal
//
//  Created by nile on 31/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class AppliedJobsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var getAppliedJobListlistArray = [UserInfo]()
    
    @IBOutlet weak var appliedJobTableViewOutlet: UITableView!
    @IBOutlet weak var noDataFoundLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appliedJobTableViewOutlet.estimatedRowHeight = 165
        appliedJobTableViewOutlet.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        callApiGetAppliedJob()
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 165
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getAppliedJobListlistArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "AppliedJobsTableViewCell", for: indexPath) as! AppliedJobsTableViewCell
        let obj = getAppliedJobListlistArray[indexPath.row]
        
        cell.companyLabel.text = obj.companyName
        cell.companyLogoImageView.sd_setImage(with: URL(string: obj.companyLogo), placeholderImage: UIImage(named:"no-image"), options: SDWebImageOptions(rawValue: 0))
        cell.jobPostTitle.text = obj.jobTitle
        cell.dateLabel.text = "Applied Date: " + obj.appliedDate
        cell.locationLabel.text = obj.address
        cell.removeBtn.tag =  indexPath.row+2500
        cell.removeBtn.addTarget(self, action: #selector(DeleteButtonAction), for: .touchUpInside)
        cell.viewJobBtn.tag =  indexPath.row+3500
        cell.viewJobBtn.addTarget(self, action: #selector(ViewJobButtonAction), for: .touchUpInside)
        return cell
    }
    
    
    @objc func ViewJobButtonAction(_sender:UIButton) {
        
        let index  = _sender.tag-3500
        let viewObj = self.getAppliedJobListlistArray[index]
        let jobId = viewObj.jobID
        let empId = viewObj.eID
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "ViewJobViewController") as! ViewJobViewController
        newViewController.jobID = jobId
        newViewController.empID = empId
        APPDELEGATE.navController?.pushViewController(newViewController, animated: true)
        
    }
    
    
    @objc func DeleteButtonAction(button:UIButton){
        
        AlertController.alert(title: "", message: "Are you sure, you want to remove?", buttons: ["No","Yes"]) { (alert, index) in
            if index == 1{
                let index = button.tag-2500
                let obj = self.getAppliedJobListlistArray[index]
                let jobid = obj.jobID
                self.callApiForDeleteAppliedJobs(jobID: jobid, index: index)
                
            }
        }
    }
    
    
    //MARK :- WEB API
    func callApiGetAppliedJob()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        
        ServiceHelper.request(paramDict, method: .post, apiName:kApplyJobList , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == false {
                            self.getAppliedJobListlistArray.removeAll()
                            self.appliedJobTableViewOutlet.reloadData()
                            
                            self.noDataFoundLabel.isHidden = false
                        }
                        else if status == true
                        {
                            self.getAppliedJobListlistArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.companyName = dict.validatedValue("company_name", expected: "" as AnyObject) as! String
                                userObj.companyLogo = dict.validatedValue("company_logo", expected: "" as AnyObject) as! String
                                userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String
                                
                                userObj.appliedDate = APPDELEGATE.formattedDateFromString(dateString:dict.validatedValue("applied_date", expected: "" as AnyObject) as! String,withFormat:"dd-MMM-yyyy")!
                              
                                userObj.address = dict.validatedValue("address", expected: "" as AnyObject) as! String
                                
                                userObj.empId = dict.validatedValue("emp_id", expected: "" as AnyObject) as! String
                                userObj.jobID = dict.validatedValue("job_id", expected: "" as AnyObject) as! String
                                
                                let _ = (userObj.companyLogo  == "") ? (userObj.companyLogo = "") : (userObj.companyLogo =  userObj.companyLogo )
                                
                                userObj.url = dict.validatedValue("url", expected: "" as AnyObject) as! String
                                let arr = userObj.url.components(separatedBy: ":")
                                if arr.count == 1
                                {
                                    userObj.url = "https://" + userObj.url
                                }
                                
                                self.getAppliedJobListlistArray.insert(userObj, at: 0)
                            }
                            self.appliedJobTableViewOutlet.reloadData()
                            
                            if self.getAppliedJobListlistArray.count == 0 {
                                self.noDataFoundLabel.isHidden = true
                            }
                            else {
                                self.noDataFoundLabel.isHidden = true
                            }
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    func callApiForDeleteAppliedJobs(jobID: String,index:Int)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["job_id"] = jobID
        
        
        ServiceHelper.request(paramDict, method: .post, apiName: kDeleteAppliedJobs, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            self.getAppliedJobListlistArray.remove(at: index)
                            self.appliedJobTableViewOutlet.reloadData()
                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                            self.view.makeToast(MSG)
                            
                            if self.getAppliedJobListlistArray.count == 0 {
                                self.noDataFoundLabel.isHidden = false
                            }
                            else {
                                self.noDataFoundLabel.isHidden = true
                                
                            }
                        }
                        else if status == false {
                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                            self.view.makeToast(MSG)
                        }
                        else {
                            if (self.getAppliedJobListlistArray.count == 0)
                            {
                                self.noDataFoundLabel.isHidden = false
                            }
                            
                        }
                        
                    }
                }
            }
        }
        
    }
}
