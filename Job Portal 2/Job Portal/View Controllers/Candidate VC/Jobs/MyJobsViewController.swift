//
//  MyJobsViewController.swift
//  Job Portal
//
//  Created by nile on 31/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class MyJobsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var getmyJobListlistArray = [UserInfo]()

    @IBOutlet weak var myJobTableViewOutlet: UITableView!
    @IBOutlet weak var noDataFoundLabel: UILabel!

   // @IBOutlet weak var noDataFoundLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        myJobTableViewOutlet.estimatedRowHeight = 170
        myJobTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        self.noDataFoundLabel.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
         callApiGetMyJob()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- TableView Delegates
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 170
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getmyJobListlistArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "MyJobsTableViewCell", for: indexPath) as! MyJobsTableViewCell
        let obj = getmyJobListlistArray[indexPath.row]
        cell.compnyNameLabel.text = obj.companyName
        cell.addressLabel.text = obj.address
          cell.companyLogoImageView.sd_setImage(with: URL(string: obj.companyLogo), placeholderImage: UIImage(named:"no-image"), options: SDWebImageOptions(rawValue: 0))
        cell.jobPostLabel.text = obj.jobTitle
        cell.applyNowLAbel.text = "Status: " + obj.appliedStatus
        cell.urlButton.tag = indexPath.row+1500
        cell.removeBtn.tag =  indexPath.row+2500
        cell.heartButton.tag = indexPath.row+3500
        cell.viewJobBtn.tag = indexPath.row+4500
        cell.viewJobBtn.addTarget(self, action: #selector(viewJobButtonAction), for: .touchUpInside)
        cell.urlButton.addTarget(self, action: #selector(urlButtonAction), for: .touchUpInside)
        cell.removeBtn.addTarget(self, action: #selector(DeleteButtonAction), for: .touchUpInside)
        cell.heartButton.addTarget(self, action: #selector(saveJobButtonAction), for: .touchUpInside)

        return cell
    }

    
        @objc func viewJobButtonAction(_sender:UIButton) {
            
            let index  = _sender.tag-4500
            let viewObj = self.getmyJobListlistArray[index]
            let jobId = viewObj.jobID
            let empId = viewObj.empId
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "ViewJobViewController") as! ViewJobViewController
            newViewController.jobID = jobId
            newViewController.empID = empId
            APPDELEGATE.navController?.pushViewController(newViewController, animated: true)
        }

    
    @objc func urlButtonAction(_sender:UIButton) {
        
        let index = _sender.tag - 1500
        let URLobj = self.getmyJobListlistArray[index]
        let url = URLobj.url
        if let url = URL(string: "\(url)")
        {
            print(url)
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc func saveJobButtonAction(_sender:UIButton) {
        view.endEditing(true)
        let index = _sender.tag-3500
        let obj = self.getmyJobListlistArray[index]
        let jobid = obj.jobID
        self.callApiForDeleteMyJob(jobID: jobid, index: index)
    }

    
    @objc func DeleteButtonAction(button:UIButton){
        
        AlertController.alert(title: "", message: "Are you sure, you want to remove?", buttons: ["No","Yes"]) { (alert, index) in
            if index == 1{
                let index = button.tag-2500
                let obj = self.getmyJobListlistArray[index]
                let jobid = obj.jobID
                self.callApiForDeleteMyJob(jobID: jobid, index: index)
                
            }
        }
    }
    
    
    //MARK :- WEB API
    func callApiGetMyJob()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        
        ServiceHelper.request(paramDict, method: .post, apiName:kMyJobList , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == false {
                            self.getmyJobListlistArray.removeAll()
                            self.myJobTableViewOutlet.reloadData()
                            
                          self.noDataFoundLabel.isHidden = false
                        }
                        else if status == true
                        {
                            self.getmyJobListlistArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.companyName = dict.validatedValue("company_name", expected: "" as AnyObject) as! String
                                userObj.address = dict.validatedValue("address", expected: "" as AnyObject) as! String
                                
                                userObj.companyLogo = dict.validatedValue("company_logo", expected: "" as AnyObject) as! String
                                userObj.url = dict.validatedValue("url", expected: "" as AnyObject) as! String
                                userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String
                                userObj.empId = dict.validatedValue("employer_id", expected: "" as AnyObject) as! String
                                userObj.jobID = dict.validatedValue("job_id", expected: "" as AnyObject) as! String
                                userObj.appliedStatus = dict.validatedValue("applied", expected: "" as AnyObject) as! String
                                
//                                let _ = (userObj.companyLogo  == "") ? (userObj.companyLogo = "") : (userObj.companyLogo = "https://www.niletechinnovations.com/projects/jobportal/uploads/company_logo/" + userObj.companyLogo )
//                                
                                userObj.url = dict.validatedValue("url", expected: "" as AnyObject) as! String
                                
                                let arr = userObj.url.components(separatedBy: ":")
                                if arr.count == 1
                                {
                                    userObj.url = "https://" + userObj.url
                                }
                                
                                self.getmyJobListlistArray.insert(userObj, at: 0)
                            }
                            self.myJobTableViewOutlet.reloadData()
                            
                            if self.getmyJobListlistArray.count == 0 {
                                self.noDataFoundLabel.isHidden = true
                            }
                            else {
                                self.noDataFoundLabel.isHidden = true
                            }
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    func callApiForDeleteMyJob(jobID: String,index:Int)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["job_id"] = jobID
        
        
        ServiceHelper.request(paramDict, method: .post, apiName: kDeleteMyJob, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            self.getmyJobListlistArray.remove(at: index)
                            self.myJobTableViewOutlet.reloadData()
                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                            self.view.makeToast(MSG)
                            if self.getmyJobListlistArray.count == 0 {
                                self.noDataFoundLabel.isHidden = false
                            }
                            else {
                                self.noDataFoundLabel.isHidden = true

                            }
                        }
                            
                        else if status == false {
                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                            self.view.makeToast(MSG)
                        }
                        else {
                            if (self.getmyJobListlistArray.count == 0)
                            {
                                self.noDataFoundLabel.isHidden = false
                            }
                       
                            
                        }
                        
                    }
                }
            }
        }   
    }
    

}

