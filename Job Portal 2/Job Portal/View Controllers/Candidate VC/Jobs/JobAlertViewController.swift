//
//  JobAlertViewController.swift
//  Job Portal
//
//  Created by nile on 31/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class JobAlertViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,GetListDelegate {
    func passSaveJobData() {
        callApiGetJobAlert()

    }
    
    @IBOutlet weak var noDataFoundLabel: UILabel!

    @IBOutlet weak var JobAlertTableViewOutlet: UITableView!
    var jobAlertListlistArray = [UserInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
       callApiGetJobAlert()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobAlertListlistArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "jobAlertTableViewCell", for: indexPath) as! jobAlertTableViewCell
          let obj = jobAlertListlistArray[indexPath.row]
        let _ = (obj.myJob == 1) ? (cell.heartButton.setImage(#imageLiteral(resourceName: "fillHeart"), for: .normal)) : (cell.heartButton.setImage(#imageLiteral(resourceName: "heart"), for: .normal))
        cell.companyLabel.text = obj.companyName
        cell.companyLogoImageView.sd_setImage(with: URL(string: obj.companyLogo), placeholderImage: UIImage(named:"no-image"), options: SDWebImageOptions(rawValue: 0))
        
      
        cell.postedDate.text = "Posted Date: " + obj.postedDate
        cell.locationLabel.text = obj.address
        cell.jobPostTitle.text = obj.jobTitle
        cell.urlBtn.tag = indexPath.row+1500
        cell.heartButton.tag = indexPath.row+2100
        cell.viewJobBtn.tag = indexPath.row+3100
        cell.viewJobBtn.addTarget(self, action: #selector(viewJobButtonAction), for: .touchUpInside)
        cell.heartButton.addTarget(self, action: #selector(saveJobButtonAction), for: .touchUpInside)
        cell.urlBtn.addTarget(self, action: #selector(urlButtonAction), for: .touchUpInside)
        return cell
       
    }
    
        @objc func viewJobButtonAction(_sender:UIButton) {
        let index  = _sender.tag-3100
        let viewObj = self.jobAlertListlistArray[index]
        let jobId = viewObj.jobID
        let empId = viewObj.eID
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let newViewController = storyboard.instantiateViewController(withIdentifier: "ViewJobViewController") as! ViewJobViewController
        //newViewController.delegate = self
        newViewController.jobID = jobId
        newViewController.empID = empId
        APPDELEGATE.navController?.pushViewController(newViewController, animated: true)
    }
    

    @objc func urlButtonAction(_sender:UIButton) {
        
        let index = _sender.tag-1500
        let URLobj = self.jobAlertListlistArray[index]
        let url = URLobj.url
        if let url = URL(string: "\(url)")
        {
            print(url)
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc func saveJobButtonAction(_sender:UIButton) {
        
        view.endEditing(true)
        let index  = _sender.tag-2100
        let saveJobObj = self.jobAlertListlistArray[index]
        let jobId = saveJobObj.jobID
        if saveJobObj.myJob == 0
        {
            saveJobObj.myJob = 1
            self.jobAlertListlistArray[index] = saveJobObj
            let heartButton : UIButton = view.viewWithTag(_sender.tag) as! UIButton
            heartButton.setImage(#imageLiteral(resourceName: "fillHeart"), for: .normal)
            callApiGetSaveJob(jobID: jobId )
        }
        else {
            saveJobObj.myJob = 0
            self.jobAlertListlistArray[index] = saveJobObj
         
            callApiForDeleteMyJob(jobID:jobId,tag:_sender.tag)
        }
    }
    
    //MARK :- WEB API
    func callApiGetJobAlert()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        
        ServiceHelper.request(paramDict, method: .post, apiName: kGetListJobAlert , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == false {
                            self.jobAlertListlistArray.removeAll()
                            self.JobAlertTableViewOutlet.reloadData()
                            
                              self.noDataFoundLabel.isHidden = false
                        }
                        else if status == true
                        {
                            self.jobAlertListlistArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.employeeID = dict.validatedValue("employee_id", expected: "" as AnyObject) as! String
                                userObj.companyName = dict.validatedValue("company_name", expected: "" as AnyObject) as! String
                             
                                userObj.companyLogo = dict.validatedValue("company_logo", expected: "" as AnyObject) as! String
                                userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String
                                userObj.address = dict.validatedValue("address", expected: "" as AnyObject) as! String
                                
                                userObj.postedDate = dict.validatedValue("posted_date", expected: "" as AnyObject) as! String

                                
                                userObj.jobID = dict.validatedValue("job_id", expected: "" as AnyObject) as! String
                                userObj.myJob = dict.validatedValue("my_jobs", expected: false as AnyObject) as! Int
                                
//                                let _ = (userObj.companyLogo  == "") ? (userObj.companyLogo = "") : (userObj.companyLogo = "https:////www.niletechinnovations.com/projects/jobportal/uploads/company_logo/" + userObj.companyLogo )
//
                                userObj.url = dict.validatedValue("url", expected: "" as AnyObject) as! String
                                
                                let arr = userObj.url.components(separatedBy: ":")
                                if arr.count == 1
                                {
                                    userObj.url = "https://" + userObj.url
                                }
                                
                                self.jobAlertListlistArray.append(userObj)
                                
                              
                            }
                            self.JobAlertTableViewOutlet.reloadData()
                            
                                                        if self.jobAlertListlistArray.count == 0 {
                                                        self.noDataFoundLabel.isHidden = true
                                                        }
                                                       else {
                                                          self.noDataFoundLabel.isHidden = true
                                                        }
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
  
    func callApiGetSaveJob(jobID : String)
    {
        var paramDict = Dictionary<String,Any>()
        let userid : String = NSUSERDEFAULT.value(forKey: kUserID) as! String
        paramDict["user_id"] = userid
        paramDict["job_id"] = jobID
        
        ServiceHelper.request(paramDict, method: .post, apiName: kSaveJobs, hudType: .smoothProgress) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                            self.view.makeToast(MSG)
                        }
                        else if status == false {
                            
                            
                        }
                        else {
                            
                        }
                    }else{
                    }
                }
            }
        }
    }
    
    func callApiForDeleteMyJob(jobID: String,tag:Int)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["job_id"] = jobID
        
        
        ServiceHelper.request(paramDict, method: .post, apiName: kDeleteMyJob, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            
                            let heartButton : UIButton = self.view.viewWithTag(tag) as! UIButton
                            heartButton.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                            self.view.makeToast(MSG)
                        }
                        else {
                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                            self.view.makeToast(MSG)
                        }
                        
                        
                    }
                    
                }
            }
        }
    }
    
}

