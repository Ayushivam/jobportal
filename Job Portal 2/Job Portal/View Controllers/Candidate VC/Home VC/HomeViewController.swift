//
//  HomeViewController.swift
//  Job Portal
//
//  Created by nile on 23/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
class HomeViewController: UIViewController {

    @IBOutlet weak var notiCount: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var dashboardBtn: UIButton!
    @IBOutlet weak var jobsBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var heightContraints: NSLayoutConstraint!
    @IBOutlet weak var topTitlebl: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var logOutBtn: UIButton!
    var currentVC: UIViewController?
    var searchVc: SearchViewController?
    var jobsVc: JobsViewController?
    var dashboardVc: DashboardViewController?
    var menuVc: MenuViewController?

    var  baseNavController: UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        configureHeight()

    }

    func initialSetUp() {

        notiCount.layer.cornerRadius = 23/2
        notiCount.clipsToBounds = true
        notiCount.isHidden = true
        self.navigationController?.navigationBar.isHidden = true

        bottomView.center = self.view.center
        bottomView.backgroundColor = UIColor.white
        bottomView.layer.shadowColor = UIColor.lightGray.cgColor
        bottomView.layer.shadowOpacity = 5
        bottomView.layer.shadowOffset = CGSize.zero
        bottomView.layer.shadowRadius = 4

        baseNavController = UINavigationController()
        baseNavController?.isNavigationBarHidden = true
        let baseStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

        searchVc = baseStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
         jobsVc = baseStoryboard.instantiateViewController(withIdentifier: "JobsViewController") as? JobsViewController
        dashboardVc = baseStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
        menuVc = baseStoryboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateCount"), object: nil)
      
        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            dashboardBtn.setImage(UIImage(named: "dashboardB"), for: .normal)
            topTitlebl.text = "DASHBOARD"
            
            self.display(dashboardVc!)
        }else {
            searchBtn.setImage(UIImage(named: "searchB"), for: .normal)
            topTitlebl.text = "SEARCH"
            self.display(searchVc!)
        }
    }
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func showNotiCount(_ notification: NSNotification) {

    }

    func replaceCurrentVC(withVC newVC: UIViewController) {
        currentVC?.willMove(toParentViewController: nil)
        currentVC?.view.removeFromSuperview()
        currentVC?.removeFromParentViewController()
        currentVC = newVC
        addChildViewController(currentVC!)
        containerView?.addSubview((currentVC?.view)!)
        currentVC?.view.frame = (containerView?.bounds)!
        currentVC?.didMove(toParentViewController: self)
    }

    func display(_ viewController: UIViewController) {
        
        baseNavController?.viewControllers = [viewController]
        baseNavController?.isNavigationBarHidden = true
        replaceCurrentVC(withVC: baseNavController!)
    }
    
    func configureHeight()
           {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                heightContraints.constant = 60
            //  print("iPhone 5 or 5S or 5C")
            case 1334:
                heightContraints.constant = 60
            //  print("iPhone 6/6S/7/8")
            case 1920, 2208:
                heightContraints.constant = 60
            //   print("iPhone 6+/6S+/7+/8+")
            case 2436:
                heightContraints.constant = 80
            //   print("iPhone X, Xs")
            case 2688:
                heightContraints.constant = 80
            //   print("iPhone Xs Max")
            case 1792:
                heightContraints.constant = 80
                print("iPhone Xr")
            default:
                break
                //   print("unknown")
            }
        }
    }

    @IBAction func notificationBtnAction(_ sender: UIButton) {

        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "NotificationDetailsViewController") as! NotificationDetailsViewController
            APPDELEGATE.navController?.pushViewController(newViewController, animated: false)
        }else {
            AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                
                if index == 1{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    @IBAction func logOUTBTNACTION(_ sender: UIButton) {
        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
               let refreshAlert = UIAlertController(title: "Log Out", message: "Are you sure, you want to log out? ", preferredStyle: UIAlertControllerStyle.alert)

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
                refreshAlert .dismiss(animated: true, completion: nil)
            
                }))
            
            refreshAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in
                                    NSUSERDEFAULT.removeObject(forKey: kUserID)
                                    SocialHelper.facebookManager.logoutFromFacebook()
                                    FBSDKLoginManager().logOut()
                                    self.navigationController?.popToRootViewController(animated: true)
            
                
                }))
            
            
                present(refreshAlert, animated: true, completion: nil)
        }
        
        else {
            AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                
                if index == 1{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }

    @IBAction func commonActionBtn(_ sender: UIButton) {
        
        switch sender.tag {
        case 11:
            initialImage()
            dashboardBtn.setImage(UIImage(named: "dashboardB"), for: .normal)
            topTitlebl.text = "DASHBOARD"
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateCount"), object: nil)
            self.display(dashboardVc!)
            break
          
        case 12:
            
            if let _ = NSUSERDEFAULT.value(forKey: kUserID)
            {
                initialImage()
                jobsBtn.setImage(UIImage(named: "briefcaseB"), for: .normal)
                topTitlebl.text = "JOBS"
                self.display(jobsVc!)
            }else {
                
                AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                    
                    if index == 1{
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
                
            }
            
           
            break
            
        case 13:
            initialImage()
            searchBtn.setImage(UIImage(named: "searchB"), for: .normal)
            topTitlebl.text = "SEARCH"
            self.display(searchVc!)
            break
        case 14:
            
            initialImage()
            menuBtn.setImage(UIImage(named: "menuB"), for: .normal)
            topTitlebl.text = "MENU"
            self.display(menuVc!)
            break
        default:
            break
    }
    }
    
    func initialImage()
    {
        searchBtn.setImage(UIImage(named: "search"), for: .normal)
        jobsBtn.setImage(UIImage(named: "briefcase"), for: .normal)
        dashboardBtn.setImage(UIImage(named: "dashboard"), for: .normal)
        menuBtn.setImage(UIImage(named: "menu"), for: .normal)
    }
   
}
