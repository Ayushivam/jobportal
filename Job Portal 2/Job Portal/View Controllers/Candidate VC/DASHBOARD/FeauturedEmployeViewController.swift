//
//  FeauturedEmployeViewController.swift
//  Job Portal
//
//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class FeauturedEmployeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var FeauturedEmpTableViewOutlet: UITableView!
    var feautureEmpArray = [UserInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FeauturedEmpTableViewOutlet.estimatedRowHeight = 107
        FeauturedEmpTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        
        callApiGetFeautureEmp()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 214
//    }
//    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feautureEmpArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "FeaturedEmployerTableViewCell", for: indexPath) as! FeaturedEmployerTableViewCell
        let feutEmpObj = feautureEmpArray[indexPath.row]
        cell.companyLogoImageView.sd_setImage(with: URL(string: feutEmpObj.companyLogo), placeholderImage: UIImage(named:"no-image"), options: SDWebImageOptions(rawValue: 0))
        
        cell.contentLabel.text = feutEmpObj.desp
        return  cell
    }
    
    
    //MARK:- WEB API
    func callApiGetFeautureEmp()
    {
        var paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kFeautureEmployer , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == false {
                            self.feautureEmpArray.removeAll()
                            self.FeauturedEmpTableViewOutlet.reloadData()
                            
                            //   self.noDataFoundLabel.isHidden = false
                        }
                        else if status == true
                        {
                            self.feautureEmpArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                              
                                userObj.companyLogo = dict.validatedValue("company_logo", expected: "" as AnyObject) as! String
                                
                                let _ = (userObj.companyLogo  == "") ? (userObj.companyLogo = "") : (userObj.companyLogo = userObj.companyLogo )
                                
                                userObj.desp = dict.validatedValue("description", expected: "" as AnyObject) as! String
                                self.feautureEmpArray.append(userObj)
                                
                                
                            }
                            self.FeauturedEmpTableViewOutlet.reloadData()
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    
    
    }
    
    

    
}
