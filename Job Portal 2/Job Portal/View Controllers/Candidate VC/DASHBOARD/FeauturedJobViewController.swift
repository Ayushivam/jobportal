//
//  FeauturedJobViewController.swift
//  Job Portal
//
//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class FeauturedJobViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    var feautureJobArray = [UserInfo]()
    var emailAddress = ""
    var jobid = ""
    @IBOutlet weak var feauturedJobTableViewOutlet: UITableView!
    override func viewDidLoad() {
        callApiGetFeautureJob()
        super.viewDidLoad()
        feauturedJobTableViewOutlet.estimatedRowHeight = 107
        feauturedJobTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feautureJobArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeaturedJobTableViewCell", for: indexPath) as! FeaturedJobTableViewCell
        let obj = feautureJobArray[indexPath.row]
        cell.jobTitleLBL.text = obj.jobTitle
        cell.locationLBL.text = obj.countryName
        cell.contentLBL.text = obj.desp
        cell.dateLBL.text = "Posted Date:" + obj.postedDate
        cell.emailBtn.tag = indexPath.row+1500
        cell.viewJobBtn.tag = indexPath.row+2500
        cell.emailBtn.addTarget(self, action: #selector(emailBtnAction), for: .touchUpInside)
        
        cell.viewJobBtn.addTarget(self, action: #selector(viewJobButtonAction), for: .touchUpInside)

        return cell
    }
    
    
    
    @objc func viewJobButtonAction(_sender:UIButton) {
        
        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            let index  = _sender.tag-2500
            let viewObj = self.feautureJobArray[index]
            let jobId = viewObj.jobID
            let empId = viewObj.employeeID
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "ViewJobViewController") as! ViewJobViewController
            // newViewController.delegate = self
            newViewController.addMyJobs = true
            newViewController.jobID = jobId
            newViewController.empID = empId
            navigationController?.pushViewController(newViewController, animated: true)
        }
        else {
            
            AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                
                if index == 1{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    @objc func emailBtnAction(_sender:UIButton) {
        
        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            let index = _sender.tag - 1500
            let idObj = self.feautureJobArray[index]
            self.jobid = idObj.jobID
            
            emailPopUp()
        }else {
            
            AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                
                if index == 1{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    func emailPopUp(){

        let alertController = UIAlertController(title: "Enter Email", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Share Job", style: .default, handler: { alert -> Void in
            let valueText = alertController.textFields![0] as UITextField
            if valueText.text == ""
            {
                self.validationPopup(message: "Please enter email.")
                return
            }  else if !(valueText.text?.isEmail)!
            {
                self.validationPopup(message: "Please enter valid email address.")
                return
            }

            self.emailAddress =  valueText.text!
            self.callApiForSharedJob(jobID: self.jobid, emailAddress: valueText.text!)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email"
            textField.keyboardType = .emailAddress
            self.present(alertController, animated: true, completion: nil)
        })
    }
    
    func validationPopup(message:String)
    {

        AlertController.alert(title: "", message: message , buttons: ["OK"]) { (alert, index) in
            self.emailPopUp()
        }
    }

        //MARK :- WEB API
        func callApiGetFeautureJob()
        {
            var paramDict = Dictionary<String, Any>()
    
            ServiceHelper.request(paramDict, method: .get, apiName: kFeautureJobs , hudType: .default) { (result, error, code) in
                if let error = error {
                    AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
                }
    
                else {
                    if let response = result as? Dictionary<String, AnyObject> {
    
                        let responseCode = code
                        if (Int(responseCode) == 200) {
                            let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
    
                            if status == false {
                                self.feautureJobArray.removeAll()
                                self.feauturedJobTableViewOutlet.reloadData()
                            }
                            else if status == true
                            {
                                self.feautureJobArray.removeAll()
                                let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                                for i in data
                                {
                                    let userObj = UserInfo()
                                    let dict :Dictionary = i as! Dictionary<String,AnyObject>
    
                                    userObj.jobID = dict.validatedValue("job_id", expected: "" as AnyObject) as! String
                                    userObj.employeeID = dict.validatedValue("employer_id", expected: "" as AnyObject) as! String
                                    userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String
                                    userObj.countryName = dict.validatedValue("country_name", expected: "" as AnyObject) as! String
    
                                    userObj.postedDate = dict.validatedValue("posted_date", expected: "" as AnyObject) as! String
                                    userObj.desp = dict.validatedValue("description", expected: "" as AnyObject) as! String
                                    self.feautureJobArray.append(userObj)
                                }
                                self.feauturedJobTableViewOutlet.reloadData()

                            }
    
                        } else{
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }
                }
            }
        }
    
    
    
    func callApiForSharedJob(jobID : String , emailAddress : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["job_id"] = jobID
        paramDict["email"] = emailAddress
        ServiceHelper.request(paramDict, method: .post, apiName: kSharedJobs, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                        else if status == false {
                            
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
}
