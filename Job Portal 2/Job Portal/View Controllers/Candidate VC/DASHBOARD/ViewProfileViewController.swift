//
//  ViewProfileViewController.swift
//  Job Portal
//
//  Created by nile on 15/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
import PDFReader

class ViewProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,URLSessionDownloadDelegate,UIDocumentInteractionControllerDelegate{
    @IBOutlet weak var userEmailAddressLabel: UILabel!
    @IBOutlet weak var userlocationLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var downloadCVBtn: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    var titleArray = Array<String>()
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    var resumeUrl = ""
    var viewProfileArray = [UserInfo]()
    var experienceArray = [UserInfo]()
    var profSkillArray = [UserInfo]()
    var downloadURL = ""
    var address = ""
    var getJobOverViewArray =  Array<String>()
    
    @IBOutlet weak var viewProfileTableViewOutlet: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.frame.size.width / 2;
        self.userProfileImageView.clipsToBounds = true
        self.userProfileImageView.layer.borderWidth = 1.0
        self.userProfileImageView.layer.borderColor = UIColor.black.cgColor
        userNameLabel.text = ""
        userEmailAddressLabel.text = ""
        userlocationLabel.text = ""
        callApiGetViewProfile()
        clearTempFolder()
        viewProfileTableViewOutlet.estimatedRowHeight = 100
        viewProfileTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: (self as URLSessionDelegate), delegateQueue: OperationQueue.main)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func dowloadCVBtnAction(_ sender: UIButton) {
        
        if self.resumeUrl == ""
        {
            AlertController.alert(title: "No Resume Found.")

        }else {
            showFileWithPath(path: resumeUrl)
        }
    }
    
    
    func clearTempFolder() {
        
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            for fileURL in fileURLs {
                if fileURL.pathExtension == "pdf" {
                    try FileManager.default.removeItem(at: fileURL)
                }}
        } catch { print(error) }
        
        
    }
    
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        //  let string : String = randomStringWithLength(len: 4) as String
        
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/file.pdf"))
        let str : String = "\(destinationURLForFile)"
        self.downloadURL = str
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            showFileWithPath(path: str)
        }
        else{
            do {
                try fileManager.moveItem(at: location, to: destinationURLForFile)
                
                DispatchQueue.main.async {
                    if  str != ""
                    {
                        self.showFileWithPath(path: str)
                    }
                }
                
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
    
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64){
        
    }
    
    func showFileWithPath(path: String){
        
        let url = URL(string: path)
        let document = PDFDocument(url: url!)
        if document != nil
        {
            let readerController = PDFViewController.createNew(with: document!)
            readerController.navigationController?.isNavigationBarHidden = false
            APPDELEGATE.navController?.pushViewController(readerController, animated: true)
        }
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didCompleteWithError error: Error?){
        ServiceHelper.hideAllHuds(true, type: .default)
        downloadTask = nil
        if (error != nil) {
            
            print(error!.localizedDescription)
        }else{
            
            print("The task finished transferring data successfully")
        }
    }
    
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.section == 0
        {
            return 66
        }else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if section == 0
        {
            return titleArray.count
        }
        else if section == 1 {
            return 1
        }
        else  if section == 2{
            return viewProfileArray.count
        }
        else if section == 3 {
            return 1
        }
        else if section == 4{
            return experienceArray.count
        }
        else  if section == 5{
            return 1
        }
        else {
            return profSkillArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobOverviewTableViewCell", for: indexPath) as! JobOverviewTableViewCell
            
            let a = titleArray[indexPath.row]
            let b = getJobOverViewArray[indexPath.row]
            cell.titleLabel.text = a
            cell.getDataLabel.text = b
            
            cell.selectionStyle = .none
            
            return cell
        }
            
        else  if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EducationTableViewCell", for: indexPath) as! EducationTableViewCell
            cell.selectionStyle = .none
            
            return cell
        }
            
        else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EducationListTableViewCell", for: indexPath) as! EducationListTableViewCell
            let eduObj = viewProfileArray[indexPath.row]
            cell.eduDegreeLabel.text = eduObj.educationName
            cell.startDateLabel.text = eduObj.startYear
            cell.univsityLabel.text = eduObj.University
            cell.despLabel.text = eduObj.desp
            cell.selectionStyle = .none
            return cell
        }
            
        else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkandExpTableViewCell", for: indexPath) as! WorkandExpTableViewCell
            cell.selectionStyle = .none
            return cell
        }
            
        else if indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkNExpListTableViewCell", for: indexPath) as! WorkNExpListTableViewCell
            let expObj = experienceArray[indexPath.row]
            cell.designationLabel.text = expObj.designation
            cell.companyNameLabel.text = expObj.companyName
            cell.dateLabel.text = expObj.startDate + "-" + expObj.lastDate
            cell.descriptionLabel.text = expObj.desp
            cell.selectionStyle = .none
            
            return cell
        }
            
        else if indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfessionSkillsTableViewCell", for: indexPath) as! ProfessionSkillsTableViewCell
            cell.selectionStyle = .none
            return cell
        }
            
            
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "professionalSkillListTableViewCell", for: indexPath) as! professionalSkillListTableViewCell
            let proObj = profSkillArray[indexPath.row]
            cell.skillLabel.text = proObj.profSkill
            cell.descriptionLabel.text = proObj.desp
            cell.selectionStyle = .none
            return cell
        }
    }
    
    
    //MARK :- WEB API
    func callApiGetViewProfile()
    {
        var paramDict = Dictionary<String,Any>()
        let userid : String = NSUSERDEFAULT.value(forKey: kUserID) as! String
        paramDict["user_id"] = userid
        
        ServiceHelper.request(paramDict, method: .post, apiName: kViewProfile, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            self.viewProfileArray.removeAll()
                            self.experienceArray.removeAll()
                            self.profSkillArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data {
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                self.userNameLabel.text = (dict.validatedValue("user_name", expected: "" as AnyObject) as! String)
                                self.userEmailAddressLabel.text = (dict.validatedValue("email", expected: "" as AnyObject) as! String)
                                self.address = (dict.validatedValue("address", expected: "" as AnyObject) as! String)
                                self.userlocationLabel.text = self.address
                                if self.address == "/" {
                                    self.userlocationLabel.text = ""
                                }
                                
                                self.resumeUrl = dict.validatedValue("resume", expected: "" as AnyObject) as! String
                                var userImage : String = dict.validatedValue("profile", expected: "" as AnyObject) as! String
                                let _ = (userImage  == "") ? (userImage = "") : (userImage = userImage)
                                self.userProfileImageView.sd_setImage(with: URL(string: userImage), placeholderImage: UIImage(named:"user"), options: SDWebImageOptions(rawValue: 0))
                                self.getJobOverViewArray.append(dict.validatedValue("current_salary", expected: "" as AnyObject) as! String)
                                self.getJobOverViewArray.append(dict.validatedValue("expected_salary", expected: "" as AnyObject) as! String)
                                self.getJobOverViewArray.append(dict.validatedValue("designation", expected: "" as AnyObject) as! String)
                                self.getJobOverViewArray.append(dict.validatedValue("job_type", expected: "" as AnyObject) as! String)
                                self.getJobOverViewArray.append(dict.validatedValue("experience", expected: "" as AnyObject) as! String)
                                self.getJobOverViewArray.append(dict.validatedValue("qualification", expected: "" as AnyObject) as! String)
                                self.titleArray = ["Current Salary","Expected Salary","Designation","Job Type","Experience","Key Skill"]
                                
                                let education : Array = dict.validatedValue("education_details", expected: "" as AnyObject) as! Array<Any>
                                for j in education
                                {
                                    let eduObj = UserInfo()
                                    let dict : Dictionary = j as! Dictionary<String,AnyObject>
                                    eduObj.University = dict.validatedValue("university", expected: "" as AnyObject) as! String
                                    eduObj.startYear = dict.validatedValue("start_year", expected: "" as AnyObject) as! String
                                    eduObj.educationName = dict.validatedValue("education_name", expected: "" as AnyObject) as! String
                                    eduObj.desp = dict.validatedValue("description", expected: "" as AnyObject) as! String
                                    self.viewProfileArray.insert(eduObj, at: 0)
                                }

                                let Experience : Array = dict.validatedValue("work_exp", expected: "" as AnyObject) as! Array<Any>
                                for k in Experience
                                {
                                    let expObj = UserInfo()
                                    let dict : Dictionary = k as! Dictionary<String,AnyObject>
                                    expObj.designation = dict.validatedValue("designation", expected: "" as AnyObject) as! String
                                    expObj.companyName = dict.validatedValue("company", expected:"" as AnyObject) as! String
                                    expObj.desp = dict.validatedValue("description", expected:"" as AnyObject) as! String
                                    expObj.startDate = dict.validatedValue("start_date", expected: "" as AnyObject) as! String
                                    expObj.lastDate = dict.validatedValue("last_date" , expected: "" as AnyObject) as! String
                                    self.experienceArray.insert(expObj, at: 0)

                                }
                                let proSkill : Array = dict.validatedValue("pro_skill", expected: "" as AnyObject) as! Array<Any>
                                for l in proSkill
                                {
                                    let profObj = UserInfo()
                                    let dict : Dictionary = l as! Dictionary<String,AnyObject>
                                    profObj.profSkill = dict.validatedValue("pro_skill", expected: "" as AnyObject) as! String
                                    profObj.desp = dict.validatedValue("description", expected:"" as AnyObject) as! String
                                    self.profSkillArray.insert(profObj, at: 0)

                                }
                            }
                            self.viewProfileTableViewOutlet.reloadData()
                        }
                        else if status == false {
                            
                        }
                        
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
}



