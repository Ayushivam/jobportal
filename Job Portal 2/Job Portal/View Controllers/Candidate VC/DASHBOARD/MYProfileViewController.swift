//
//  MYProfileViewController.swift
//  Job Portal
//
//  Created by nile on 24/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class MYProfileViewController: UIViewController,UITextFieldDelegate,passValue,passLangValue,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    var picker = UIImagePickerController()
    @IBOutlet weak var subJobTypeBtn: UIButton!
    @IBOutlet weak var subJobTypeView: UIView!
    @IBOutlet weak var jobTypeBtn: UIButton!
    @IBOutlet weak var jobTypeView: UIView!
    @IBOutlet weak var jobtitleTextField: PaddedTextField!
    @IBOutlet weak var keySkillBtn: UIButton!
    @IBOutlet weak var keySkillView: UIView!
    @IBOutlet weak var highestEductnBtn: UIButton!
    @IBOutlet weak var highestEductnView: UIView!
    @IBOutlet weak var expMonthBtn: UIButton!
    @IBOutlet weak var expYearBtn: UIButton!
    @IBOutlet weak var expMonthView: UIView!
    @IBOutlet weak var expYearView: UIView!
    @IBOutlet weak var expectedSalaryTextField: PaddedTextField!
    @IBOutlet weak var currentSalaryTextField: PaddedTextField!
    @IBOutlet weak var personalInfoView: UIView!
    @IBOutlet weak var genderBtn: UIButton!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var lastNameTextField: PaddedTextField!
    @IBOutlet weak var firstNameTextField: PaddedTextField!
    @IBOutlet weak var countryBtn: UIButton!

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var EmailTextField: PaddedTextField!
    @IBOutlet weak var phoneNumTextField: PaddedTextField!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var userProfileView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var chooseFileBtn: UIButton!
    @IBOutlet weak var userProfileImageView: UIImageView!
    var genderArray = ["Male","Female","Any"]
    var languageNameArray = Array<String>()
    var languageIDArray = Array<String>()
    var countryArray = Array<String>()
    var countryIDArray = Array<String>()
    var stateArray = Array<String>()
    var stateIDArray = Array<String>()
    var cityArray = Array<String>()
    var cityIDArray = Array<String>()
    var highestEducationArray =  Array<String>()
    var highestEducationIDArray =  Array<String>()
    var JobTypeArray = Array<String>()
    var JobIDArray = Array<String>()
    var subJobTypeArray = Array<String>()
    var subJobIDArray = Array<String>()
    var skillsArray = Array<String>()
     var skillsString = String()
    var langString = ""
    var keySkillString = ""
    var languageArray = Array<String>()
    var langvalue = String()
    var keySkilssArray = Array<String>()
    var  isAlreadyUpdates = false
    ///////////////////***************//idString Veriable**************//////////////////////////

    var idKeySkillString = ""
    var idLanguageString = ""
    var keyLanguageIDArray = Array<String>()
    
    var keySkilssIDArray = Array<String>()

    var jobTypeIdString = ""
    var subJobTypeIdString = ""
    var highestEducationIdString = ""
    var countryIdString = ""
    var stateIdString = ""
    var cityIDString = ""
   
    //****************************//

    var expYearArray = ["0 Year","1 Year","2 Year","3 Year","4 Year","5 Year","6 Year","7 Year","8 Year","9 Year","10 Year","11 Year","12 Year","13 Year","14 Year","15 Year","16 Year","17 Year","18 Year","19 Year","20 Year","21 Year","22 Year","23 Year","24 Year","25 Year","26 Year","27 Year","28 Year","29 Year","30 Year","31 Year","32 Year","33 Year","34 Year","35 Year","36 Year","37 Year","38 Year","39 Year","40 Year","41 Year","42 Year","43 Year","44 Year","45 Year","46 Year","47 Year","48 Year","49 Year","50 Year"]
    var expMonthArray = ["0 Month","1 Month","2 Month","3 Month","4 Month","5 Month","6 Month","7 Month","8 Month","9 Month","10 Month","11 Month","12 Month"]

    override func viewDidLoad() {

        super.viewDidLoad()

        initialSetUp()

        callApiGetCandidateprofile()

        // Do any additional setup after loading the view.
    }
    
    
    func initialSetUp(){
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        topView.center = self.view.center
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOpacity = 5
        topView.layer.shadowOffset = CGSize.zero
        topView.layer.shadowRadius = 4
        
        self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.frame.size.width / 2;
        self.userProfileImageView.clipsToBounds = true
        self.userProfileImageView.layer.borderWidth = 1.0
        self.userProfileImageView.layer.borderColor = UIColor.black.cgColor
        userProfileView.layer.cornerRadius = 2.0
        personalInfoView.layer.cornerRadius = 2.0

        chooseFileBtn.layer.cornerRadius = 2.0
        uploadBtn.layer.cornerRadius = 2.0
        firstNameTextField.layer.borderWidth = 0.50
        firstNameTextField.layer.borderColor = UIColor.black.cgColor
        firstNameTextField.layer.cornerRadius = 1.0
        lastNameTextField.layer.borderWidth = 0.50
        lastNameTextField.layer.borderColor = UIColor.black.cgColor
        lastNameTextField.layer.cornerRadius = 1.0
        dateView.layer.borderWidth = 0.50
        dateView.layer.borderColor = UIColor.black.cgColor
        dateView.layer.cornerRadius = 1.0
        genderView.layer.borderWidth = 0.50
        genderView.layer.borderColor = UIColor.black.cgColor
        genderView.layer.cornerRadius = 1.0
        phoneNumTextField.layer.borderWidth = 0.50
        phoneNumTextField.layer.borderColor = UIColor.black.cgColor
        phoneNumTextField.layer.cornerRadius = 1.0
        EmailTextField.layer.borderWidth = 0.50
        EmailTextField.layer.borderColor = UIColor.black.cgColor
        EmailTextField.layer.cornerRadius = 1.0
        languageView.layer.borderWidth = 0.50
        languageView.layer.borderColor = UIColor.black.cgColor
        languageView.layer.cornerRadius = 1.0
        countryView.layer.borderWidth = 0.50
        countryView.layer.borderColor = UIColor.black.cgColor
        countryView.layer.cornerRadius = 1.0
        stateView.layer.borderWidth = 0.50
        stateView.layer.borderColor = UIColor.black.cgColor
        stateView.layer.cornerRadius = 1.0
        cityView.layer.borderWidth = 0.50
        cityView.layer.borderColor = UIColor.black.cgColor
        cityView.layer.cornerRadius = 1.0
        firstNameTextField.tag = 11
        lastNameTextField.tag = 12
        phoneNumTextField.tag = 13
        EmailTextField.tag = 14
        currentSalaryTextField.layer.borderWidth = 0.50
        currentSalaryTextField.layer.borderColor = UIColor.black.cgColor
        currentSalaryTextField.layer.cornerRadius = 1.0
        expectedSalaryTextField.layer.borderWidth = 0.50
        expectedSalaryTextField.layer.borderColor = UIColor.black.cgColor
        expectedSalaryTextField.layer.cornerRadius = 1.0
        expYearView.layer.borderWidth = 0.50
        expYearView.layer.borderColor = UIColor.black.cgColor
        expYearView.layer.cornerRadius = 1.0
        expMonthView.layer.borderWidth = 0.50
        expMonthView.layer.borderColor = UIColor.black.cgColor
        expMonthView.layer.cornerRadius = 1.0
        highestEductnView.layer.borderWidth = 0.50
        highestEductnView.layer.borderColor = UIColor.black.cgColor
        highestEductnView.layer.cornerRadius = 1.0
        currentSalaryTextField.tag = 15
        expectedSalaryTextField.tag = 16
        currentSalaryTextField.keyboardType = .numberPad
        expectedSalaryTextField.keyboardType = .numberPad
        keySkillView.layer.borderWidth = 0.50
        keySkillView.layer.borderColor = UIColor.black.cgColor
        keySkillView.layer.cornerRadius = 1.0
        jobtitleTextField.layer.borderWidth = 0.50
        jobtitleTextField.layer.borderColor = UIColor.black.cgColor
        jobtitleTextField.layer.cornerRadius = 1.0
        jobtitleTextField.tag = 16
        jobTypeView.layer.borderWidth = 0.50
        jobTypeView.layer.borderColor = UIColor.black.cgColor
        jobTypeView.layer.cornerRadius = 1.0
        subJobTypeView.layer.borderWidth = 0.50
        subJobTypeView.layer.borderColor = UIColor.black.cgColor
        subJobTypeView.layer.cornerRadius = 1.0
        EmailTextField.isUserInteractionEnabled = false
        for i in skillsArray
        {
            if skillsArray.count == 1
            {
                skillsString = i
            }else  {
                   skillsString =  i + "," +  skillsString
            }
        }
        
         for i in languageNameArray
        {
            if languageNameArray.count == 1
            {
                langString = i
            }else  {
                langString =  i + "," +  langString
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Button Action
    @IBAction func backBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func chooseFileBtnActn(_ sender: UIButton) {
        imageTapped()
    }
    
    @IBAction func uploadFileBtnActn(_ sender: UIButton) {
        
        if isAlreadyUpdates{
            AlertController.alert(title: "Please choose image.")
        }
      else if userProfileImageView.image == #imageLiteral(resourceName: "user") {
            AlertController.alert(title: "Please select profile image.")
        }
        else {
            callApiForUpdateProfileImage()
        }
        
    }
    //MARK:- Image Picker Functions
    func imageTapped(){
        
        self.view.endEditing(true)
        let alert = UIAlertController()
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ (UIAlertAction)in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerControllerSourceType.camera;
                self.picker.allowsEditing = false
                self.present(self.picker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Choose from gallery", style: .default , handler:{ (UIAlertAction)in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: {
                })
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: {
        })
    }

    //MARK Camera delegate
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            picker.dismiss(animated: true, completion: {() -> Void in
                self.userProfileImageView.image = image.fixOrientationAndResize()
                self.isAlreadyUpdates = false
            })
        }
    }

    @IBAction func commonBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        switch sender.tag {
        case 101:
            
            let newDate = Calendar.current.date(byAdding: .month, value: 0, to: Date())
            commonOptionPicker(button: dateBtn,date:newDate!)
            

            break
        case 102:
            commonOptionPicker(selectArray: genderArray,button: genderBtn)
            break
        case 103:
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KeySkillsViewController") as! KeySkillsViewController
            popOverVC.skilllangType = skillsLangEnumType.Language
            popOverVC.providesPresentationContextTransitionStyle = true
            popOverVC.definesPresentationContext = true
            popOverVC.delegate1 = self
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            popOverVC.langPreviousArray = self.languageNameArray
            popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
            self.present(popOverVC, animated: false, completion: nil)
         
            break
            
        case 104:
            commonOptionPicker(selectArray: countryArray,button: countryBtn)
            break
            
        case 105:
            if countryBtn.currentTitle == "Select Country" {
                AlertController.alert(title: "Please select country.")
            }
            else {
            commonOptionPicker(selectArray: stateArray,button: stateBtn)
            }
            break
            
        case 106:
            if stateBtn.currentTitle == "Select State" {
                AlertController.alert(title: "Please select state.")
            }
            else {
            commonOptionPicker(selectArray: cityArray,button: cityBtn)
            }
            break
       
        default:
            break
        }
    }

    @IBAction func commonBtnActionForProfInfo(_ sender: UIButton) {
        view.endEditing(true)
        switch sender.tag {

        case 107:
            commonOptionPicker(selectArray: expYearArray,button: expYearBtn)
            break
        case 108:
            commonOptionPicker(selectArray: expMonthArray,button: expMonthBtn)
            break
            
        case 109:
            commonOptionPicker(selectArray: highestEducationArray,button: highestEductnBtn)
            break
            
        case 110:
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KeySkillsViewController") as! KeySkillsViewController
            popOverVC.skilllangType = skillsLangEnumType.keySkill
            popOverVC.providesPresentationContextTransitionStyle = true
            popOverVC.definesPresentationContext = true
            popOverVC.delegate = self
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            popOverVC.previousValues = self.skillsArray
            popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
            self.present(popOverVC, animated: false, completion: nil)
            break

        default:
            break
        }
    }
    
    
      @IBAction func commonBtnActionForDesJob(_ sender: UIButton) {
        
        view.endEditing(true)
        switch sender.tag {
        case 111:
            commonOptionPicker(selectArray: JobTypeArray,button: jobTypeBtn)
            break
        case 112:
            if jobTypeBtn.currentTitle == "Select Job Type" {
                AlertController.alert(title: "Please select job type.")
            }
            else {
            commonOptionPicker(selectArray: subJobTypeArray,button: subJobTypeBtn)
            }
            break
        default:
            break
    }
    }

    @IBAction func saveBtnAction(_ sender: UIButton) {
        if isAllFieldVerified(){
        callApiForUpdateProfile()
        }
    }

    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){

        if selectArray.count == 0
        {
            
        }else
        {
        
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in

            if button == self.countryBtn{
                button.setTitle(string, for: .normal)
                let id = self.countryIDArray[i]
                self.countryIdString = id
                self.callApiGetState(countryid: id,isgetProfile: false,stateId:"",cityId: "")
            }
                
            else if button == self.stateBtn
            {
                button.setTitle(string, for: .normal)
                let id = self.stateIDArray[i]
                self.stateIdString = id

                self.callApiGetCity(stateId: id,isFromGetProfile: false,cityId: "")
            }
            else if button == self.jobTypeBtn{
                button.setTitle(string, for: .normal)
                 let id = self.JobIDArray[i]
                self.jobTypeIdString = id
                self.callApiGetSubJobType(subjobID: "",isfromGetProfile:false,jobId: id)
            }
            else if button == self.cityBtn{
                button.setTitle(string, for: .normal)
                let id = self.cityIDArray[i]
                self.cityIDString = id
            }
            else if button == self.highestEductnBtn{
                button.setTitle(string, for: .normal)
                let id = self.highestEducationIDArray[i]
                self.highestEducationIdString = id
            }
            else if button == self.subJobTypeBtn{
                button.setTitle(string, for: .normal)
                let id = self.subJobIDArray[i]
                self.subJobTypeIdString = id
            }
           
            else {
                button.setTitle(string, for: .normal)
            }
        }
        }
    }

    //MARK:- Delegate
    func  passKeySkills(valueArray:Array<String>,valueIDarray:Array<String>)
    {
        skillsString = ""
        skillsArray = valueArray
        idKeySkillString = ""
        keySkilssIDArray = valueIDarray
        
        for i in (0..<skillsArray.count)
        {
            let j = skillsArray[i]

            if skillsArray.count == 1
            {
                skillsString = j
            }else {
                let str = j
                if i == 0
                {
                skillsString = str
                }else {
                    skillsString = skillsString + "," + str
                }
            }
        }
        
        for i in (0..<keySkilssIDArray.count)
        {
            let j = keySkilssIDArray[i]
            
            if keySkilssIDArray.count == 1
            {
                idKeySkillString = j
            }else {
                let str = j
                if i == 0
                {
                    idKeySkillString = str
                }else {
                    idKeySkillString = idKeySkillString + "," + str
                }

            }
        }
        
        if keySkilssIDArray.count == 0
        {
            idKeySkillString = ""
        }
        

        self.keySkillBtn.setTitle(skillsString, for: .normal)
        if skillsArray.count == 0
        {
         self.keySkillBtn.setTitle("Select Key Skills", for: .normal)
        }
    }

    func passLang(langValueArray:Array<String>,langIdArray:Array<String>) {
        
        langString = ""
        languageNameArray = langValueArray
        idLanguageString = ""
        keyLanguageIDArray  = langIdArray
        
        
        for i in (0..<languageNameArray.count)
        {
            let j = languageNameArray[i]
            if languageNameArray.count == 1
            {
                langString = j
            }else {
                let str = j
                if i == 0 {
                    langString = str
                } else {
                langString =   langString + "," + str
            }
        }
        }
        
        for i in (0..<keyLanguageIDArray.count)
        {
            let j = keyLanguageIDArray[i]
            
            if keyLanguageIDArray.count == 1
            {
                idLanguageString = j
            }else {
                let str = j
                if i == 0
                {
                    idLanguageString = str
                }else {
                    idLanguageString = idLanguageString + "," + str
                }
            }
        }

        if keyLanguageIDArray.count == 0
        {
            idLanguageString = ""
        }

        self.languageBtn.setTitle(langString, for: .normal)
        if languageNameArray.count == 0
        {
            self.languageBtn.setTitle("Select Language", for: .normal)
        }
    }

    func commonOptionPicker(button: UIButton, date:Date) {
        
        RPicker.selectDate(datePickerMode: .date, selectedDate: date, minDate: nil, maxDate: date) {(date: Date) in
            let formattedDate = self.formattedDateFromString(dateString: "\(date)", withFormat: "dd-MM-yyyy")
            button.setTitle(formattedDate, for: .normal)
        }
    }

    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        if let date = inputFormatter.date(from: dateString) {
            
            
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    //MARK:- Validation
    func isAllFieldVerified()-> Bool{
        
        var isVerified = false
        if firstNameTextField.text == ""
        {
            isVerified = false
            AlertController.alert(title: "Please enter first name.")
        }
        else if lastNameTextField.text == ""
        {
            AlertController.alert(title: "Please enter last name.")
            isVerified = false

        }
        else if jobtitleTextField.text == ""
        {            isVerified = false

            AlertController.alert(title: "Please enter job title.")
            
        }
        else if jobTypeBtn.currentTitle == "Select Job Type"
        {
            isVerified = false

            AlertController.alert(title: "Please Select Job Type.")
        }
        else if subJobTypeBtn.currentTitle == "Select Sub Job Type"
        {
            isVerified = false

            AlertController.alert(title: "Please select sub job type.")
        }
            
        else if expYearBtn.currentTitle == "Select Year" {
            isVerified = false
            AlertController.alert(title: "Please select experience year.")
        }
            
        else if expMonthBtn.currentTitle == "Select Month" {
            isVerified = false
            AlertController.alert(title: "Please select experience month.")
        }
            
            
        else if dateBtn.currentTitle == "DD-MM-YYYY"
        {
            
            isVerified = false
            AlertController.alert(title: "Please select date of birth.")
            
        }
            
        else if genderBtn.currentTitle == "Select Gender"
        {

            isVerified = false
            AlertController.alert(title: "Please select gender.")
            
        }
            
        else if currentSalaryTextField.text == "" {
            isVerified = false
            AlertController.alert(title: "Please enter current salary.")
        }
            
        else if highestEductnBtn.currentTitle == "Select Education"
        {

            isVerified = false
            AlertController.alert(title: "Please select education.")
            
        }
//        else if keySkillBtn.currentTitle == "Select Key Skills"
//        {
//            isVerified = false
//            AlertController.alert(title: "Please Select Key Skills.")
//            
//        }
//        else if phoneNumTextField.text == ""
//        {
//            AlertController.alert(title: "Please enter phone number.")
//            isVerified = false
//
//        }
            
            
            
            
    
        else if languageBtn.currentTitle == "Select Language"
        {
            AlertController.alert(title: "Please select language.")
            isVerified = false

        }
        else if countryBtn.currentTitle == "Select Country"
        {
            AlertController.alert(title: "Please select country.")
            isVerified = false

        }else if stateBtn.currentTitle == "Select State"
        {
            AlertController.alert(title: "Please select state.")
            isVerified = false

        }else if cityBtn.currentTitle == "Select City"
        {
            AlertController.alert(title: "Please select city.")
            isVerified = false
        }
        else {
            isVerified = true
        }
        return isVerified
    }
    
    
    
    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        var str:NSString = textField.text! as NSString
//        str = str.replacingCharacters(in: range, with: string) as NSString
//        switch textField.tag {
//        case 100:
//            if str.length > 20
//            {
//                return false
//            }
//            break
//        case 101:
//            if str.length > 20
//            {
//                return false
//            }
//            
//            
//        case 102:
//            if str.length > 60
//            {
//                return false
//            }
//            
//            
//        case 103:
//            if str.length > 15
//            {
//                return false
//            }
//            
//            
//        case 104:
//            if str.length > 12
//            {
//                return false
//            }
//        default:
//            break
//        }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    
    
    //MARK:- Web Api
    
    func callApiGetSelectJobType(isGetprofile:Bool,jobID:String,subJobId:String)
    {
        let paramDict = Dictionary<String, Any>()
        ServiceHelper.request(paramDict, method: .get, apiName: kGetSelectJobType, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let jobArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        for i in jobArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let jobName : String = dict.validatedValue("job_type", expected: "" as AnyObject) as! String
                            self.JobTypeArray.append(jobName)
                            let jobid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.JobIDArray.append(jobid)
                        }
                        
                        if isGetprofile {
                            for  i in (0..<self.JobIDArray.count) {
                                let obj = self.JobIDArray[i]
                                if jobID == obj {
                                    self.jobTypeBtn.setTitle(self.JobTypeArray[i], for: .normal)
                                    self.callApiGetSubJobType(subjobID: subJobId,isfromGetProfile: true,jobId: jobID)
                                }
                            }
                        }
                    }
                    else
                    {
                      //  AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetSelectCountry(countryID:String)
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetCountry, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let countryArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in countryArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let countryName : String = dict.validatedValue("country_name", expected: "" as AnyObject) as! String
                            self.countryArray.append(countryName)
                            let countryid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.countryIDArray.append(countryid)
                        }
                        for j in (0..<self.countryIDArray.count) {
                            let obj = self.countryIDArray[j]
                            if countryID == obj {
                                self.countryBtn.setTitle(self.countryArray[j], for: .normal)
                            }
                        }
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    func callApiGetSelectEducation(eduId:String)
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetEducation, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                
                if let educationArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        for i in educationArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let educationName : String = dict.validatedValue("name", expected: "" as AnyObject) as! String
                            self.highestEducationArray.append(educationName)
                            let educationId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.highestEducationIDArray.append(educationId)
                        }
                        for  i in (0..<self.highestEducationIDArray.count) {
                            let obj = self.highestEducationIDArray[i]
                            if eduId == obj {
                                self.highestEductnBtn.setTitle(self.highestEducationArray[i], for: .normal)
                            }
                        }
                        
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetState(countryid : String,isgetProfile:Bool,stateId:String,cityId:String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["country_id"] = countryid

        ServiceHelper.request(paramDict, method: .post, apiName: KGetState, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let stateArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {

                        self.stateArray.removeAll()
                        self.stateIDArray.removeAll()
                        for i in stateArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let stateName : String = dict.validatedValue("state_name", expected: "" as AnyObject) as! String
                            self.stateArray.append(stateName)
                            let stateId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.stateIDArray.append(stateId)
                        }

                        if isgetProfile{

                            for j in (0..<self.stateIDArray.count) {
                                let obj = self.stateIDArray[j]
                                if  stateId == obj {
                                    self.stateBtn.setTitle(self.stateArray[j], for: .normal)
                                    self.callApiGetCity(stateId: stateId, isFromGetProfile: true,cityId:cityId)
                                }
                            }
                        }
                        else {
                            self.stateBtn.setTitle("Select State", for: .normal)
                            self.stateIdString = ""
                            self.cityBtn.setTitle("Select City", for: .normal)
                            self.cityIDString = ""
                        }
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetCity(stateId:String,isFromGetProfile:Bool,cityId:String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["state_id"] = stateId
    
        ServiceHelper.request(paramDict, method: .post, apiName: KGetCity, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let cityArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        self.cityArray.removeAll()
                        self.cityIDArray.removeAll()
                        for i in cityArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let cityName : String = dict.validatedValue("city_name", expected: "" as AnyObject) as! String
                            self.cityArray.append(cityName)
                            let cityId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.cityIDArray.append(cityId)
                        }
                        if isFromGetProfile
                        {
                            for j in (0..<self.cityIDArray.count) {
                                let obj = self.cityIDArray[j]
                                if  cityId == obj {
                                    self.cityBtn.setTitle(self.cityArray[j], for: .normal)
                                }
                            }
                        }else {
                            self.cityBtn.setTitle("Select City", for: .normal)
                            self.cityIDString = ""
                        }
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetSubJobType(subjobID: String,isfromGetProfile:Bool,jobId:String)
    {
        var paramDict = Dictionary<String,Any>()
        paramDict["job_sub_id"] = jobId
        ServiceHelper.request(paramDict, method: .post, apiName: kSelectSubJobType, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let subJobArray : Array = result as? Array<AnyObject>
                {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        self.subJobTypeArray.removeAll()
                        self.subJobIDArray.removeAll()
                        self.subJobTypeBtn.setTitle("Select Sub Job Type",for:.normal)
                        for i in subJobArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let jobSubType: String = dict.validatedValue("job_sub_type", expected: "" as AnyObject) as! String
                            self.subJobTypeArray.append(jobSubType)
                            let jobSubTypeID : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.subJobIDArray.append(jobSubTypeID)
                        }
                        
                        if isfromGetProfile{

                            for j in (0..<self.subJobIDArray.count) {
                                let obj = self.subJobIDArray[j]
                                if  subjobID == obj {
                                    self.subJobTypeBtn.setTitle(self.subJobTypeArray[j], for: .normal)
                                }
                            }
                        }
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
                
            }
            
        }
    }
    
    func callApiGetCandidateprofile()
    {
        var paramDict = Dictionary<String,Any>()
        let userid : String = NSUSERDEFAULT.value(forKey: kUserID) as! String
        paramDict["user_id"] = userid
        
        ServiceHelper.request(paramDict, method: .post, apiName: kGetCandidateProfile, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            let message : Array = response.validatedValue("message", expected: "" as AnyObject) as! Array<Any>
                            for i in message {
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                self.firstNameTextField.text! = (dict.validatedValue("f_name", expected: "" as AnyObject) as! String)
                                self.lastNameTextField.text! = (dict.validatedValue("l_name", expected: "" as AnyObject) as! String)
                                self.jobtitleTextField.text! = (dict.validatedValue("job_title", expected: "" as AnyObject) as! String)
                                self.EmailTextField.text! = (dict.validatedValue("email", expected: "" as AnyObject) as! String)
                                self.phoneNumTextField.text! = (dict.validatedValue("contact", expected: "" as AnyObject) as! String)
                                var userImage : String = dict.validatedValue("profile_pic", expected: "" as AnyObject) as! String
                                let _ = (userImage  == "") ? (userImage = "") : (userImage = userImage)
                                self.userProfileImageView.sd_setImage(with: URL(string: userImage), placeholderImage: UIImage(named:"user"), options: SDWebImageOptions(rawValue: 0))
                                let month = dict.validatedValue("exp_months", expected: "" as AnyObject) as! String
                                
                                if month == "" {
                                    self.expMonthBtn.setTitle("Select Month", for: .normal)

                                }
                                else {
                                    self.expMonthBtn.setTitle((dict.validatedValue("exp_months", expected: "" as AnyObject) as! String) + " Month", for: .normal)

                                    
                                }
                                
                                let year = dict.validatedValue("exp_years", expected: "" as AnyObject) as! String
                                
                                if year == "" {
                                    self.expYearBtn.setTitle("Select Year", for: .normal)
                                    
                                }else {
                                    self.expYearBtn.setTitle((dict.validatedValue("exp_years", expected: "" as AnyObject) as! String) + " Year", for: .normal)

                                  
                                }
                                
                                self.currentSalaryTextField.text! = (dict.validatedValue("cur_sal_min", expected: "" as AnyObject) as! String)
                                self.expectedSalaryTextField.text! = (dict.validatedValue("exp_sal_min", expected: "" as AnyObject) as! String)
                                let dob : String = dict.validatedValue("dob", expected: "" as AnyObject) as! String
                                
                                if dob == ""
                                {
                                   self.dateBtn.setTitle("DD-MM-YYYY", for: .normal)
                                }
                                
                                else {
                                    self.dateBtn.setTitle(dict.validatedValue("dob", expected: "" as AnyObject) as? String, for: .normal)

                                }
                                
                                let eduLevel : String = dict.validatedValue("edu_level", expected: "" as AnyObject) as! String
                                self.highestEducationIdString = eduLevel
                                self.callApiGetSelectEducation(eduId: eduLevel)
                                let jobID : String = dict.validatedValue("job_type_id", expected: "" as AnyObject) as! String
                                let subJobID : String = dict.validatedValue("sub_job_id", expected: "" as AnyObject) as! String
                                self.jobTypeIdString = jobID
                                self.subJobTypeIdString = subJobID

                                self.callApiGetSelectJobType(isGetprofile: true, jobID: jobID,subJobId: subJobID)
                                let gender : String = dict.validatedValue("gender", expected: "" as AnyObject) as! String
                                if gender == "1" {
                                    self.genderBtn.setTitle( "Male", for: .normal)
                                }
                                else if gender == "2" {
                                    self.genderBtn.setTitle( "Female", for: .normal)
                                }
                                    
                                else if gender == "3" {
                                    self.genderBtn.setTitle( "Any", for: .normal)
                                }
                                    
                                else {
                                    self.genderBtn.setTitle( "Select Gender", for: .normal)
                                }
                                
                                 ////****** Language Logic
                                let lang : Array = dict.validatedValue("languages", expected: "" as AnyObject) as! Array<Any>
                                for i in (0..<lang.count)
                                {
                                    let k = lang[i]
                                    let dict : Dictionary = k as! Dictionary<String,AnyObject>
                                    let languageValue : String = dict.validatedValue("language_name", expected: "" as AnyObject) as! String
                                    let languageID : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                  
                                    if lang.count == 1{
                                        self.idLanguageString = languageID
                                    }else {
                                        if i == lang.count-1
                                        {
                                            self.idLanguageString =   self.idLanguageString + languageID
                                        }else{
                                            self.idLanguageString = languageID + "," +  self.idLanguageString
                                        }
                                    }
                                     self.keyLanguageIDArray.append(languageID)
                                    self.languageArray.append(languageValue)
                                    for i in self.languageArray {
                                        if self.languageArray.count == 1 {
                                             self.langvalue = i
                                            self.languageNameArray.append(i)
                                            self.languageBtn.setTitle(self.langvalue, for: .normal)
                                        }
                                        else {
                                            let obj = i
                                            self.languageNameArray.append(obj)
                                            let abc : String = self.langvalue + "," + obj
                                            print(abc)
                                            self.languageBtn.setTitle(abc, for: .normal)
                                        }
                                    }
                                    if self.languageArray.count == 0{
                                        self.languageBtn.setTitle("Select Language", for: .normal)
                                    }
                                }
                                ////****** Skill Logic
                                let keySkill : Array = dict.validatedValue("key_skill", expected: "" as AnyObject) as! Array<Any>
                                for i in (0..<keySkill.count)
                                {
                                    let k = keySkill[i]
                                    
                                    let dict : Dictionary = k as! Dictionary<String,AnyObject>
                                    let Skill : String = dict.validatedValue("title", expected: "" as AnyObject) as! String
                                    let skillID : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                    if keySkill.count == 1{
                                        self.idKeySkillString = skillID
                                    }else {
                                        if i == keySkill.count-1
                                        {
                                             self.idKeySkillString =   self.idKeySkillString + skillID
                                        }else{
                                         self.idKeySkillString = skillID + "," +  self.idKeySkillString
                                        }
                                    }
                                    self.keySkilssIDArray.append(skillID)
                                    self.keySkilssArray.append(Skill)
                                    for j in self.keySkilssArray {
                                        if self.keySkilssArray.count == 1 {
                                            self.keySkillString = j
                                            self.skillsArray.append(j)
                                            self.keySkillBtn.setTitle(self.keySkillString, for: .normal)
                                        }
                                        else
                                        {
                                            let obj = j
                                            self.skillsArray.append(obj)
                                            let setSkillValue = self.keySkillString + "," + obj
                                            self.keySkillBtn.setTitle(setSkillValue, for: .normal)
                                        }
                                    }
                                    if self.keySkilssArray.count == 0 {
                                        self.keySkillBtn.setTitle("Select Key Skills", for: .normal)
                                    }
                                }

                                let countryId : String = dict.validatedValue("country_id", expected: "" as AnyObject) as! String
                                self.countryIdString = countryId
                                
                                self.callApiGetSelectCountry(countryID:countryId)
                                let stateId : String = dict.validatedValue("state_id", expected: "" as AnyObject) as! String
                                self.stateIdString = stateId

                                let cityID : String = dict.validatedValue("city_id", expected: "" as AnyObject) as! String
                                self.cityIDString = cityID
                                self.callApiGetState(countryid: countryId, isgetProfile: true, stateId: stateId,cityId: cityID)
                            }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    }

    func callApiForUpdateProfileImage()
    {
        var paramDict = Dictionary<String, Any>()
        let userid : String = NSUSERDEFAULT.value(forKey: kUserID) as! String
        paramDict["user_id"] = userid

        var imageData = Data()
        var isImage = false

        if self.userProfileImageView.image != nil{
            //imageData = UIImageJPEGRepresentation(self.userProfileImageView.image!, 0.1)!
            imageData = UIImagePNGRepresentation(self.userProfileImageView.image!)!
            isImage = true
        }
        else{
            isImage = false
        }
        ServiceHelper.updateUserInfo(hudType:.default,urlString: kUploadProfilePhoto,parameterDict:paramDict, imageData: imageData,haveImage:isImage,imageName:"pic",fileName:"",type:"png",  callback:
            { (result,error) -> Void in
                if result != nil {
                    let status : Bool = (result?.value(forKey: "status") as? Bool)!
                    if status == true
                    {
                        AlertController.alert(title:  (result?.value(forKey: "message") as! String))
                        self.isAlreadyUpdates = true
                    }
                    else if status == false {
                        AlertController.alert(title:  (result?.value(forKey: "message") as! String))
                    }
                }
                else{

                }
        })
    }

    //MARK :- WEB API
    func callApiForUpdateProfile()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["f_name"] = firstNameTextField.text!
        paramDict["l_name"] = lastNameTextField.text!
        paramDict["job_title"] = jobtitleTextField.text!
        paramDict["job_type_id"] = self.jobTypeIdString
        paramDict["sub_job_id"] = self.subJobTypeIdString
        paramDict["years"] = expYearBtn.currentTitle
        paramDict["months"] = expMonthBtn.currentTitle
        paramDict["birth"] = dateBtn.currentTitle
        let _ = (self.genderBtn.currentTitle == "Male") ? (paramDict["gender"] = "1") : (self.genderBtn.currentTitle == "Female") ? (paramDict["gender"] = "2") :  (self.genderBtn.currentTitle == "Any") ? (paramDict["gender"] = "3") :                          (paramDict["gender"] = "")
        paramDict["cur_sal_min"] = currentSalaryTextField.text!
        paramDict["exp_sal_min"] = expectedSalaryTextField.text!
        paramDict["edu_level"] = self.highestEducationIdString
        paramDict["lang"] = self.idLanguageString
        paramDict["contact"] = phoneNumTextField.text!
        paramDict["email"] = EmailTextField.text!
        paramDict["skill"] = self.idKeySkillString
        paramDict["country_id"] = self.countryIdString
        paramDict["state_id"] = self.stateIdString
        paramDict["city_id"] = self.cityIDString
        
        ServiceHelper.request(paramDict, method: .post, apiName:kUpdateCandidateProfile , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }

            else {
                if let response = result as? Dictionary<String, AnyObject> {

                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                         if status == true
                        {
                            
                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                               self.navigationController?.popViewController(animated: true)
                            })
                        }
                         else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String

                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
}
