//
//  MyResumeViewController.swift
//  Job Portal
//
//  Created by nile on 21/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
import MobileCoreServices
import PDFReader
class MyResumeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CreateDelegate,ProfessionalDelegate,WorknExpDelegate,UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate,UIDocumentInteractionControllerDelegate
{
    @IBOutlet weak var resumeImage: UIImageView!
    var educationgetArray = [UserInfo]()
    var ProfessionalSkillArray = [UserInfo]()
    var worknEducationArray = [UserInfo]()
    var fileUrl = ""
    
    @IBOutlet weak var chooseFileLBL: UILabel!
    var monthNamearray = ["Jan","Feb","March","April","May","June","July","Aug","Sept","Oct","Nov","Dec"]
    var monthArray = ["1","2","3","4","5","6","7","8","9","10","11","12"]
    var sectionTitleArray = ["Education","Work Experience","Professional Skills"]
    var titleLabelStrting = ["Add Education","Add Experience","Add Skills"]
    var lastMonthnYearValue = ""
    var startMonthValue = ""
    //Resume Outlets
    var resumeData = Data()
    @IBOutlet weak var viewResumeBtn: UIButton!
    @IBOutlet weak var uploadResumeBtn: UIButton!
    @IBOutlet weak var deleteResumeBtn: UIButton!
    var  monthID = ""
    @IBOutlet weak var myResumeTableView: UITableView!
    @IBOutlet weak var resumeNameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        callApiGetCandidateList()
        myResumeTableView.estimatedRowHeight = 110
        myResumeTableView.rowHeight = UITableViewAutomaticDimension
       // clearTempFolder()
        
        chooseFileLBL.isHidden = true
        deleteResumeBtn.isHidden = true
        viewResumeBtn.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- DELEGATE METHODS
    func passProfData() {
        callApiGetCandidateList()
        
    }
    
    func passData() {
        callApiGetCandidateList()
    }
    
    func passWorknExpData() {
        callApiGetCandidateList()
    }
    
    //MARK:- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }

    @IBAction func deleteResumeAction(_ sender: UIButton) {
        view.endEditing(true)
       
//        if resumeImage.image == nil {
//
//            AlertController.alert(title: "No Resume Found.")
//        }
//        else {
        
        
        
        let refreshAlert = UIAlertController(title: "", message: "Are you sure, you want to Delete Your Resume? ", preferredStyle: UIAlertControllerStyle.alert)
        
        
            refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
                refreshAlert .dismiss(animated: true, completion: nil)
                
            
        }))
        
            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                self.callApiForDeleteResume()
            
        }))
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func viewResumeAction(_ sender: UIButton) {
        
        view.endEditing(true)
        if self.fileUrl != ""
        {
        showFileWithPath(path: self.fileUrl)
        }
        else {
            AlertController.alert(title: "Please upload resume.")
        }
    }
    
    @IBAction func uploadResumeAction(_ sender: UIButton) {
        
        view.endEditing(true)
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    //MARK:- Document Picker
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        //        myURL = url
        //        print("import result : \(myURL)")
        if  url != nil {
            do {
                resumeData = try Data(contentsOf: url as URL)
                print(resumeData)
                self.callApiForUploadResume(url: url)
                // profileImageView.image = UIImage(data: imageData)
            }
            catch {
                print("Unable to load data: \(error)")
            }
        }

    }

    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- UI Table View datasource and Delageta
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let rectView = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 44)
        let rectLabelTitle = CGRect(x: 10, y:11 , width: 140, height: 20)
        let rectLabelButton = CGRect(x: tableView.frame.size.width-90, y:11 , width: 120, height: 20)
        let rectPlusButton = CGRect(x: tableView.frame.size.width-125, y:5 , width: 32, height: 32)
        let footerView = UIView(frame:rectView)
        let titleLabel = UILabel(frame: rectLabelTitle)
        let titleLabelBtn = UILabel(frame: rectLabelButton)
        let plusButton = UIButton(frame: rectPlusButton)
        
        titleLabel.textColor = UIColor(red: 47/255, green: 85/255, blue: 151/255, alpha: 1)
        titleLabel.font = UIFont(name: "RobotoCondensed-Bold", size: 16)
        titleLabel.text = self.sectionTitleArray[section]
        titleLabelBtn.text = self.titleLabelStrting[section]
        titleLabelBtn.textColor = UIColor(red: 96/255, green: 195/255, blue: 132/255, alpha: 1)

        titleLabelBtn.font = UIFont(name: "RobotoCondensed-Regular", size: 14)
        plusButton.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        plusButton.layer.cornerRadius = 16
        plusButton.tag = section+1000
        plusButton.backgroundColor = UIColor(red: 96/255, green: 195/255, blue: 132/255, alpha: 1)
        plusButton.clipsToBounds = true
        footerView.addSubview(titleLabel)
        footerView.addSubview(titleLabelBtn)
        footerView.addSubview(plusButton)
        footerView.backgroundColor = UIColor.white
        plusButton.addTarget(self, action: #selector(plusAction(sender:)), for: .touchUpInside)
        return footerView
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return educationgetArray.count
            
        }
        else if section == 1
        {
            return worknEducationArray.count
        }
        else {
            return ProfessionalSkillArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ResumeTableViewCell", for: indexPath) as! ResumeTableViewCell
            let obj = educationgetArray[indexPath.row]
            cell.educationLabel.text = obj.educationName
            cell.dateLabel.text = obj.startYear
            cell.universityNameLabel.text = obj.University
            cell.descriptionLabel.text = obj.desp
            cell.deleteButton.tag = indexPath.row+1500
            cell.deleteButton.addTarget(self, action: #selector(deleteBtnAction), for: .touchUpInside)
            cell.editButton.tag = indexPath.row+2500
            cell.editButton.addTarget(self, action: #selector(educationEditBtnAction), for: .touchUpInside)
            return cell
        }
            
        else if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "workandexpResumeTableViewCell", for: indexPath) as! workandexpResumeTableViewCell
            
            let obj = worknEducationArray[indexPath.row]
            
            cell.designationLabel.text = obj.designation
            cell.organization.text = obj.company
            if obj.isPresent  ==  "1" {
                cell.monthnTillLabell.text = self.startMonthValue + "/" + obj.startYear + "-" + obj.present
            }
            else  {
                cell.monthnTillLabell.text = self.startMonthValue + "/" + obj.startYear + "-" + self.lastMonthnYearValue
            }
            print(cell.monthnTillLabell)
            cell.descriptionLabel.text = obj.desp
            cell.deleteBtn.tag = indexPath.row+5500
            cell.editBtn.tag = indexPath.row+6500
            cell.deleteBtn.addTarget(self, action: #selector(workNExpdeleteBtnAction), for: .touchUpInside)
            cell.editBtn.addTarget(self, action: #selector(workNExpEditBtnAction), for: .touchUpInside)
            
            return cell
        }
            
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfessionalResumeTableViewCell", for: indexPath) as! ProfessionalResumeTableViewCell
            let obj = ProfessionalSkillArray[indexPath.row]
            cell.skillLabel.text = obj.name
            cell.descriptionLabel.text = obj.desp
            cell.deleteBtn.tag = indexPath.row+3500
            cell.deleteBtn.addTarget(self, action: #selector(ProfdeleteBtnAction), for: .touchUpInside)
            cell.editBtn.tag = indexPath.row+4500
            cell.editBtn.addTarget(self, action: #selector(ProfEditBtnAction), for: .touchUpInside)
            
            return cell
        }
    }
    

    func showFileWithPath(path: String){
    
        let url = URL(string: path)
        let document = PDFDocument(url: url!)
        if document != nil
        {
            let readerController = PDFViewController.createNew(with: document!)
            readerController.navigationController?.isNavigationBarHidden = false
            APPDELEGATE.navController?.pushViewController(readerController, animated: true)
        }
    }
 
    
        //MARK:- HEADER BUTTON ACTION
        @objc func plusAction(sender:UIButton)
        {
            let index = sender.tag-1000
            if index == 0{
                let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddEducationPOPViewController") as! AddEducationPOPViewController
                popOverVC.providesPresentationContextTransitionStyle = true
                popOverVC.delegate = self
                popOverVC.definesPresentationContext = true
                popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
                self.present(popOverVC, animated: false, completion: nil)
            }else if index == 1{
                let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WorknExpResumeViewController") as! WorknExpResumeViewController
                popOverVC.providesPresentationContextTransitionStyle = true
                popOverVC.delagate = self
                popOverVC.definesPresentationContext = true
                popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
                self.present(popOverVC, animated: false, completion: nil)
            }else {
                let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalViewController") as! ProfessionalViewController
                popOverVC.providesPresentationContextTransitionStyle = true
                popOverVC.delagate = self
                popOverVC.definesPresentationContext = true
                popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
                self.present(popOverVC, animated: false, completion: nil)
            }
            
        }
        
        
        
        //****************** EDUCATION BUTTON ******************\\
        @objc func educationEditBtnAction(sender:UIButton)
        {
            let index = sender.tag-2500
            let educationObj  = self.educationgetArray[index]
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddEducationPOPViewController") as! AddEducationPOPViewController
            popOverVC.isUpdated = true
            popOverVC.delegate = self
            popOverVC.obj = educationObj
            popOverVC.providesPresentationContextTransitionStyle = true
            popOverVC.definesPresentationContext = true
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
            self.present(popOverVC, animated: true, completion: nil)
        }
        
        @objc func deleteBtnAction(sender:UIButton)
        {
            
            AlertController.alert(title: "", message: "Are you sure, you want to remove?", buttons: ["No","Yes"]) { (alert, index) in
                if index == 1{
                    let index = sender.tag-1500
                    
                    let obj = self.educationgetArray[index]
                    let eduid = obj.id
                    self.callApiForDeleteEducation(eduID: eduid, index: index)
                }
            }
        }
        //****************** PROFESSIONAL BUTTON ******************\\
        
        @objc func ProfdeleteBtnAction(sender:UIButton) {
            AlertController.alert(title: "", message: "Are you sure, you want to remove?", buttons: ["No","Yes"]) { (alert, index) in
                if index == 1{
                    let index = sender.tag-3500
                    
                    let obj = self.ProfessionalSkillArray[index]
                    let proID = obj.id
                    self.callApiForDeleteProfessional(id: proID, index: index)
                }
            }
        }
        
        
        @objc func ProfEditBtnAction(sender:UIButton) {
            
            let index = sender.tag-4500
            let ProfObj  = self.ProfessionalSkillArray[index]
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfessionalViewController") as! ProfessionalViewController
            popOverVC.isUpdated = true
            popOverVC.delagate = self
            popOverVC.proObj = ProfObj
            popOverVC.providesPresentationContextTransitionStyle = true
            popOverVC.definesPresentationContext = true
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
            self.present(popOverVC, animated: true, completion: nil)
            
        }
        
        //****************** WORK N EXP BUTTON ******************\\
        
        @objc func workNExpdeleteBtnAction(sender:UIButton) {
            AlertController.alert(title: "", message: "Are you sure, you want to remove?", buttons: ["No","Yes"]) { (alert, index) in
                if index == 1{
                    let index = sender.tag-5500
                    let obj = self.worknEducationArray[index]
                    let expID = obj.id
                    self.callApiForDeleteExp(expid: expID, index: index)
                }
            }
        }
        
        @objc func workNExpEditBtnAction(sender:UIButton)  {
            
            
            let index = sender.tag-6500
            let workObj  = self.worknEducationArray[index]
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WorknExpResumeViewController") as! WorknExpResumeViewController
            popOverVC.isUpdated = true
            popOverVC.delagate = self
            popOverVC.WorknExpObj = workObj
            popOverVC.providesPresentationContextTransitionStyle = true
            popOverVC.definesPresentationContext = true
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
            self.present(popOverVC, animated: true, completion: nil)
        }
        //MARK:- WEB API
        func callApiGetCandidateList()
        {
            var paramDict = Dictionary<String, Any>()
            paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
            ServiceHelper.request(paramDict, method: .post, apiName:kGetCandidateEducation , hudType: .default) { (result, error, code) in
                if let error = error {
                    AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
                }
                else {
                    if let response = result as? Dictionary<String, AnyObject> {
                        let responseCode = code
                        if (Int(responseCode) == 200) {
                            let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                            
                            if status == false {
                                self.educationgetArray.removeAll()
                                self.myResumeTableView.reloadData()
                                
                                //   self.noDataFoundLabel.isHidden = false
                            }
                            else if status == true
                            {
                                self.educationgetArray.removeAll()
                                let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                                for i in data
                                {
                                    let userObj = UserInfo()
                                    let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                    userObj.id = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                    userObj.candidate_id = dict.validatedValue("candidate_id", expected: "" as AnyObject) as! String
                                    userObj.education = dict.validatedValue("educations", expected: "" as AnyObject) as! String
                                    userObj.courses = dict.validatedValue("course_id", expected: "" as AnyObject) as! String
                                    userObj.University = dict.validatedValue("university", expected: "" as AnyObject) as! String
                                    userObj.startYear = dict.validatedValue("start_year", expected: "" as AnyObject) as! String
                                    userObj.desp = dict.validatedValue("description", expected: "" as AnyObject) as! String
                                    userObj.educationID = dict.validatedValue("e_id", expected: "" as AnyObject) as! String
                                    userObj.educationName = dict.validatedValue("education_name", expected: "" as AnyObject) as! String
                                    self.educationgetArray.insert(userObj, at: 0)
                                }
                                self.worknEducationArray.removeAll()
                                let work : Array = response.validatedValue("work_experiance", expected: "" as AnyObject) as! Array<Any>
                                for j in work
                                {
                                    let userObj = UserInfo()
                                    let dict : Dictionary = j as! Dictionary<String,AnyObject>
                                    userObj.id = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                    userObj.candidate_id = dict.validatedValue("candidate_id", expected: "" as AnyObject) as! String
                                    userObj.designation = dict.validatedValue("designation", expected: "" as AnyObject) as! String
                                    userObj.company = dict.validatedValue("company", expected: "" as AnyObject) as! String
                                    userObj.isPresent = dict.validatedValue("is_present", expected: "" as AnyObject) as! String
                                    userObj.present = dict.validatedValue("present", expected: "" as AnyObject) as! String
                                    userObj.startYear = dict.validatedValue("start_year", expected: "" as AnyObject) as! String
                                    userObj.startMonth = dict.validatedValue("start_month", expected: "" as AnyObject) as! String
                                    self.monthID = userObj.startMonth
                                    for i in (0..<self.monthArray.count)
                                    {
                                        let id  = self.monthArray[i]
                                        if id == self.monthID
                                        {
                                            let val = self.monthNamearray[i]
                                            self.startMonthValue = val
                                        }
                                    }
                                    userObj.lastYear = dict.validatedValue("last_year", expected: "" as AnyObject) as! String
                                    let lastYearValue = userObj.lastYear
                                    userObj.lastMonth = dict.validatedValue("last_month", expected: "" as AnyObject) as! String
                                    self.monthID = userObj.lastMonth
                                    for j in (0..<self.monthArray.count)
                                    {
                                        let id = self.monthArray[j]
                                        if id == self.monthID
                                            
                                        {
                                            let val = self.monthNamearray[j]
                                            self.lastMonthnYearValue = val + "/" + lastYearValue
                                        }
                                    }
                                    userObj.desp = dict.validatedValue("description", expected: "" as AnyObject) as! String
                                    self .worknEducationArray.insert(userObj, at: 0)
                                }
                                self.ProfessionalSkillArray.removeAll()
                                
                                let profSkill : Array = response.validatedValue("pro_details", expected: "" as AnyObject) as! Array<Any>
                                
                                for k in profSkill
                                {
                                    let userObj = UserInfo()
                                    let dict :Dictionary = k as! Dictionary<String,AnyObject>
                                    userObj.id = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                    userObj.profSkill = dict.validatedValue("pro_skill", expected: "" as AnyObject) as! String
                                    userObj.name = dict.validatedValue("name", expected: "" as AnyObject) as! String
                                    userObj.desp = dict.validatedValue("description", expected: "" as AnyObject) as! String
                                    self.ProfessionalSkillArray.insert(userObj, at: 0)
                                }
                                
                                self.resumeNameLabel.text = response.validatedValue("resume_name", expected: "" as AnyObject) as? String
                                
                                
                                let path = response.validatedValue("resume", expected: "" as AnyObject) as! String
                                self.fileUrl = path
                                if path != ""
                                {
                                    self.chooseFileLBL.isHidden = true
                                    self.deleteResumeBtn.isHidden = false
                                    self.viewResumeBtn.isHidden = false

                                    self.resumeImage.image = UIImage(named: "pdfR")
                                    //self.dowloadAction(resumeurl: path)
                                }
                                else {
                                    self.chooseFileLBL.isHidden = false
                                    self.deleteResumeBtn.isHidden = true
                                    self.viewResumeBtn.isHidden = true

                                }
                                
                                self.myResumeTableView.reloadData()
                            }
                        } else{
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }
                }
            }
        }
        
        func callApiForDeleteEducation(eduID : String , index : Int)
        {
            var paramDict = Dictionary<String, Any>()
            paramDict["edu_id"] = eduID
            
            ServiceHelper.request(paramDict, method: .post, apiName: kDeleteEducation, hudType: .default) { (result, error, code) in
                if let error = error {
                    AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
                }
                else {
                    if let response = result as? Dictionary<String, AnyObject> {
                        let responseCode = code
                        if (Int(responseCode) == 200) {
                            let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                            if status == true{
                                self.educationgetArray.remove(at: index)
                                self.myResumeTableView.reloadData()
                                let msg : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                                self.view.makeToast(msg)
                            }

                            
                        }
                        
                    }
                }
            }
        }
        
        func callApiForDeleteProfessional(id : String , index : Int)
        {
            var paramDict = Dictionary<String, Any>()
            paramDict["id"] = id
            
            
            ServiceHelper.request(paramDict, method: .post, apiName: kDeleteProfessional, hudType: .default) { (result, error, code) in
                if let error = error {
                    AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
                }
                else {
                    if let response = result as? Dictionary<String, AnyObject> {
                        let responseCode = code
                        if (Int(responseCode) == 200) {
                            let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                            if status == true{
                                self.ProfessionalSkillArray.remove(at: index)
                                self.myResumeTableView.reloadData()
                                let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                                self.view.makeToast(MSG)
                            }
                         
                          
                            
                        }
                        
                    }
                }
            }
        }
        
        func callApiForDeleteExp(expid : String , index : Int)
        {
            var paramDict = Dictionary<String, Any>()
            paramDict["id"] = expid
            
            
            ServiceHelper.request(paramDict, method: .post, apiName: kDeleteExp, hudType: .default) { (result, error, code) in
                if let error = error {
                    AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
                }
                else {
                    if let response = result as? Dictionary<String, AnyObject> {
                        let responseCode = code
                        if (Int(responseCode) == 200) {
                            let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                            if status == true{
                                self.worknEducationArray.remove(at: index)
                                self.myResumeTableView.reloadData()
                                let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                                self.view.makeToast(MSG)
                            }
                          
                        }
                        
                    }
                }
            }
        }

    func callApiForUploadResume(url:URL)
        {
            var dict : Dictionary = Dictionary<String,AnyObject>()
            dict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID) as AnyObject
            let nme : String = "\(url)"
            let arr = nme.components(separatedBy: "/")
            let fileName : String = arr.last!

            ServiceHelper.updateUserInfo(hudType:.default,urlString: "candidate_resume", parameterDict: dict, imageData: resumeData, haveImage: true, imageName: "resume",fileName:fileName,type:"pdf") { (result, error) in
               
                if error != nil{
                    AlertController.alert(title: "\(String(describing: error))")
                    return
                }
                let status : Bool = result!["status"] as! Bool
                if status
                {
                    self.fileUrl = "\(url)"
                    self.resumeImage.image = UIImage(named: "pdfR")
                   
                    self.resumeNameLabel.text = fileName
                    self.chooseFileLBL.isHidden = true
                    self.deleteResumeBtn.isHidden = false
                    self.viewResumeBtn.isHidden = false

                  //  self.thumbnailFromPdf(withUrl: url)
                }
            }
        }

    func callApiForDeleteResume()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        
        ServiceHelper.request(paramDict, method: .post, apiName: kDeleteResume, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            self.fileUrl = ""
                            self.chooseFileLBL.isHidden = false
                            self.deleteResumeBtn.isHidden = true
                            self.viewResumeBtn.isHidden = true
                            self.resumeImage.image = nil
                            self.resumeNameLabel.text = ""
                           // self.clearTempFolder()
                        }
                        
//                        let message : String = response.validatedValue("message", expected: "" as AnyObject) as! String
//                        self.view.makeToast(message)
                        
                    }
                }
            }
        }
    }
}
