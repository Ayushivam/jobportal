//
//  JobsViewController.swift
//  Job Portal
//
//  Created by nile on 23/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
import CarbonKit

class JobsViewController: UIViewController,CarbonTabSwipeNavigationDelegate {

    @IBOutlet weak var containerView: UIView!
    var carbonTabSwipeNavigation =  CarbonTabSwipeNavigation()

    override func viewDidLoad() {
        super.viewDidLoad()
        let item = ["JOB ALERTS", "MY JOBS", "APPLIED JOBS"]
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: item, delegate: self)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(kWindowWidth/3, forSegmentAt: 0)

        carbonTabSwipeNavigation.setNormalColor(UIColor.black, font: UIFont(name: "RobotoCondensed-Regular", size: 16.0)!)

        carbonTabSwipeNavigation.setSelectedColor(UIColor(red: 47/255, green: 85/255, blue: 151/255, alpha: 1),font: UIFont(name: "RobotoCondensed-Regular", size: 16.0)!)

        carbonTabSwipeNavigation.setIndicatorHeight(2)
        carbonTabSwipeNavigation.setIndicatorColor(UIColor(red: 47/255, green: 85/255, blue: 151/255, alpha: 1))

        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(kWindowWidth/3, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(kWindowWidth/3, forSegmentAt: 2)
        carbonTabSwipeNavigation.setTabBarHeight(50)

        carbonTabSwipeNavigation.insert(intoRootViewController:self, andTargetView:containerView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
            
            let baseStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

        switch index {
            case 0:
                let firstViewController = baseStoryboard.instantiateViewController(withIdentifier: "JobAlertViewController") as? JobAlertViewController
                return firstViewController!
            case 1:
                let secondViewController = baseStoryboard.instantiateViewController(withIdentifier: "MyJobsViewController") as? MyJobsViewController
                
                return secondViewController!
            default:
                let thirdViewController = baseStoryboard.instantiateViewController(withIdentifier: "AppliedJobsViewController") as? AppliedJobsViewController
                return thirdViewController!
            }
    }

}
