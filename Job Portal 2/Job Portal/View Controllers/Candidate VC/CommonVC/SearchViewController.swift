//
//  SearchViewController.swift
//  Job Portal
//
//  Created by nile on 23/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var subJobTypeBtn: UIButton!
    @IBOutlet weak var subJobTypeView: UIView!
    @IBOutlet weak var jobTypeBtn: UIButton!
    @IBOutlet weak var jobTypeView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var jobKeywordTextField: PaddedTextField!
    var countryArray = Array<String>()
    var countryIDArray = Array<String>()
    var stateArray = Array<String>()
    var stateIDArray = Array<String>()
    var cityArray = Array<String>()
    var cityIDArray = Array<String>()
    var JobTypeArray = Array<String>()
    var JobIDArray = Array<String>()
    var subJobTypeArray = Array<String>()
    var subJobIDArray = Array<String>()
    var countryIdString = ""
    var stateIdString = ""
    var cityIDString = ""
    var jobTypeIdString = ""
    var subJobTypeIdString = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        callApiGetSelectJobType()
        callApiGetSelectCountry()
        mainView.backgroundColor = UIColor.white
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 3
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 2
        mainView.layer.cornerRadius = 2.0
        
        jobKeywordTextField.layer.borderWidth = 0.50
        jobKeywordTextField.layer.borderColor = UIColor.black.cgColor
        jobKeywordTextField.layer.cornerRadius = 2.0
        jobKeywordTextField.returnKeyType = .done

        countryView.layer.borderWidth = 0.50
        countryView.layer.borderColor = UIColor.black.cgColor
        countryView.layer.cornerRadius = 2.0
        stateView.layer.borderWidth = 0.50
        stateView.layer.borderColor = UIColor.black.cgColor
        stateView.layer.cornerRadius = 2.0
        cityView.layer.borderWidth = 0.50
        cityView.layer.borderColor = UIColor.black.cgColor
        cityView.layer.cornerRadius = 2.0
        
        jobTypeView.layer.borderWidth = 0.50
        jobTypeView.layer.borderColor = UIColor.black.cgColor
        jobTypeView.layer.cornerRadius = 2.0
        subJobTypeView.layer.borderWidth = 0.50
        subJobTypeView.layer.borderColor = UIColor.black.cgColor
        subJobTypeView.layer.cornerRadius = 2.0
        submitBtn.layer.cornerRadius = 5.0
        resetBtn.layer.cornerRadius = 5.0
         jobKeywordTextField.tag = 100
        jobKeywordTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK :- Button Action
    
    
    @IBAction func commonBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        switch sender.tag {
        case 10:
            
            commonOptionPicker(selectArray: JobTypeArray, button: jobTypeBtn)
            break
            
        case 11:
            
            if jobTypeBtn.currentTitle == "Select Category" {
                AlertController.alert(title: "Please select category.")
            }
            else {
            commonOptionPicker(selectArray: subJobTypeArray, button: subJobTypeBtn)
            }
            break
            
        case 12:
            
           
            commonOptionPicker(selectArray: countryArray, button: countryBtn)
            
            break
            
        case 13:
            
            if countryBtn.currentTitle == "Select Country" {
                AlertController.alert(title: "Please select country.")
            }
            else {
                commonOptionPicker(selectArray: stateArray,button: stateBtn)
            }
            break
            
        case 14:
            if stateBtn.currentTitle == "Select State" {
                AlertController.alert(title: "Please select state.")
            }
            else {
                commonOptionPicker(selectArray: cityArray,button: cityBtn)
            }
            break
            
        case 15:
            
            view.endEditing(true)
            if jobKeywordTextField.text?.count == 0
            {
                AlertController.alert(title: "Please enter job keyword.")
            }

            else {
                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                let newViewController = storyboard.instantiateViewController(withIdentifier: "SearchResultViewController") as! SearchResultViewController
                newViewController.jobKeyValue = jobKeywordTextField.text!
                newViewController.countryID = self.countryIdString
                newViewController.stateID = self.stateIdString
                newViewController.cityID = self.cityIDString
                newViewController.jobTypeID = self.jobTypeIdString
                newViewController.subJobTypeID = self.subJobTypeIdString
                APPDELEGATE.navController?.pushViewController(newViewController, animated: true)
            }
            break
            
            
        case 16:
            view.endEditing(true)
             jobKeywordTextField.text = ""
            
            
            self.jobTypeBtn.setTitle("Select Category", for: .normal)
            self.subJobTypeBtn.setTitle("Select sub Type", for: .normal)
            
            self.countryBtn.setTitle("Select Country", for: .normal)
            
            self.stateBtn.setTitle("Select State", for: .normal)
            self.cityBtn.setTitle("Select City", for: .normal)
        default:
           break
        }
    }
    
   
    
   // MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString

        switch textField.tag {
        case 100:

            if str.length > 20 {
                return false
            }
            break
        default:
            break
        }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    

    
    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){
        if selectArray.count == 0
        {
            
        }else
        {
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
            
            if button == self.countryBtn{
                button.setTitle(string, for: .normal)
                let id = self.countryIDArray[i]
                self.countryIdString = id
                self.callApiGetState(countryid: id)
            }
                
            else if button == self.stateBtn
            {
                button.setTitle(string, for: .normal)
                let id = self.stateIDArray[i]
                self.stateIdString = id
                self.callApiGetCity(cityId: id)
            }
                
            else if button == self.cityBtn{
                button.setTitle(string, for: .normal)
                let id = self.cityIDArray[i]
                self.cityIDString = id
            }
                
            else if button == self.jobTypeBtn{
                button.setTitle(string, for: .normal)
                let id = self.JobIDArray[i]
                self.jobTypeIdString = id
                self.callApiGetSubJobType(jobID: id)
            }
            else if button == self.subJobTypeBtn{
                button.setTitle(string, for: .normal)
                let id = self.subJobIDArray[i]
                self.subJobTypeIdString = id
            }
                
          
                
            else {
                button.setTitle(string, for: .normal)
            }
        }
        }
    }
    
    
    //MARK:- WEB API
    func callApiGetSelectJobType()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetSelectJobType, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                
                if let jobArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in jobArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let jobName : String = dict.validatedValue("job_type", expected: "" as AnyObject) as! String
                            self.JobTypeArray.append(jobName)
                            let jobid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.JobIDArray.append(jobid)
                        }
                        
                    }
                    else
                    {
                        //  AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    func callApiGetSubJobType(jobID: String)
    {
        var paramDict = Dictionary<String,Any>()
        paramDict["job_sub_id"] = jobID
        ServiceHelper.request(paramDict, method: .post, apiName: kSelectSubJobType, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let subJobArray : Array = result as? Array<AnyObject>
                {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                
                        self.subJobTypeBtn.setTitle("Select Sub Type", for: .normal)
                        self.subJobTypeArray.removeAll()
                        self.subJobIDArray.removeAll()
                        for i in subJobArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let jobSubType: String = dict.validatedValue("job_sub_type", expected: "" as AnyObject) as! String
                            self.subJobTypeArray.append(jobSubType)
                            let jobSubTypeID : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.subJobIDArray.append(jobSubTypeID)
                        }
                        
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
                
            }
            
        }
    }
    
    func callApiGetSelectCountry()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetCountry, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                
                
                if let countryArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in countryArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let countryName : String = dict.validatedValue("country_name", expected: "" as AnyObject) as! String
                            self.countryArray.append(countryName)
                            let countryid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.countryIDArray.append(countryid)
                            
                        }
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    
    }
    
    func callApiGetState(countryid : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["country_id"] = countryid
        
        ServiceHelper.request(paramDict, method: .post, apiName: KGetState, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let stateArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        self.stateBtn.setTitle("Select State", for: .normal)
                        self.cityBtn.setTitle("Select City", for: .normal)

                        self.stateArray.removeAll()
                        self.stateIDArray.removeAll()
                        for i in stateArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let stateName : String = dict.validatedValue("state_name", expected: "" as AnyObject) as! String
                            self.stateArray.append(stateName)
                            let stateId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.stateIDArray.append(stateId)
                            
                        }
                        
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetCity(cityId:String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["state_id"] = cityId
        
        ServiceHelper.request(paramDict, method: .post, apiName: KGetCity, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let cityArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        self.cityBtn.setTitle("Select City", for: .normal)

                        self.cityArray.removeAll()
                        self.cityIDArray.removeAll()
                        for i in cityArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let cityName : String = dict.validatedValue("city_name", expected: "" as AnyObject) as! String
                            self.cityArray.append(cityName)
                            let cityId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.cityIDArray.append(cityId)
                        }
                        
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }


  
}
