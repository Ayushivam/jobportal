//
//  DashboardViewController.swift
//  Job Portal
//
//  Created by nile on 23/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var dashboardCollectionViewOutlet: UICollectionView!
    var titleItem = ["My Profile","Applied Job","My Resume","View Profile","Featured Jobs","Featured Employers"]
    var updatedItem = ["View Profile","Applications","Create New Resume","","",""]
    
    
    var imageicon = [#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "bag"),#imageLiteral(resourceName: "myrsume"),#imageLiteral(resourceName: "eye"),#imageLiteral(resourceName: "fj"),#imageLiteral(resourceName: "fe")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callApiForDashboardData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCount(_:)), name: NSNotification.Name(rawValue: "updateCount"), object: nil)
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    //notification selector
    @objc func updateCount(_ notification: NSNotification) {
        callApiForDashboardData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // callApiForDashboardData()
    }
    deinit
    {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "updateCount"), object: nil)
        
    }
    
    //MARK:- Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return titleItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellsAcross: CGFloat = 2
        var widthRemainingForCellContent = collectionView.bounds.width
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            let borderSize: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
            widthRemainingForCellContent -= borderSize + ((cellsAcross - 1) * flowLayout.minimumInteritemSpacing)
        }
        let cellWidth = widthRemainingForCellContent / cellsAcross
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBoardCollectionViewCell", for: indexPath) as! DashBoardCollectionViewCell
        cell.tittleLabel.text = titleItem[indexPath.row]
        cell.upddatedTitleLabel.text = updatedItem[indexPath.row]
        cell.commonImageView.image = imageicon[indexPath.row]
        cell.commonButton.tag = indexPath.row+100
        cell.commonButton.addTarget(self, action: #selector(centerAction), for: .touchUpInside)
        return cell
    }
    
    //MARK :- Button Action For Collection View
    @objc func centerAction(_sender:UIButton) {
        
        let tag = _sender.tag-100
        if tag == 0{
            if let _ = NSUSERDEFAULT.value(forKey: kUserID)
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newIncomeVC = storyBoard.instantiateViewController(withIdentifier: "MYProfileViewController") as! MYProfileViewController
                APPDELEGATE.navController?.pushViewController(newIncomeVC, animated: true)
            }else {
                AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                    if index == 1{
                        self.navigationController!.viewControllers.removeAll()
                        APPDELEGATE.goToLogin()
                        
                    }
                }
            }
        }
        else if tag == 1{
            if let _ = NSUSERDEFAULT.value(forKey: kUserID)
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newIncomeVC = storyBoard.instantiateViewController(withIdentifier: "AppliedJobsDashBoradViewController") as! AppliedJobsDashBoradViewController
                APPDELEGATE.navController?.pushViewController(newIncomeVC, animated: true)
            }else {
                
                
                AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                    if index == 1{
                        self.navigationController!.viewControllers.removeAll()
                        APPDELEGATE.goToLogin()
                    }
                }
            }
        }
            
        else if tag == 2{
            
            if let _ = NSUSERDEFAULT.value(forKey: kUserID)
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newIncomeVC = storyBoard.instantiateViewController(withIdentifier: "MyResumeViewController") as! MyResumeViewController
                APPDELEGATE.navController?.pushViewController(newIncomeVC, animated: true)
            }else {
                AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                    if index == 1{
                        self.navigationController!.viewControllers.removeAll()
                        APPDELEGATE.goToLogin()
                    }
                }            }
        }
        
        if tag == 3{
            if let _ = NSUSERDEFAULT.value(forKey: kUserID)
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newIncomeVC = storyBoard.instantiateViewController(withIdentifier: "ViewProfileViewController") as! ViewProfileViewController
                APPDELEGATE.navController?.pushViewController(newIncomeVC, animated: true)
            }else {
                AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                    if index == 1{
                        self.navigationController!.viewControllers.removeAll()
                        APPDELEGATE.goToLogin()
                    }
                }            }
        }
            
        else  if tag == 4{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newIncomeVC = storyBoard.instantiateViewController(withIdentifier: "FeauturedJobViewController") as! FeauturedJobViewController
            APPDELEGATE.navController?.pushViewController(newIncomeVC, animated: true)
        }
            
        else  if tag == 5{
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newIncomeVC = storyBoard.instantiateViewController(withIdentifier: "FeauturedEmployeViewController") as! FeauturedEmployeViewController
            APPDELEGATE.navController?.pushViewController(newIncomeVC, animated: true)
        }
    }
    
    func callApiForDashboardData()
    {
        var paramDict = Dictionary<String, Any>()
        
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        
        ServiceHelper.request(paramDict, method: .post, apiName: kDashboard, hudType: .smoothProgress) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            let appliedDict : Dictionary = response.validatedValue("applied_job", expected: "" as AnyObject) as! Dictionary<String,AnyObject>
                            self.updatedItem[1] = appliedDict["total_applied"] as! String + " Applications"
                            
                            self.dashboardCollectionViewOutlet.reloadData()
                        }
                        else {
                            
                        }
                    }
                }
            }
        }
    }
    
    
    
    
}
