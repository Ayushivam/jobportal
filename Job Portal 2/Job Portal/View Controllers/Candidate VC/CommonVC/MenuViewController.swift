//
//  MenuViewController.swift
//  Job Portal
//
//  Created by nile on 23/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

 
   var imgArr = [UIImage(named: "presentation"),UIImage(named: "res"), UIImage(named: "rser"), UIImage(named: "meeting"), UIImage(named: "policy"), UIImage(named: "shield"), UIImage(named: "question"),UIImage(named: "lock")]
    var nameArray =  ["TRAINING","RESOURCES","RESUME SERVICES","INTERVIEW SERVICES","TERMS & CONDITIONS","PRIVACY POLICY","FAQ","CHANGE PASSWORD"]

    @IBOutlet weak var menuTableViewOutlet: UITableView!

    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        if let _ = NSUSERDEFAULT.value(forKey: kcheckLoginType){
            let type : String = NSUSERDEFAULT.value(forKey: kcheckLoginType) as! String
            if type == "1" {
                nameArray = ["TRAINING","RESOURCES","RESUME SERVICES","INTERVIEW SERVICES","TERMS & CONDITIONS","PRIVACY POLICY","FAQ"]
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 45
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
      
        cell.selectionStyle = .none
        cell.commonNameLbl.text = nameArray[indexPath.row]
        cell.commonImageView.image = imgArr[indexPath.row]
        cell.commonBtn.tag = indexPath.row+1001
        cell.commonBtn.addTarget(self, action: #selector(clickButtonAction), for: .touchUpInside)
        return cell
    }

    @objc func clickButtonAction(_sender : UIButton)
    {
        let tag = _sender.tag-1000
        
        
        if tag == 1 {
            
            UIApplication.shared.openURL(NSURL(string: "https://www.excellot.com/training")! as URL)
        }
        
        
       else if tag == 2 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let resourceVC = storyBoard.instantiateViewController(withIdentifier: "ResourcesViewController") as! ResourcesViewController
            APPDELEGATE.navController?.pushViewController(resourceVC, animated: true)
        }
        else if tag == 3 {

//            if let _ = NSUSERDEFAULT.value(forKey: kUserID)
//            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let resumeVC = storyBoard.instantiateViewController(withIdentifier: "ResumeServiceViewController") as! ResumeServiceViewController
                APPDELEGATE.navController?.pushViewController(resumeVC, animated: true)
//            }else {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gotoLogin"), object: nil)
//            }
        }

        else if tag == 4 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let interviewVC = storyBoard.instantiateViewController(withIdentifier: "InterviewServiceViewController") as! InterviewServiceViewController
            APPDELEGATE.navController?.pushViewController(interviewVC, animated: true)
        }
        else if tag == 5 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            staticVC.contentType = methodType.terms
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
            
            
        else if tag == 6 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            staticVC.contentType = methodType.privacy
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
        else if tag == 7 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            staticVC.contentType = methodType.faq
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
        else if tag == 8 {
            
            if let _ = NSUSERDEFAULT.value(forKey: kUserID)
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let interviewVC = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                APPDELEGATE.navController?.pushViewController(interviewVC, animated: true)
            }else {
                AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                    if index == 1{
                        
                        self.navigationController!.viewControllers.removeAll()
                        APPDELEGATE.goToLogin()
                    }
                }            }
        }
    }
}
