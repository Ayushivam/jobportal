//
//  ChangePasswordViewController.swift
//  Job Portal
//
//  Created by nile on 03/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var oldPassTextField: PaddedTextField!
    @IBOutlet weak var newPassTextField: PaddedTextField!
    @IBOutlet weak var confirmNewPassTextField: PaddedTextField!
   
    var isfromEmployee = false
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        self.navigationController?.navigationBar.isHidden = true

       oldPassTextField.layer.cornerRadius = 2.0
        oldPassTextField.layer.borderWidth = 0.50
        oldPassTextField.layer.borderColor = UIColor.black.cgColor
        newPassTextField.layer.cornerRadius = 2.0
        newPassTextField.layer.borderWidth = 0.50
        newPassTextField.layer.borderColor = UIColor.black.cgColor
        confirmNewPassTextField.layer.cornerRadius = 2.0
        confirmNewPassTextField.layer.borderWidth = 0.50
        confirmNewPassTextField.layer.borderColor = UIColor.black.cgColor
        oldPassTextField.tag = 100
        newPassTextField.tag = 101
        confirmNewPassTextField.tag = 102
        oldPassTextField.isSecureTextEntry = true
        confirmNewPassTextField.isSecureTextEntry = true
        newPassTextField.isSecureTextEntry = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Button Action
    @IBAction func backBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        if oldPassTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter old password.")
        }
        
        else if newPassTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter new password.")
        }
        
        else if confirmNewPassTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter confirm new password.")
        }
            
        else if oldPassTextField.text == newPassTextField.text {
            
            AlertController.alert(title: "Old password and new password are same.")
        }
        
        else if newPassTextField.text != confirmNewPassTextField.text {
            
            AlertController.alert(title: "New password and confirm new password must be same.")
        }
        
        else {
            
            if isfromEmployee
            {
                 callApiForChangePassword(api:kEmployeeChangePassword)
            }else{
                callApiForChangePassword(api:kChangePassword)
            }
            
          
        }
        
    }
    
    
    //MARK:- UITEXT FIELD DELEGATE
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 15
            {
                return false
            }
            break
        case 101:
            if str.length > 15
            {
                return false
            }
            
        case 102:
            if str.length > 15
            {
                return false
            }
        default:
            break
        }
        return true
    }

    //MARK:- WEB API
    func callApiForChangePassword(api:String)
    {
        var paramDict = Dictionary<String,Any>()
        let _ = (isfromEmployee) ? ( paramDict["emp_id"] = NSUSERDEFAULT.value(forKey: kEmpID) as! String) : ( paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID) as! String)
        paramDict["oldpass"] = oldPassTextField.text!
        paramDict["confirmpass"] = confirmNewPassTextField.text!

        ServiceHelper.request(paramDict, method: .post, apiName: api, hudType: .smoothProgress) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            if (response.validatedValue("message", expected: "" as AnyObject) as! String ==  "Your old password is incorrect!") {
                                AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)

                            }
                            else {
                            AlertController.alert(title: "", message: response.validatedValue("message", expected: "" as AnyObject) as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            }
                        }
                        else  if status == false {
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)

                        }
                    }else{
                    }
                }
            }
        }
    }
}
