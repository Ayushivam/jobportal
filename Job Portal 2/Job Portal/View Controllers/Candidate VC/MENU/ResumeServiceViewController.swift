//
//  ResumeServiceViewController.swift
//  Job Portal
//
//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
class ResumeServiceViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,isPurhase {
   
    
    @IBOutlet weak var resumeTableViewOutlet: UITableView!
    var priceArr = Array<String>()
    var titleArr = Array<String>()
    var contentArr = Array<String>()
    var subserviceArr = Array<String>()
    var resumeServiceArray = [UserInfo]()
    var subService = ""
    var service = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        callApiForGetResumeService()
        resumeTableViewOutlet.estimatedRowHeight = 200
        resumeTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    func isFromPaypal() {
        
        callApiForGetResumeService()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      ///  let _ = (section == 0) ? (return 1) : (return resumeServiceArray.count)
        if section == 0{
            return 1
        }else {
            return resumeServiceArray.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ResumeServiceTableViewCell", for: indexPath) as! ResumeServiceTableViewCell
        if indexPath.section == 1{

            let resumeServiceObj = resumeServiceArray[indexPath.row]
            cell.resumeWritingLabel.text = resumeServiceObj.title
            cell.orderNowConstrints.constant = 40
            cell.dollarMoneyLabel.text = "$" + resumeServiceObj.price + " " + "per User"
            cell.contentLabel.text = resumeServiceObj.content
            cell.orderNowBtn.isHidden = false
            
            if resumeServiceObj.bought == "1" {
                cell.orderNowBtn.setTitle("ORDERED", for: .normal)
                 cell.orderNowBtn.isUserInteractionEnabled = false
            }
            else {
                cell.orderNowBtn.isUserInteractionEnabled = true
                cell.orderNowBtn.setTitle("ORDER NOW", for: .normal)

            }
            
            cell.subTittleLabel.text = resumeServiceObj.subService
            cell.orderNowBtn.tag = indexPath.row+1500
            cell.contentLabel.textColor = UIColor.black
            cell.contentLabel.font =  UIFont(name: "RobotoCondensed-Regular", size: 15)
            cell.orderNowBtn.addTarget(self, action: #selector(ordernowBtnAction), for: .touchUpInside)

            return cell
        }else {
            cell.dollarMoneyLabel.text = "Select Your Resume Service"
            cell.contentLabel.text = self.service
            cell.orderNowConstrints.constant = 0
            cell.orderNowBtn.isHidden = true
            cell.subTittleLabel.text = ""
            cell.resumeWritingLabel.text = ""
            cell.contentLabel.font =  UIFont(name: "RobotoCondensed-Regular", size: 16)
            cell.contentLabel.textColor = UIColor.gray
            return cell
        }
    }
    
    @objc func ordernowBtnAction(sender:UIButton) {

        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            let index = sender.tag-1500
            let resumeServiceValueObj  = resumeServiceArray[index]
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OrderNowViewController") as! OrderNowViewController
            popOverVC.providesPresentationContextTransitionStyle = true
            popOverVC.isGetValue = true
            popOverVC.valueObj = resumeServiceValueObj
            popOverVC.delegate = self
            popOverVC.definesPresentationContext = true
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
            self.present(popOverVC, animated: false, completion: nil)
        }else {
            AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                
                if index == 1{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }

    //MARK :- WEB API
    func callApiForGetResumeService()
    {
        var paramDict = Dictionary<String, Any>()
        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)

        }else{
              paramDict["user_id"] = ""
        }

        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        
        ServiceHelper.request(paramDict, method: .post, apiName:kResumeService , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            let arr : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            self.resumeServiceArray.removeAll()
                            for i in arr{
                              let userObj = UserInfo()
                                let dict : Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.title = dict.validatedValue("title", expected: "" as AnyObject) as! String
                                userObj.price = dict.validatedValue("price", expected: "" as AnyObject) as! String
                               
                                userObj.content = dict.validatedValue("content", expected: "" as AnyObject) as! String
                                userObj.subService = dict.validatedValue("sub_services", expected: "" as AnyObject) as! String
                                userObj.bought = dict.validatedValue("baught", expected: "" as AnyObject) as! String
                                userObj.resume_id = dict.validatedValue("resume_id", expected: "" as AnyObject) as! String
                                let data = userObj.subService.data(using: String.Encoding.unicode)!
                                let attrStr = try? NSAttributedString(
                                    data: data,
                                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                    documentAttributes: nil)
                                userObj.subService = (attrStr?.string)!
                               // if userObj.bought == "0"{
                                self.resumeServiceArray.append(userObj)
                            //    }
                            }
                            
                            let header : Dictionary = response.validatedValue("header", expected: "" as AnyObject) as! Dictionary<String,AnyObject>
                            let headerValue : String = header.validatedValue("header", expected: "" as AnyObject) as! String
                            // works even without <html><body> </body></html> tags, BTW
                            let data = headerValue.data(using: String.Encoding.unicode)! // mind "!"
                            let attrStr = try? NSAttributedString( // do catch
                                data: data,
                                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                documentAttributes: nil)
                            // suppose we have an UILabel, but any element with NSAttributedString will do
                            self.service = (attrStr?.string)!
                            self.resumeTableViewOutlet.reloadData()
                        }
                        else {
                           
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
}
