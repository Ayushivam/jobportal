//
//  StaticPagesVC.swift
//  Job Portal
//
//  Created by nile on 30/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
enum methodType {
    case terms,privacy,faq
}

class StaticPagesVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var termsnConditionArray = [UserInfo]()
    var faqArray = [UserInfo]()
    @IBOutlet weak var staticTableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    var contentType:methodType?

    override func viewDidLoad() {
        super.viewDidLoad()
        let _  = (contentType == methodType.terms) ? (titleLbl.text = "TERMS & CONDITIONS") : (contentType == methodType.privacy) ? (titleLbl.text = "PRIVACY POLICY") : (titleLbl.text = "FAQ")
        let _  = (contentType == methodType.terms) ? (callApiForStaticPage(apiname: kTerms)) : (contentType == methodType.privacy) ? (callApiForStaticPage(apiname: kPrivacy)) : (callApiForStaticPage(apiname: kFaqs))

        staticTableView.estimatedRowHeight = 100
        staticTableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }

    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- TABLE VIEW DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return termsnConditionArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "staticTableViewCell", for: indexPath) as! staticTableViewCell
        let obj  = termsnConditionArray[indexPath.row]
        cell.contentLabel.text = obj.title
        if contentType == methodType.faq
        {
            cell.answerTitle.text = obj.content
         //   cell.contentLabel.textColor = UIColor.black
          //  cell.contentLabel.font =  UIFont(name: "RobotoCondensed-Bold", size: 17)
           // cell.answerTitle.textColor = UIColor.darkGray
        //    cell.answerTitle.font =  UIFont(name: "RobotoCondensed-Regular", size: 15)
        }else {

            cell.contentLabel.textColor = UIColor.black
            cell.contentLabel.font =  UIFont(name: "RobotoCondensed-Regular", size: 15)
            cell.answerTitle.text = ""
        }
        return cell
    }

    func callApiForStaticPage(apiname:String)
    {
        let paramDict = Dictionary<String, Any>()
        ServiceHelper.request(paramDict, method: .get, apiName:apiname , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }

            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            let arr : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            self.termsnConditionArray.removeAll()
                            if self.contentType == methodType.faq{
                                for i in arr{
                                    let userObj = UserInfo()
                                    let dict : Dictionary = i as! Dictionary<String,AnyObject>
                                    userObj.id = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                    userObj.title = dict.validatedValue("question", expected: "" as AnyObject) as! String
                                    userObj.content = dict.validatedValue("answer", expected: "" as AnyObject) as! String
                                    let data = userObj.content.data(using: String.Encoding.unicode)!
                                    let attrStr = try? NSAttributedString(
                                        data: data,
                                        options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                        documentAttributes: nil)
                                    userObj.content = (attrStr?.string)!
                                    self.termsnConditionArray.append(userObj)
                                }
                        }else{
                            for i in arr{
                                let userObj = UserInfo()
                                let dict : Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.id = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                userObj.title = dict.validatedValue("content", expected: "" as AnyObject) as! String
                                let data = userObj.title.data(using: String.Encoding.unicode)!
                                let attrStr = try? NSAttributedString(
                                    data: data,
                                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                    documentAttributes: nil)
                                userObj.title = (attrStr?.string)!
                                self.termsnConditionArray.append(userObj)
                            }
                        }
                            self.staticTableView.reloadData()
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
        
    }
    
    func callApiForFaq()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName:kFaqs , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            let arr : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            self.faqArray.removeAll()
                            for i in arr{
                                let userObj = UserInfo()
                                let dict : Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.id = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                userObj.name = dict.validatedValue("name", expected: "" as AnyObject) as! String
                                userObj.content = dict.validatedValue("content", expected: "" as AnyObject) as! String
                                let data = userObj.content.data(using: String.Encoding.unicode)!
                                let attrStr = try? NSAttributedString(
                                    data: data,
                                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                                    documentAttributes: nil)
                                userObj.content = (attrStr?.string)!
                                self.faqArray.append(userObj)
                                
                            }
                            self.staticTableView.reloadData()
                        }
                        else {
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
        
    }
  
    
}
