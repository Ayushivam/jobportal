//
//  InterviewServiceViewController.swift
//  Job Portal
//
//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class InterviewServiceViewController: UIViewController,PayPalPaymentDelegate {
  
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buyBtn: UIButton!
    @IBOutlet weak var langBtn: UIButton!
    @IBOutlet weak var jobTypeBtn: UIButton!
    @IBOutlet weak var expBtn: UIButton!
   
    @IBOutlet weak var langView: UIView!
    @IBOutlet weak var jobTypeView: UIView!
    @IBOutlet weak var experienceView: UIView!
    @IBOutlet weak var beniftsPointMAinView: UIView!
    var JobTypeArray = Array<String>()
    var JobIDArray = Array<String>()
    var countryArray = Array<String>()
    var currencyArray = Array<String>()
    var codeArray = Array<String>()
    var symbolArray = Array<String>()
    var code = ""
    var dataInterviewSer = ""
    var jobTypeIdString = ""

     var expYearArray = ["0 Year","1 Year","2 Year","3 Year","4 Year","5 Year","6 Year","7 Year","8 Year","9 Year","10 Year","11 Year","12 Year","13 Year","14 Year","15 Year","16 Year","17 Year","18 Year","19 Year","20 Year","21 Year","22 Year","23 Year","24 Year","25 Year","26 Year","27 Year","28 Year","29 Year","30 Year","31 Year","32 Year","33 Year","34 Year","35 Year","36 Year","37 Year","38 Year","39 Year","40 Year","41 Year","42 Year","43 Year","44 Year","45 Year","46 Year","47 Year","48 Year","49 Year","50 Year"]
    var interviewServiceArray = [UserInfo]()
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    var payPalConfig = PayPalConfiguration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        callApiForGetInterviewService()
        callApiGetSelectJobType()
        callApiGetCurrency()
        priceView.layer.borderWidth = 0.50
        priceView.layer.borderColor = UIColor.black.cgColor
        priceView.layer.cornerRadius = 2.0
        experienceView.layer.borderWidth = 0.50
        experienceView.layer.borderColor = UIColor.black.cgColor
        experienceView.layer.cornerRadius = 2.0
        jobTypeView.layer.borderWidth = 0.50
        jobTypeView.layer.borderColor = UIColor.black.cgColor
        jobTypeView.layer.cornerRadius = 2.0
        langView.layer.borderWidth = 0.50
        langView.layer.borderColor = UIColor.black.cgColor
        langView.layer.cornerRadius = 2.0
        currencyLabel.text = "0.0"
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Actions
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func commonBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        
        switch sender.tag {
        case 11:
            commonOptionPicker(selectArray: expYearArray, button: expBtn)

            break
        case 12:
            commonOptionPicker(selectArray: JobTypeArray, button: jobTypeBtn)

            break
        case 13:
            commonOptionPicker(selectArray: symbolArray,button:langBtn )
            break
        case 14:
            view.endEditing(true)
            if expBtn.currentTitle == "Select Experience"
            {
                AlertController.alert(title: "Please select experience.")
            }

            else if jobTypeBtn.currentTitle == "Select Job Type"{
                AlertController.alert(title: "Please select job type.")
            }

            else if langBtn.currentTitle == "Select Currency"{
                AlertController.alert(title: "Please select currency.")
            }

            else {
                if let _ = NSUSERDEFAULT.value(forKey: kUserID)
                {
                    payPalConfiguration(price:self.currencyLabel.text!, code:code) //self.currencyLabel.text
                }else {
                    
                    AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                        
                        if index == 1{
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
            }
            break
        default:
            break
        }
    }

    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){

        if selectArray.count == 0
        {

        }else
        {
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
            if button == self.langBtn{
                button.setTitle(string, for: .normal)
                let id = self.codeArray[i]
                if i == self.codeArray.count-1
                {

                    self.currencyLabel.text = self.dataInterviewSer
                    button.setTitle(string , for: .normal)
                    self.code = self.codeArray[i]
                }else {
                    button.setTitle(string , for: .normal)
                    self.code = self.codeArray[i]
                    self.callApiForConvertCurrency(codeId: id)
                }
            }

            else if button == self.jobTypeBtn{
                button.setTitle(string, for: .normal)
                let id = self.JobIDArray[i]
                self.jobTypeIdString = id
            }

            else {
                button.setTitle(string, for: .normal)
            }
            }
        }
    }

    //MARK:- Paypal Configuration
    func payPalConfiguration(price:String,code:String)
    {
        let payablePrice : Float = Float(price)!
        let roundUpPRice = Int(round(payablePrice))
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "Excellot"//Give your company name here.
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        //This is the language in which your paypal sdk will be shown to users.
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        //Here you can set the shipping address. You can choose either the address associated with PayPal account or different address. We’ll use .both here.
        payPalConfig.payPalShippingAddressOption = .both;
        let item1 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string:"\(roundUpPRice)"), withCurrency: code, withSku: "job+portal")
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items) //This is the total price of all the items
        // Optional: include payment details
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: 0, withTax: 0)
        let total = subtotal //This is the total price including shipping and ta
        let payment = PayPalPayment(amount: total, currencyCode: code, shortDescription: "Excellot", intent: .sale)
        payment.items = items
        let email : String = NSUSERDEFAULT.value(forKey: kUserEmail) as! String
        payment.payeeEmail = email
        payment.paymentDetails = paymentDetails
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn’t be processable, and you’d want
            // to handle that here.
            print("Payment not processalbe: (payment)")
        }
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        
        paymentViewController.dismiss(animated: true, completion: nil)
        print("PayPal Payment cancel ")
        
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            let dict : Dictionary = completedPayment.confirmation
            let responseDict : Dictionary = dict["response"] as! Dictionary<String,AnyObject>
            let amount  = completedPayment.amount
            var status : String = responseDict.validatedValue("state", expected: "" as AnyObject) as! String
            var payerStatus = ""
            let email : String = completedPayment.payeeEmail!
            if status == "approved"{
                status = "completed"
                payerStatus = "VERIFIED"
                
            }else {
                status = "pending"
                payerStatus = "UNVERIFIED"
            }
            self.callApiForSuccessPayment(trans_id:responseDict.validatedValue("id", expected: "" as AnyObject) as! String , status: status, total: "\(amount)",payeeEmail: email,payerStatus:payerStatus)

        })
    }

    //MARK:- WEB API
    func callApiGetSelectJobType()
    {
        let paramDict = Dictionary<String, Any>()

        ServiceHelper.request(paramDict, method: .get, apiName: kGetSelectJobType, hudType: .default) { (result, error, code) in
            if let error = error {

                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }

            else {
                
                if let jobArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in jobArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let jobName : String = dict.validatedValue("job_type", expected: "" as AnyObject) as! String
                            self.JobTypeArray.append(jobName)
                            let jobid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.JobIDArray.append(jobid)
                        }
                    }
                    else
                    {
                        //  AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }

    func callApiGetCurrency()
    {
        let paramDict = Dictionary<String, Any>()
        ServiceHelper.request(paramDict, method: .get, apiName: kGetCurrency, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            
                            for i in data
                            {
                                let dict : Dictionary = i as! Dictionary<String,AnyObject>
                                let  country: String = dict.validatedValue("country", expected: "" as AnyObject) as! String
                                self.countryArray.append(country)
                                let  currency: String = dict.validatedValue("currency", expected: "" as AnyObject) as! String
                                self.currencyArray.append(currency)
                                let  code: String = dict.validatedValue("code", expected: "" as AnyObject) as! String
                                self.codeArray.append(code)
                                let  symbol: String = dict.validatedValue("symbol", expected: "" as AnyObject) as! String + "(" + (dict.validatedValue("country", expected: "" as AnyObject) as! String) + ")"
                                self.symbolArray.append(symbol)
                            }
                        } else
                        {
                        }
                    }
                }
            }
        }
    }

    func callApiForConvertCurrency(codeId: String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["cur"] = codeId
        paramDict["price"] = dataInterviewSer
        
        ServiceHelper.request(paramDict, method: .post, apiName: kConvertCurrency, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            let message : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            self.currencyLabel.text = message.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range:nil)

                        }
                        else {

                        }

                    }
                }
            }
        }
    }

    //MARK :- WEB API
    func callApiForGetInterviewService()
    {
        let paramDict = Dictionary<String, Any>()
       // paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        
        ServiceHelper.request(paramDict, method: .get, apiName:kInterviewService , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            self.dataInterviewSer = response.validatedValue("price", expected: "" as AnyObject) as! String

                            self.priceLabel.text  = ("$" + (response.validatedValue("price", expected: "" as AnyObject) as! String) as? String)! + " " + "(Inclusive of all taxes)"
                            }
                        }
                    
                    } else{
                    }
                }
            }
        }
    
    
    func callApiForSuccessPayment(trans_id:String,status:String,total:String,payeeEmail:String,payerStatus:String)
    {
        var paramDict = Dictionary<String, Any>()
        
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["txn_id"] = trans_id
        paramDict["exp_year"] = expBtn.currentTitle
        paramDict["job_type_id"] = self.jobTypeIdString
        paramDict["PaymentMethod"] = "paypal"
        paramDict["PayerStatus"] = payerStatus
        paramDict["PayerMail"] = payeeEmail
        paramDict["Total"] = total
        paramDict["SubTotal"] = total
         paramDict["Tax"] = "0"
        paramDict["Payment_state"] = status
        
        
        ServiceHelper.request(paramDict, method: .post, apiName: kInterviewOrder, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            
                            let message : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            AlertController.alert(title: message, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                        else {
                            
                        }
                        
                    }
                }
            }
        }
    }
    }

