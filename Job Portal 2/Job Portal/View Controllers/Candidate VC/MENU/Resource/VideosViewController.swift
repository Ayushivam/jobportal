//
//  VideosViewController.swift
//  Job Portal
//
//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
import PDFReader
import  AVKit

class VideosViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var videoArray =  [MediaInfo]()

    @IBOutlet weak var videoCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callApiForGetResumeVideo()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- UI Collection View Delegate And Datasource
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PDFCollectionViewCell", for: indexPath) as! PDFCollectionViewCell
        let obj = self.videoArray[indexPath.row]
        cell.pdfName.text = obj.title
        cell.pdfDate.text = obj.data
        cell.pdfDesc.text = obj.desc

        DispatchQueue.global().async {
            let asset = AVAsset(url: URL(string: obj.video_file)!)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time = CMTimeMake(1, 2)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {

                    cell.videoThumb.image = frameImg
                })
            }
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return self.videoArray.count
    }

    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let padding: CGFloat = 5
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize.init(width: collectionViewSize/2, height: collectionViewSize/2)

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.videoArray[indexPath.row]

        let player = AVPlayer(url: URL(string: obj.video_file)!)
        player.volume = 1.0
        let vc = AVPlayerViewController()
        vc.player = player
        present(vc, animated: true) {
            vc.player?.play()
        }
        
    }
    
//    func showFileWithPath(path: String){
//
//        let url = URL(string: path)
//        let document = PDFDocument(url: url!)
//        if document != nil
//        {
//            let readerController = PDFViewController.createNew(with: document!)
//            readerController.navigationController?.isNavigationBarHidden = false
//            APPDELEGATE.navController?.pushViewController(readerController, animated: true)
//        }
//    }
    
    //MARK :- WEB API
    func callApiForGetResumeVideo()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName:kResources , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            self.videoArray += MediaInfo.getMediaArayList(responseArray: response.validatedValue("video", expected: "" as AnyObject) as! Array<Any> as! Array<Dictionary<String, Any>>)
                                self.videoCollectionView.reloadData()
                        }
                        else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            print(msg)
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
   

}
