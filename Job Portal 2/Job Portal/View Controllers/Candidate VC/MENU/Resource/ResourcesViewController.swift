//
//  ResourcesViewController.swift
//  Job Portal
//
//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
import CarbonKit
class ResourcesViewController: UIViewController,CarbonTabSwipeNavigationDelegate {
    
    var carbonTabSwipeNavigation =  CarbonTabSwipeNavigation()
    @IBOutlet weak var cointainerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let item = ["VIDEOS","PDF"]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: item, delegate: self)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(kWindowWidth/2, forSegmentAt: 0)
        carbonTabSwipeNavigation.setNormalColor(UIColor.black, font: UIFont(name: "RobotoCondensed-Regular", size: 16.0)!)
        carbonTabSwipeNavigation.setSelectedColor(UIColor(red: 47/255, green: 85/255, blue: 151/255, alpha: 1),font: UIFont(name: "RobotoCondensed-Regular", size: 16.0)!)
        carbonTabSwipeNavigation.setIndicatorHeight(2)
        carbonTabSwipeNavigation.setIndicatorColor(UIColor(red: 47/255, green: 85/255, blue: 151/255, alpha: 1))
        
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(kWindowWidth/2, forSegmentAt: 1)
        carbonTabSwipeNavigation.setTabBarHeight(50)
        
        carbonTabSwipeNavigation.insert(intoRootViewController:self, andTargetView:cointainerView)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func bckBTNACTION(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        
        
        let baseStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        switch index {
        case 0:
            // if stepOne{
            
            
            let firstViewController = baseStoryboard.instantiateViewController(withIdentifier: "VideosViewController") as? VideosViewController
            
            return firstViewController!
            //  }
            
        default:
            //   if stepThree{
            let thirdViewController = baseStoryboard.instantiateViewController(withIdentifier: "PdfViewController") as? PdfViewController
            
            return thirdViewController!
            //  }
        }
        // return firstViewController!
        
    }
    
}
