
//  PdfViewController.swift
//  Job Portal

//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.


import UIKit
import PDFReader

class PdfViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var pdfCollectionView: UICollectionView!

    var pdfArray =  [MediaInfo]()

    override func viewDidLoad() {

        super.viewDidLoad()
        callApiForGetResumeService()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UI Collection View Delegate And Datasource
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PDFCollectionViewCell", for: indexPath) as! PDFCollectionViewCell
        let obj = self.pdfArray[indexPath.row]
        cell.pdfName.text = obj.title
        cell.pdfDate.text = obj.data
        cell.pdfDesc.text = obj.desc
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.pdfArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {

        let padding: CGFloat = 5
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize.init(width: collectionViewSize/2, height: collectionViewSize/2)

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
          let obj = self.pdfArray[indexPath.row]
         self.showFileWithPath(path:obj.video_file)
        
    }
    
    func showFileWithPath(path: String){
        
        let url = URL(string: path)
        let document = PDFDocument(url: url!)
        if document != nil
        {
            let readerController = PDFViewController.createNew(with: document!)
            readerController.navigationController?.isNavigationBarHidden = false
            APPDELEGATE.navController?.pushViewController(readerController, animated: true)
        }
    }
    
   
    
    //MARK :- WEB API
    func callApiForGetResumeService()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName:kResources , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            self.pdfArray += MediaInfo.getMediaArayList(responseArray: response.validatedValue("pdf", expected: "" as AnyObject) as! Array<Any> as! Array<Dictionary<String, Any>>)
                            self.pdfCollectionView.reloadData()
                            
                        }
                        else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            print(msg)
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
}
