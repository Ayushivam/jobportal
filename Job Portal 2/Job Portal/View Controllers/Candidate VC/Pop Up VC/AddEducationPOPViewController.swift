//
//  AddEducationPOPViewController.swift
//  Job Portal
//
//  Created by nile on 23/11/18.
//  Copyright © 2018 nile. All rights reserved.
//


protocol CreateDelegate: class {
    func passData()
}


import UIKit

class AddEducationPOPViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var specilizationBtn: UIButton!
    @IBOutlet weak var DateBtn: UIButton!
    @IBOutlet weak var educationBtn: UIButton!
    @IBOutlet weak var educationView: UIView!
    @IBOutlet weak var specilizationView: UIView!
    @IBOutlet weak var graDateView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var universityTextField: PaddedTextField!
    var eduArray = Array<String>()
    var eduIDArray = Array<String>()
    var specArray = Array<String>()
    var specIDArray = Array<String>()
    //****FOR PASS ID ***//
    var educationIdString = ""
    var sprcilizationIDString = ""
    @IBOutlet weak var topHeadingLbl: UILabel!
    
    var delegate: CreateDelegate?
    //*** UPDATION METHODS ***//
    var obj = UserInfo()
    var isUpdated = Bool()
    var eduID  = ""
    var education = ""
    var cource = ""
    var date = ""
    var university = ""
    var descrip = ""
    var specilizationArray = Array<String>()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        callApiGetSelectEducation()
        educationView.layer.borderWidth = 0.50
        educationView.layer.borderColor = UIColor.black.cgColor
        educationView.layer.cornerRadius = 2.0
        specilizationView.layer.borderWidth = 0.50
        specilizationView.layer.borderColor = UIColor.black.cgColor
        specilizationView.layer.cornerRadius = 2.0
        graDateView.layer.borderWidth = 0.50
        graDateView.layer.borderColor = UIColor.black.cgColor
        graDateView.layer.cornerRadius = 2.0
        universityTextField.layer.borderWidth = 0.50
        universityTextField.layer.borderColor = UIColor.black.cgColor
        universityTextField.layer.cornerRadius = 2.0
        descriptionTextView.layer.borderWidth = 0.50
        descriptionTextView.layer.borderColor = UIColor.black.cgColor
        descriptionTextView.layer.cornerRadius = 2.0
        descriptionTextView.text = "Description"
        descriptionTextView.textColor = UIColor.gray
        universityTextField.tag = 17
        descriptionTextView.tag = 18
        topHeadingLbl.text = "ADD EDUCATION"
        initialSetup()
        // Do any additional setup after loading the view.
    }

    func initialSetup() {

        if isUpdated {
            topHeadingLbl.text = "EDIT EDUCATION"
            descriptionTextView.textColor = UIColor.black
            DateBtn.setTitle(obj.startYear, for: .normal)
            universityTextField.text = obj.University
            descriptionTextView.text = obj.desp
            eduID = obj.id
        }
        else {

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Action
    @IBAction func commonBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        switch sender.tag {
        case 10:
            commonOptionPicker(selectArray: eduArray, button: educationBtn)
            
            break
        case 11:
            if educationBtn.currentTitle == "Select Education"
            {
                AlertController.alert(title: "Please select education.")
            }
            else {
                commonOptionPicker(selectArray: specArray, button: specilizationBtn)
            }
            break
        case 12:
            
            let newDate = Calendar.current.date(byAdding: .month, value: 0, to: Date())
            commonOptionPicker(button: DateBtn,date:newDate!)
      //      commonOptionPickerDate(button: DateBtn)
            break
        case 13:
            view.endEditing(true)
            if isAllFieldVerified(){
                let _ = (!isUpdated) ? ( callApiForCreateAddEducation()) :  (callApiForUpdateAddEducation(eduID: eduID))
            }
            break
        default:
            break
        }
    }
    
    func isAllFieldVerified()->Bool{
        var isVerified = false
        if educationBtn.currentTitle == "Select Education" {
            AlertController.alert(title: "Please select education.")
            isVerified = false
        }
        else if specilizationBtn.currentTitle == "Select Specialization" {
            AlertController.alert(title: "Please select specialization.")
            isVerified = false
        }
        else if universityTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter University/Institute.")
            isVerified = false
        }
        else if DateBtn.currentTitle == "Select Date" {
            AlertController.alert(title: "Please select date.")
            isVerified = false
        }
        else {
            isVerified = true
        }
        return isVerified
    }

    @IBAction func closeBtn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 17:
            if str.length > 100
            {
                return false
            }
            break
        
        default:
            break
        }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
    
    
    //MARK:- TEXTView DELEGATE

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.isFirstResponder {
            if (textView.textInputMode?.primaryLanguage == "emoji") || textView.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
   
 
    //DATE PICKER
    func commonOptionPicker(button: UIButton, date:Date) {
        RPicker.selectDate(datePickerMode: .date, selectedDate: date, minDate: nil, maxDate: date) {(date: Date) in
            let formattedDate = self.formattedDateFromString(dateString: "\(date)", withFormat: "yyyy-MM-dd")
            button.setTitle(formattedDate, for: .normal)
        }
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    
    
    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){
        
        if selectArray.count == 0
        {
            
        }else
        {
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
            
            if button == self.educationBtn{
                button.setTitle(string, for: .normal)
                let id = self.eduIDArray[i]
                self.educationIdString = id
                self.callApiGetSpecilization(eduId: id)
            }
            else if button == self.specilizationBtn{
                button.setTitle(string, for: .normal)
                let id = self.specIDArray[i]
                self.sprcilizationIDString = id
            }
            else {
                button.setTitle(string, for: .normal)
            }
        }
    }
    }
    
    //MARK:- TextView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if  descriptionTextView.text == "Description"  {
            descriptionTextView.text = ""
            descriptionTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if  descriptionTextView.text == ""  {
            descriptionTextView.text = "Description"
        }
    }
    
    //MARK:- WEB API
    func callApiGetSelectEducation()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetEducation, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let eduArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in eduArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let eduName : String = dict.validatedValue("name", expected: "" as AnyObject) as! String
                            self.eduArray.append(eduName)
                            let eduid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.eduIDArray.append(eduid)
                        }
                        if self.isUpdated{
                            for i in (0..<self.eduIDArray.count)
                            {
                                let id  = self.eduIDArray[i]
                                if id == self.obj.educationID
                                {
                                    let val = self.eduArray[i]
                                    self.educationIdString = id
                                    self.educationBtn.setTitle(val, for: .normal)
                                    self.callApiGetSpecilization(eduId: id)
                                }
                            }
                            
                        }
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetSpecilization(eduId : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["edu_id"] = eduId
        
        ServiceHelper.request(paramDict, method: .post, apiName: kGetSpeciliztion, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                
                if let response = result as? Dictionary<String , AnyObject>
                {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        self.specilizationBtn.setTitle("Select Specialization", for: .normal)
                        self.specArray.removeAll()
                        self.specIDArray.removeAll()
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                
                                let specName : String = dict.validatedValue("course_name", expected: "" as AnyObject) as! String
                                self.specArray.append(specName)
                                let specId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                                self.specIDArray.append(specId)
                            }
                            if self.isUpdated{
                            
                                for i in (0..<self.specIDArray.count)
                                {
                                    let id = self.specIDArray[i]
                                    if id == self.obj.courses
                                    {
                                         self.sprcilizationIDString = id
                                        self.specilizationBtn.setTitle(self.specArray[i], for: .normal)
                                    }
                                }
                            }
                        }else{
                        }
                    }
                }
            }
            
        }
    }

    func callApiForCreateAddEducation()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["education"] = self.educationIdString
        paramDict["course"] = self.sprcilizationIDString
        paramDict["univercity"] = self.universityTextField.text!
        paramDict["start_year"] = self.DateBtn.currentTitle
        paramDict["desc"] = self.descriptionTextView.text!

        ServiceHelper.request(paramDict, method: .post, apiName:kAddEducation , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            
                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                self.delegate?.passData()
                                self.dismiss(animated: true, completion: nil)
                            })
                            
                            
                        }
                        else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            print(msg)
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    
    
    func callApiForUpdateAddEducation(eduID : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["edu_id"] = eduID
        paramDict["education"] = self.educationIdString
        paramDict["course"] = self.sprcilizationIDString
        paramDict["univercity"] = self.universityTextField.text!
        paramDict["start_year"] = self.DateBtn.currentTitle
        paramDict["desc"] = self.descriptionTextView.text!
        
        
        ServiceHelper.request(paramDict, method: .post, apiName:kAddEducation , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            
                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                self.delegate?.passData()
                                self.dismiss(animated: true, completion: nil)
                            })
                            
                            
                        }
                        else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            print(msg)
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
}

