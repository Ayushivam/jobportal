//
//  WorknExpResumeViewController.swift
//  Job Portal
//
//  Created by nile on 27/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

protocol WorknExpDelegate : class {
    func passWorknExpData ()
}

import UIKit

class WorknExpResumeViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate {
    @IBOutlet weak var organizationTextField: PaddedTextField!
    @IBOutlet weak var designationTextField: PaddedTextField!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var monthBtn: UIButton!
    @IBOutlet weak var yearBtn: UIButton!
    @IBOutlet weak var tillmonthBtn: UIButton!
    @IBOutlet weak var tillyearBtn: UIButton!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var yearView: UIView!
    @IBOutlet weak var tillmonthView: UIView!
    @IBOutlet weak var  tillyearViewView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    var monthArray = ["1","2","3","4","5","6","7","8","9","10","11","12"]
    var yearValue = 0
    var isPresent = ""
    var workid = ""
    var designation = ""
    var organization = ""
    var isPresentBTn  = ""
    var month = ""
    var year = ""
    var tillMonth = ""
    var tillYear = ""
    var descrip = ""
    var delagate : WorknExpDelegate?
    var WorknExpObj = UserInfo()
    var isUpdated = Bool()
 
    @IBOutlet weak var topHeadingLbl: UILabel!
    @IBOutlet weak var downimageView: UIImageView!

    override func viewDidLoad() {

        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        descriptionTextView.layer.borderColor = UIColor.black.cgColor
        descriptionTextView.layer.cornerRadius = 2.0
        descriptionTextView.layer.borderWidth = 0.50
        organizationTextField.layer.borderColor = UIColor.black.cgColor
        organizationTextField.layer.cornerRadius = 2.0
        organizationTextField.layer.borderWidth = 0.50
        designationTextField.layer.borderColor = UIColor.black.cgColor
        designationTextField.layer.cornerRadius = 2.0
        designationTextField.layer.borderWidth = 0.50
        monthView.layer.borderColor = UIColor.black.cgColor
        monthView.layer.cornerRadius = 2.0
        monthView.layer.borderWidth = 0.50
        yearView.layer.borderColor = UIColor.black.cgColor
        yearView.layer.cornerRadius = 2.0
        yearView.layer.borderWidth = 0.50
        tillmonthView.layer.borderColor = UIColor.black.cgColor
        tillmonthView.layer.cornerRadius = 2.0
        tillmonthView.layer.borderWidth = 0.50
        tillyearViewView.layer.borderColor = UIColor.black.cgColor
        tillyearViewView.layer.cornerRadius = 2.0
        tillyearViewView.layer.borderWidth = 0.50
        initialSetup()
        submitBtn.layer.cornerRadius = 2.0
        topHeadingLbl.text = "EDIT WORK EXPERIENCE"

        if !isUpdated {
            topHeadingLbl.text =  "ADD WORK EXPERIENCE"

        yesBtn.setImage(#imageLiteral(resourceName: "fillcir"), for: .normal)
        yesBtn.isSelected = true
        if yesBtn.isSelected == true {
            tillyearBtn.isHidden = true
            tillyearViewView.isHidden = true
            tillmonthBtn.setTitle("Present", for: .normal)
            tillmonthBtn.isUserInteractionEnabled = false
            downimageView.isHidden = true
            isPresent = "1"
            }
        }

        // Do any additional setup after loading the view.

    }

    func initialSetup() {
        if isUpdated {
            workid = WorknExpObj.id
            designationTextField.text = WorknExpObj.designation
            organizationTextField.text = WorknExpObj.company
            isPresentBTn = WorknExpObj.isPresent
            descriptionTextView.text = WorknExpObj.desp
            monthBtn.setTitle(WorknExpObj.startMonth, for: .normal)
            yearBtn.setTitle(WorknExpObj.startYear, for: .normal)
            if isPresentBTn == "1"
            {
                yesBtn.setImage(#imageLiteral(resourceName: "fillcir"), for: .normal)
                tillmonthBtn.setTitle("Present", for: .normal)
                tillmonthBtn.isUserInteractionEnabled = false
                tillyearBtn.isHidden = true
                tillyearViewView.isHidden = true
                downimageView.isHidden = true
                yesBtn.isSelected = true
                isPresent = "1"
            }
            else if isPresentBTn == "0"{
                noBtn.setImage(#imageLiteral(resourceName: "fillcir"), for: .normal)
                noBtn.isSelected = true
                isPresent = "0"
                tillmonthBtn.setTitle(WorknExpObj.lastMonth, for: .normal)
                tillyearBtn.setTitle(WorknExpObj.lastYear, for: .normal)
            }
            else {
                
            }
   
        }
        else {
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func commonActionBtn(_ sender: UIButton) {
        view.endEditing(true)
        
        switch sender.tag {
        case 10:
            dismiss(animated: true, completion: nil)
            break
            
        case 11:
            initialBoxSetup()
            yesBtn.setImage(#imageLiteral(resourceName: "fillcir"), for: .normal)
            yesBtn.isSelected = true
            
            if yesBtn.isSelected == true {
                tillyearBtn.isHidden = true
                tillyearViewView.isHidden = true
                tillmonthBtn.setTitle("Present", for: .normal)
                tillmonthBtn.isUserInteractionEnabled = false
                downimageView.isHidden = true
            }
            
            noBtn.isSelected = false
            isPresent = "1"
            break
            
        case 12:
            initialBoxSetup()
            noBtn.setImage(#imageLiteral(resourceName: "fillcir"), for: .normal)
            noBtn.isSelected = true
            if noBtn.isSelected == true {
                tillyearBtn.isHidden = false
                tillyearViewView.isHidden = false
                tillmonthBtn.setTitle("Select Month", for: .normal)
                tillyearBtn.setTitle("Select Year", for: .normal)
                tillmonthBtn.isUserInteractionEnabled = true
                downimageView.isHidden = false
            }
            yesBtn.isSelected = false
            isPresent = "0"
            break
        case 13:
            commonOptionPicker(selectArray: monthArray, button: monthBtn)
            break
        case 14:
            commonOptionPickerOnlyDate(btn: yearBtn)
            break
            
        case 15:
            commonOptionPicker(selectArray: monthArray, button: tillmonthBtn)
            break
        case 16:
            commonOptionPickerOnlyDate(btn: tillyearBtn)
            break
        case 17:
            view.endEditing(true)
            if isAllVerified() {
                
                if !isUpdated {
                
                if yesBtn.isSelected
                {
                    callApiForCreateAddWorkNExp()
                }else {
                    if tillmonthBtn.currentTitle == "Select Month"
                    {
                     AlertController.alert(title: "Please select working till month.")
                    }else if  tillyearBtn.currentTitle == "Select Year"{
                        AlertController.alert(title: "Please select working till year.")
                    }else {
                        callApiForCreateAddWorkNExp()
                    }
                }
                }
                    
                else if yesBtn.isSelected {
                    callApiForUpdateWorknExp(worknExpID: workid)
                }
                
                else
                {
                    if tillmonthBtn.currentTitle == "Select Month"
                    {
                        AlertController.alert(title: "Please select working till month.")
                    }else if  tillyearBtn.currentTitle == "Select Year"{
                        AlertController.alert(title: "Please select working till year.")
                    }else {
                        callApiForUpdateWorknExp(worknExpID: workid)
                    }
                }
            }
            
            break
        default:
            break
        }
    }

    func isAllVerified()-> Bool{
        
        var isVerified = false
        if designationTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter your designation.")
            isVerified = false
            return isVerified
        }
            
        else if organizationTextField.text?.count == 0{
            AlertController.alert(title: "Please enter your organization.")
            isVerified = false
            return isVerified
        }
            
        else if yesBtn.isSelected == false && noBtn.isSelected == false {
            AlertController.alert(title: "Please select is this your Current Company?")
            isVerified = false
            return isVerified
        }
            
        else if monthBtn.currentTitle == "Select Month" {
            AlertController.alert(title: "Please select month.")
            isVerified = false
            return isVerified
        }
        else if yearBtn.currentTitle == "Select Year" {
            AlertController.alert(title: "Please select year.")
            isVerified = false
            return isVerified
        }
        else{
            isVerified = true
        }

        if noBtn.isSelected == true
        {
            let startMonth : Int =  (self.monthBtn.currentTitle! as NSString).integerValue
            let startYear : Int =  (self.yearBtn.currentTitle! as NSString).integerValue
            let tillMonth : Int =  (self.tillmonthBtn.currentTitle! as NSString).integerValue
            let tillYear : Int =  (self.tillyearBtn.currentTitle! as NSString).integerValue
            if startYear >= tillYear{
                if startYear == tillYear
                {
                    if startMonth > tillMonth
                    {
                        AlertController.alert(title: "Please select valid working duration.")
                        isVerified = false
                        return isVerified
                    }else {
                        isVerified = true

                        
                    }
                }else {
                    AlertController.alert(title: "Please select valid working duration.")
                    isVerified = false
                    return isVerified
                }
            }
        }
        
        return isVerified
    }
    
    //METHOD FOR BTN
    func initialBoxSetup()
    {
        yesBtn.setImage(UIImage(named:"circle"), for: .normal)
        noBtn.setImage(UIImage(named:"circle"), for: .normal)
    }
        //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){
        if selectArray.count == 0
        {
            
        }else
        {
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
            button.setTitle(string, for: .normal)
        }
        }
    }
    
    func commonOptionPickerOnlyDate(btn:UIButton){
        
        let date = Date()
        let year : String = formattedDateFromStringForDate(dateString: "\(date)", withFormat: "yyyy") as! String
        let countYear : Int = Int(year)!
        print(countYear)
        
        var arr = [String]()
        for j in (1970..<countYear+1)
        {
            arr.append("\(j)")
        }

        RPicker.sharedInstance.selectOption(dataArray: arr , selectedIndex: arr.count-1) { (string, i) in
            btn.setTitle(string, for: .normal)
        }
    }

    //DATE PICKER
    func commonOptionPickerDate(button: UIButton) {
        
        RPicker.selectDate(datePickerMode: .date, selectedDate: Date(), minDate: nil, maxDate: nil) {  (date: Date) in
            let formattedDate = self.formattedDateFromString(dateString: "\(date)", withFormat: "dd-MM-yyyy")
            button.setTitle(formattedDate, for: .normal)
        }
    }

    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }

        return nil
    }
    
    func formattedDateFromStringForDate(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        
        return nil
    }

    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
    
    //MARK:- TEXTView DELEGATE
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.isFirstResponder {
            if (textView.textInputMode?.primaryLanguage == "emoji") || textView.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
    
    //MARK:- WEB API
    func callApiForCreateAddWorkNExp()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["designation"] = designationTextField.text
        paramDict["company"] = organizationTextField.text
        paramDict["is_present"] = self.isPresent
        if self.isPresent == "1"
        {
            paramDict["present"] = tillmonthBtn.currentTitle
            paramDict["last_month"] = ""
            paramDict["last_year"] = ""
        }
        if self.isPresent == "0" {
            paramDict["present"] = ""
            paramDict["last_month"] = tillmonthBtn.currentTitle
            paramDict["last_year"] = tillyearBtn.currentTitle
        }
        
        
       
        paramDict["start_month"] = monthBtn.currentTitle
        paramDict["start_year"] = yearBtn.currentTitle
        paramDict["desc"] = descriptionTextView.text
        
        ServiceHelper.request(paramDict, method: .post, apiName:kAddWorknExp , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            
                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                self.delagate?.passWorknExpData()
                                self.dismiss(animated: true, completion: nil)
                            })
                            
                            
                        }
                        else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            print(msg)
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }

    func callApiForUpdateWorknExp(worknExpID : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["work_id"] = worknExpID

        paramDict["designation"] = designationTextField.text
        paramDict["company"] = organizationTextField.text
        paramDict["is_present"] = self.isPresent
        if self.isPresent == "1"
        {
            paramDict["present"] = tillmonthBtn.currentTitle
            paramDict["last_month"] = ""
            paramDict["last_year"] = ""
        }
        if self.isPresent == "0" {
            paramDict["present"] = ""
            paramDict["last_month"] = tillmonthBtn.currentTitle
            paramDict["last_year"] = tillyearBtn.currentTitle
        }
        paramDict["start_month"] = monthBtn.currentTitle
        paramDict["start_year"] = yearBtn.currentTitle
        paramDict["desc"] = descriptionTextView.text
        
        ServiceHelper.request(paramDict, method: .post, apiName:kAddWorknExp , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            
                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                self.delagate?.passWorknExpData()
                                self.dismiss(animated: true, completion: nil)
                            })
                            
                            
                        }
                        else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            print(msg)
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
}
