//
//  ProfessionalViewController.swift
//  Job Portal
//
//  Created by nile on 27/11/18.
//  Copyright © 2018 nile. All rights reserved.
//



protocol ProfessionalDelegate : class {
    func passProfData ()
}
import UIKit

class ProfessionalViewController: UIViewController,UITextViewDelegate {
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var skillsView: UIView!
    @IBOutlet weak var skillsBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    var skillArray = Array<String>()
    var skllIdArray = Array<String>()
    var delagate : ProfessionalDelegate?
    var proObj = UserInfo()
    var isUpdated = Bool()

    @IBOutlet weak var topHeadingLabel: UILabel!
    //****FOR PASS ID ***//
    var skillIdString = ""
    
    var profID = ""
    var skills = ""
    var profDescription = ""
    override func viewDidLoad() {
        super.viewDidLoad()
     callApiGetSelectSkill()
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        descriptionTextView.layer.borderColor = UIColor.black.cgColor
        descriptionTextView.layer.cornerRadius = 2.0
        descriptionTextView.layer.borderWidth = 0.50
        submitBtn.layer.cornerRadius = 2.0
        mainView.layer.cornerRadius = 5.0
        skillsView.layer.borderWidth = 0.50
        skillsView.layer.borderColor = UIColor.black.cgColor
        skillsView.layer.cornerRadius = 2.0
        topHeadingLabel.text = "ADD PROFESSIONAL SKILL"
        initialSetup()
        // Do any additional setup after loading the view.
    }

    
    
    func initialSetup() {
        if isUpdated {
            topHeadingLabel.text = "EDIT PROFESSIONAL SKILL"
            descriptionTextView.text = proObj.desp
            profID = proObj.id
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func closeButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func commonActionBtn(_ sender: UIButton) {
        view.endEditing(true)
        switch sender.tag {
        case 100:
               commonOptionPicker(selectArray: skillArray, button: skillsBtn)
            break
            
        case 101:
            view.endEditing(true)
            if isAllFieldVerified(){
                let _ = (!isUpdated) ? ( callApiForCreateAddProfessionalSkill()) :  (callApiForUpdateProfessionalSkill(profID: profID))
            }
            
            
       break
        default:
            break
        }
    }
    
    func isAllFieldVerified()->Bool{
        var isVerified = false
        if skillsBtn.currentTitle == "Select Skill"
        {
            AlertController.alert(title: "Please Select Skill")
            isVerified = false
        }
        else {
            isVerified = true
        }
        return isVerified
    }
    
    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){
        
        if selectArray.count == 0
        {
            
        }else
        {
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
            
            if button == self.skillsBtn {
                button.setTitle(string, for: .normal)
                let id = self.skllIdArray[i]
                self.skillIdString = id
            }
            else {
                button.setTitle(string, for: .normal)
            }
        }
        }
    }
    
    //MARK:- TEXTView DELEGATE
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.isFirstResponder {
            if (textView.textInputMode?.primaryLanguage == "emoji") || textView.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
    
    //MARK:- WEB API
    func callApiGetSelectSkill()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetSkill, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                
                
                if let skillArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in skillArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let skillName : String = dict.validatedValue("name", expected: "" as AnyObject) as! String
                            self.skillArray.append(skillName)
                            let skillid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.skllIdArray.append(skillid)
                            
                        }
                        
                        if self.isUpdated{
                            for i in (0..<self.skllIdArray.count)
                            {
                                let id  = self.skllIdArray[i]
                                if id == self.proObj.profSkill
                                {
                                    let val = self.skillArray[i]
                                    self.skillIdString = id
                                    self.skillsBtn.setTitle(val, for: .normal)
                                }
                            }
                            
                        }
                        
                        
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    func callApiForCreateAddProfessionalSkill()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["skill"] = self.skillIdString
        paramDict["desc"] = self.descriptionTextView.text!
        
        ServiceHelper.request(paramDict, method: .post, apiName:kAddProfessional , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            
                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                
                                self.delagate?.passProfData()
                                self.dismiss(animated: true, completion: nil)
                            })
                            
                            
                        }
                        else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            print(msg)
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    func callApiForUpdateProfessionalSkill( profID : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["id"] = profID

        paramDict["skill"] = self.skillIdString
        paramDict["desc"] = self.descriptionTextView.text!
        
        ServiceHelper.request(paramDict, method: .post, apiName:kAddProfessional , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true
                        {
                            
                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                self.delagate?.passProfData()
                                self.dismiss(animated: true, completion: nil)
                            })
                            
                            
                        }
                        else {
                            let msg : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            print(msg)
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
   
}
