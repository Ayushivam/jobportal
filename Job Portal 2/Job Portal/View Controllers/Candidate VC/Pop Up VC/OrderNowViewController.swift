//
//  OrderNowViewController.swift
//  Job Portal
//
//  Created by nile on 29/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit


protocol isPurhase : class {
    func isFromPaypal()
}

class OrderNowViewController: UIViewController,PayPalPaymentDelegate{

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var resumeView: UIView!
    @IBOutlet weak var resumePriceView: UIView!
    @IBOutlet weak var currencyView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var resumeTittleLbl: UILabel!
    @IBOutlet weak var resumePriceLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var currencyBtn: UIButton!
    var delegate : isPurhase?
    var countryID = ""
    var commonarray = Array<String>()
    var code = ""
    var countryArray = Array<String>()
    var currencyArray = Array<String>()
    var codeArray = Array<String>()
    var symbolArray = Array<String>()
    var resumePrice = ""
    var isGetValue = Bool()
    var valueObj = UserInfo()
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }

    var payPalConfig = PayPalConfiguration()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        callApiGetCurrency()
        mainView.layer.cornerRadius = 5.0
        resumeView.layer.borderColor = UIColor.black.cgColor
        resumeView.layer.cornerRadius = 2.0
        resumeView.layer.borderWidth = 0.50
        resumePriceView.layer.borderColor = UIColor.black.cgColor
        resumePriceView.layer.cornerRadius = 2.0
        resumePriceView.layer.borderWidth = 0.50
        currencyView.layer.borderColor = UIColor.black.cgColor
        currencyView.layer.cornerRadius = 2.0
        currencyView.layer.borderWidth = 0.50
        priceView.layer.borderColor = UIColor.black.cgColor
        priceView.layer.cornerRadius = 2.0
        priceView.layer.borderWidth = 0.50
        priceLbl.text = "0.0"
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    
    func  initialSetUp() {
        if isGetValue {
         resumeTittleLbl.text = valueObj.title
            resumePriceLbl.text = valueObj.price
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Btn Action
    @IBAction func closeBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }

    @IBAction func commonBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        switch sender.tag {
        case 10:
         commonOptionPicker(selectArray: symbolArray,button:currencyBtn )
        break
        case 11:
            if currencyBtn.currentTitle == "Select Currency" {
                AlertController.alert(title: "Please select currency.")
            }
            else {
                payPalConfiguration(price: self.priceLbl.text!,code: self.code)
            }
        break
        default:
        break
        }
    }

    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any> ,button:UIButton){
        if selectArray.count == 0
        {
            
        }else
        {
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
            if button == self.currencyBtn{
                button.setTitle(string, for: .normal)
                let id = self.codeArray[i]
                if i == self.codeArray.count-1
                {
                    self.priceLbl.text = self.resumePriceLbl.text
                    button.setTitle(string , for: .normal)
                    self.code = self.codeArray[i]
                    }else {
                    button.setTitle(string , for: .normal)
                    self.code = self.codeArray[i]
                    self.callApiForConvertCurrency(codeId: id, price: self.valueObj.price)
                }
            }
        }
        }
    }

    //MARK:- Paypal Configuration
    func payPalConfiguration(price:String,code:String)
    {
        payPalConfig.acceptCreditCards = true
        let payablePrice : Float = Float(price)!
        let roundUpPRice = Int(round(payablePrice))
        
        payPalConfig.merchantName = "Excellot"//Give your company name here.
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        //This is the language in which your paypal sdk will be shown to users.
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        //Here you can set the shipping address. You can choose either the address associated with PayPal account or different address. We’ll use .both here.
        payPalConfig.payPalShippingAddressOption = .both;
        let item1 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "\(roundUpPRice)"), withCurrency: code, withSku: "job+portal")
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items) //This is the total price of all the items
        // Optional: include payment details
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: 0, withTax: 0)
        let total = subtotal //This is the total price including shipping and ta
        let payment = PayPalPayment(amount: total, currencyCode: code, shortDescription: "Excellot", intent: .sale)
        payment.items = items
        let email : String = NSUSERDEFAULT.value(forKey: kUserEmail) as! String
        payment.payeeEmail = email
        payment.paymentDetails = paymentDetails
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn’t be processable, and you’d want
            // to handle that here.
            print("Payment not processalbe: (payment)")
        }
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
     
        paymentViewController.dismiss(animated: true, completion: nil)
        print("PayPal Payment cancel ")
        
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {

        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            let dict : Dictionary = completedPayment.confirmation
            let responseDict : Dictionary = dict["response"] as! Dictionary<String,AnyObject>
            let amount  = completedPayment.amount
            var status : String = responseDict.validatedValue("state", expected: "" as AnyObject) as! String
            var payerStatus = ""
            let email : String = completedPayment.payeeEmail!
            if status == "approved"{
                status = "completed"
                payerStatus = "VERIFIED"
            }else {
                 status = "pending"
                payerStatus = "UNVERIFIED"
            }

            self.callApiForSuccessPayment(trans_id:responseDict.validatedValue("id", expected: "" as AnyObject) as! String , status: status, total: "\(amount)",payeeEmail: email,payerStatus:payerStatus)
        })
    }
    
    //MARK:-  WEB API
    func callApiGetCurrency()
    {
        let paramDict = Dictionary<String, Any>()
        ServiceHelper.request(paramDict, method: .get, apiName: kGetCurrency, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            
                            for i in data
                            {
                                let dict : Dictionary = i as! Dictionary<String,AnyObject>
                                
                                let  country: String = dict.validatedValue("country", expected: "" as AnyObject) as! String
                                self.countryArray.append(country)
                                let  currency: String = dict.validatedValue("currency", expected: "" as AnyObject) as! String  
                                self.currencyArray.append(currency)
                                let  code: String = dict.validatedValue("code", expected: "" as AnyObject) as! String
                                self.codeArray.append(code)
                                let  symbol: String = dict.validatedValue("symbol", expected: "" as AnyObject) as! String + "(" + (dict.validatedValue("country", expected: "" as AnyObject) as! String) + ")"
                                self.symbolArray.append(symbol)
                            }
                            
                        } else
                        {
                        }
                    }
                }
            }
        }
    }
    
    func callApiForConvertCurrency(codeId: String ,price : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["cur"] = codeId
        paramDict["price"] = price
        
        ServiceHelper.request(paramDict, method: .post, apiName: kConvertCurrency, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            let message : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            self.priceLbl.text = message.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range:nil)
                            
                        }
                        else {
                            
                        }
                        
                    }
                }
            }
        }
    }

    func callApiForSuccessPayment(trans_id:String,status:String,total:String,payeeEmail:String,payerStatus:String)
    {
        var paramDict = Dictionary<String, Any>()

        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["txn_id"] = trans_id
        paramDict["resume_title"] = valueObj.title
        paramDict["resume_id"] = valueObj.resume_id
        paramDict["Total"] = total
        paramDict["SubTotal"] = total
        paramDict["Tax"] = "0"
        paramDict["Payment_state"] = status
        paramDict["PaymentMethod"] = "paypal"
        paramDict["PayerStatus"] = payerStatus
        paramDict["PayerMail"] = payeeEmail

        ServiceHelper.request(paramDict, method: .post, apiName: kResumeOrder, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            
                            let message : String = response.validatedValue("message", expected: false as AnyObject) as! String
                            AlertController.alert(title: message, message: "", buttons: ["OK"], tapBlock: { (alert, index) in
                               self.dismiss(animated: true, completion: nil)
                                self.delegate?.isFromPaypal()
                            })
                        }
                        else {
                            
                        }
                    }
                }
            }
        }
    }

}
