//
//  KeySkillsViewController.swift
//  Job Portal
//
//  Created by nile on 29/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

enum skillsLangEnumType {
    case keySkill,Language
}

import UIKit
protocol passValue : class {
    
    func passKeySkills(valueArray:Array<String>,valueIDarray:Array<String>)
}


protocol passLangValue : class {

    func passLang(langValueArray:Array<String>,langIdArray:Array<String>)

}


class KeySkillsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var skilllangType:skillsLangEnumType?
    
    @IBOutlet weak var mainView: UIView!
    var keySkillArray = [UserInfo]()
    var languageArray = [UserInfo]()
    var delegate1 : passLangValue?
    var delegate : passValue?
    var valueArray = Array<String>()
    
    var valueIDArray = Array<String>()
    
    var previousValues = Array<String>()
    var langValueArray = Array<String>()
    var langValueIDArray = Array<String>()

    var langPreviousArray = Array<String>()
    
    @IBOutlet weak var keySkillTableViewOutlet: UITableView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        keySkillTableViewOutlet.estimatedRowHeight = 45.0
        keySkillTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        self.navigationController?.navigationBar.isHidden = true
        mainView.center = self.view.center
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 5
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 4
        
        
        if  (skilllangType == skillsLangEnumType.keySkill) {
            callApiGetSelectKeySkill()
        }
        else {
            callApiGetLanguage()
            
        }
        self.hideKeyboardWhenTappedAround()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK :- Buttton Action
    @IBAction func closeActionBtn(_ sender: UIButton) {
        
        if  (skilllangType == skillsLangEnumType.keySkill){
           
            dismiss(animated: false, completion: nil)
            
            var arr  = Array<String>()
            var arrID  = Array<String>()

            for i in self.valueArray
            {
                if i != ""
                {
                    arr.append(i)
                }
            }
            
            for j in self.valueIDArray
            {
                if j != ""
                {
                    arrID.append(j)
                }
            }
            
            self.delegate?.passKeySkills(valueArray: arr,valueIDarray: arrID)
        }

        else {
            
            dismiss(animated: false, completion: nil)
            
            var arr  = Array<String>()
            var arrID  = Array<String>()

            for i in self.langValueArray
            {
                if i != ""
                {
                    arr.append(i)
                }
            }
            
            for j in self.langValueIDArray
            {
                if j != ""
                {
                    arrID.append(j)
                }
            }
            self.delegate1?.passLang(langValueArray: arr,langIdArray: arrID)
        }
    }
    
    // Mark:- TableViewDelegate    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (skilllangType == skillsLangEnumType.keySkill) {
            
            return keySkillArray.count
        }
        else {
            return languageArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KeySkillTableViewCell", for: indexPath) as! KeySkillTableViewCell
        
        if (skilllangType == skillsLangEnumType.keySkill) {
            
            let obj = keySkillArray[indexPath.row]
            cell.keyskillLbl.text =  obj.keySkills
            cell.selectButton.tag = indexPath.row+1000
            cell.selectButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            let _ = (obj.isCheck == "0") ? (cell.selectButton.isSelected = false) : (cell.selectButton.isSelected = true)
            
        }
            
        else {
            let obj = languageArray[indexPath.row]
            cell.keyskillLbl.text =  obj.language
            cell.selectButton.tag = indexPath.row+1000
            cell.selectButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            let _ = (obj.isCheck == "0") ? (cell.selectButton.isSelected = false) : (cell.selectButton.isSelected = true)
            
        }
        return cell
    }
    
    //MARK :- Button Action   
    
    @objc func buttonAction(_sender:UIButton) {
        
        if (skilllangType == skillsLangEnumType.keySkill)  {
            

            _sender.isSelected = !_sender.isSelected
            view.endEditing(true)

            if _sender.isSelected{

                let index = _sender.tag - 1000
                let obj = self.keySkillArray[index]
                obj.isCheck = "1"
                self.keySkillArray[index] = obj
                let button : UIButton = view.viewWithTag(_sender.tag) as! UIButton
                button.isSelected = true
                self.valueArray[index] = obj.keySkills
                self.valueIDArray[index] = obj.keySkillsID
            }
            else{
                let index = _sender.tag - 1000
                let obj = self.keySkillArray[index]
                obj.isCheck = "0"
                self.keySkillArray[index] = obj
                let button : UIButton = view.viewWithTag(_sender.tag) as! UIButton
                self.valueArray[index] = ""
                self.valueIDArray[index] = ""
                button.isSelected = false
            }
        }
            
        else {
            
            _sender.isSelected = !_sender.isSelected
            view.endEditing(true)
            if _sender.isSelected
            {
                let index = _sender.tag - 1000
                let obj = self.languageArray[index]
                obj.isCheck = "1"
                self.languageArray[index] = obj
                let button : UIButton = view.viewWithTag(_sender.tag) as! UIButton
                button.isSelected = true
                self.langValueArray[index] = obj.language
                self.langValueIDArray[index] = obj.languageValueID
            }else{

                let index = _sender.tag - 1000
                let obj = self.languageArray[index]
                obj.isCheck = "0"
                self.languageArray[index] = obj
                let button : UIButton = view.viewWithTag(_sender.tag) as! UIButton
                self.langValueArray[index] = ""
                self.langValueIDArray[index] = ""
                button.isSelected = false
            }
        }
    }
    
    //MARK:- WEB API
    func callApiGetSelectKeySkill()
    {
        let paramDict = Dictionary<String, Any>()
        ServiceHelper.request(paramDict, method: .get, apiName: kEducationSkill, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                
                if let response = result as? Dictionary<String, AnyObject> {

                    let responseCode = code
                    
                    if (Int(responseCode) == 200) {
                        
                          let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        
                        if status == true {
                  let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                        
                        for i in data
                        {
                            let userObj = UserInfo()
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            userObj.keySkills = dict.validatedValue("title", expected: "" as AnyObject) as! String
                            userObj.keySkillsID = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            userObj.isCheck = "0"
                            self.keySkillArray.append(userObj)
                        }
                        
                        for i in self.keySkillArray{
                            self.valueArray.append("")
                            self.valueIDArray.append("")
                        }
                        
                        if self.previousValues.count > 0{
                            
                            // self.keySkillArray.removeAll()
                            for j in 0..<self.previousValues.count
                            {
                                
                                let obj = self.previousValues[j]
                                
                                for i in 0..<self.keySkillArray.count
                                {
                                    let val = self.keySkillArray[i]
                                    if val.keySkills == obj
                                    {
                                        val.isCheck = "1"
                                        self.valueIDArray[i] = val.keySkillsID
                                        self.valueArray[i] = val.keySkills
                                        self.keySkillArray[i] = val
                                    }
                                }
                            }
                        }
                        
                        self.keySkillTableViewOutlet.reloadData()
                        
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    }
        
    func callApiGetLanguage()
    {
        let paramDict = Dictionary<String, Any>()
        ServiceHelper.request(paramDict, method: .get, apiName: kGetLang, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                
                if let langSkill : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    
                    if (Int(responseCode) == 200) {
                        
                        for i in langSkill
                        {
                            let userObj = UserInfo()
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            userObj.language = dict.validatedValue("name", expected: "" as AnyObject) as! String
                            userObj.languageValueID = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            userObj.isCheck = "0"
                            self.languageArray.append(userObj)
                        }

                        for i in self.languageArray{
                            self.langValueArray.append("")
                            self.langValueIDArray.append("")
                        }

                        if self.langPreviousArray.count > 0{
                            
                            for j in 0..<self.langPreviousArray.count
                            {
                                
                                let obj = self.langPreviousArray[j]
                                
                                for i in 0..<self.languageArray.count
                                {
                                    let val = self.languageArray[i]
                                    if val.language == obj
                                    {
                                        val.isCheck = "1"
                                        self.langValueArray[i] = val.language
                                        self.langValueIDArray[i] = val.languageValueID
                                        self.languageArray[i] = val
                                    }
                                }
                            }
                        }
                        self.keySkillTableViewOutlet.reloadData()
                    } else
                    {
                    }
                }
            }
        }
    }
    
}
