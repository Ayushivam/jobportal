//
//  ViewJobViewController.swift
//  Job Portal
//
//  Created by nile on 31/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

protocol GetListDelegate: class {
    func passSaveJobData()
}



class ViewJobViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    var ViewJobDataArray = [UserInfo]()

    var titleArray = Array<String>()
   
    var getValueArray = Array<String>()
    @IBOutlet weak var viewJobTableViewOutlet: UITableView!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    @IBOutlet weak var companyLocationLbl: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!
    
    @IBOutlet weak var jobTypeTittleLbl: UILabel!
    
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var applyBtn: UIButton!
    var jobID = ""
    var emailAddress = ""
    var skillValue = ""
    var empID = ""
    var myJob = ""
    var addMyJobs = Bool()

    weak var delegate: GetListDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        self.navigationController?.navigationBar.isHidden = true
        companyNameLbl.text = ""
        companyLocationLbl.text = ""
        
        companyLogoImageView.image = nil
        self.jobID = "\(jobID)"
        self.empID = "\(empID)"
         callApiGetViewJob(jobID : jobID)
        applyBtn.layer.cornerRadius = 5.0
        viewJobTableViewOutlet.estimatedRowHeight = 54
        viewJobTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        
        if !addMyJobs {
            self.heartBtn.isHidden = true
            
        }
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup() {
        if addMyJobs {
            self.heartBtn.isHidden = false

        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK:- Button Action
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.delegate?.passSaveJobData()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func mailButtonAction(_ sender: UIButton) {

        emailPopUp()
        
    }
    
    @IBAction func heartButtonAction(_ sender: UIButton) {
        view.endEditing(true)
        if myJob == "0" {
            self.myJob = "1"
            heartBtn.setImage(#imageLiteral(resourceName: "fillHeart"), for: .normal)
            callApiGetSaveJob(jobID: jobID )
        }
        
        else {
            self.myJob = "0"
            
            callApiForDeleteMyJob(jobID:jobID)
        }
    }
    
    func emailPopUp(){
        
        let alertController = UIAlertController(title: "Enter Email", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Share Job", style: .default, handler: { alert -> Void in
            let valueText = alertController.textFields![0] as UITextField
            if valueText.text == ""
            {
                self.validationPopup(message: "Please enter email.")
                return
            }  else if !(valueText.text?.isEmail)!
            {
                self.validationPopup(message: "Please enter valid email address.")
                return
            }
            
                        self.emailAddress =  valueText.text!
                        self.callApiForSharedJob(jobID: self.jobID, emailAddress: valueText.text!)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email"
            textField.keyboardType = .emailAddress
            self.present(alertController, animated: true, completion: nil)
        })
    }
    
    func validationPopup(message:String)
    {
        
        AlertController.alert(title: "", message: message , buttons: ["OK"]) { (alert, index) in
            self.emailPopUp()
        }
    }
    
    @IBAction func applyButtonAction(_ sender: UIButton) {
        callApiForApplyJob(jobID: self.jobID, empID: self.empID)
    }
    
    //MARK:- TableView Delegates

//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return titleArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "ViewjobTableViewCell", for: indexPath) as! ViewjobTableViewCell
        let a = titleArray[indexPath.row]
        let b = getValueArray[indexPath.row]

        cell.tittleLabel.text = a
        cell.getDataLabel.text = b
        return cell
    }
    
    
//MARK :- WEB API
 func callApiGetViewJob(jobID : String)
 {
    var paramDict = Dictionary<String,Any>()
    let userid : String = NSUSERDEFAULT.value(forKey: kUserID) as! String
    paramDict["user_id"] = userid
    paramDict["job_id"] = jobID

    ServiceHelper.request(paramDict, method: .post, apiName: kViewJobs, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool

                        if status == true {

                            if (response.validatedValue("message", expected: "" as AnyObject) as! String == "Success") {

                                let dict : Dictionary = response.validatedValue("data", expected: "" as AnyObject) as! Dictionary<String,AnyObject>
                                self.companyNameLbl.text = (dict.validatedValue("company", expected: "" as AnyObject) as! String)
                                self.companyLocationLbl.text = (dict.validatedValue("job_location", expected: "" as AnyObject) as! String)
                                self.jobTypeTittleLbl.text = dict.validatedValue("job_type", expected: "" as AnyObject) as? String
                                self.getValueArray.append(dict.validatedValue("job_type", expected: "" as AnyObject) as! String)
                                self.getValueArray.append(dict.validatedValue("job_sub_type", expected: "" as AnyObject) as! String)
                                self.getValueArray.append(dict.validatedValue("job_location", expected: "" as AnyObject) as! String)
                                self.getValueArray.append(dict.validatedValue("posted_date", expected: "" as AnyObject) as! String)
                                self.getValueArray.append(dict.validatedValue("job_desc", expected: "" as AnyObject) as! String)
                                self.getValueArray.append(dict.validatedValue("qlf_required", expected: "" as AnyObject) as! String)
                                self.getValueArray.append(dict.validatedValue("skill_required", expected: "" as AnyObject) as! String)

                                var companyLogo : String   = (dict.validatedValue("logo", expected: "" as AnyObject) as! String)
                                let _ = (companyLogo  == "") ? (companyLogo = "") : (companyLogo = companyLogo)
                                self.companyLogoImageView.sd_setImage(with: URL(string: companyLogo), placeholderImage: UIImage(named:"no-image"), options: SDWebImageOptions(rawValue: 0))
                                
                                self.titleArray = ["Job Type :","Job Sub Type :","Job Location :","Posted Date :","Job Description :","Qualification Required :","Skills Required :"]
                                self.viewJobTableViewOutlet.reloadData()
                                let  jobApply : String   = dict.validatedValue("job_applied", expected: "" as AnyObject) as! String
                                
                                if jobApply ==  "1"
                                {

                                    self.applyBtn.setTitle("Applied", for: .normal)
                                    self.applyBtn.isUserInteractionEnabled = false
                                    
                                    if self.addMyJobs {
                                        self.heartBtn.isHidden = true
                                        
                                    }
                                   // self.heartBtn.isHidden = true

                                }
                                else {
                                    self.applyBtn.setTitle("Apply", for: .normal)
                                    self.applyBtn.isUserInteractionEnabled = true
                                   // self.heartBtn.isHidden = false
                                    if self.addMyJobs {
                                        self.heartBtn.isHidden = false
                                        
                                    }
                                    
                                }
                                
                                
                                
                                 self.myJob  = dict.validatedValue("my_job", expected: "" as AnyObject) as! String
                                
                                if self.myJob == "1" {
                                    self.heartBtn.setImage(#imageLiteral(resourceName: "fillHeart"), for: .normal)
                                }
                                else {
                                self.heartBtn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)

                                }
                            }
                        }
                        else if status == false {

                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }

    func callApiForSharedJob(jobID : String , emailAddress : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["job_id"] = jobID
        paramDict["email"] = emailAddress
        ServiceHelper.request(paramDict, method: .post, apiName: kSharedJobs, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                        else if status == false {
                            
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
  
    
    func callApiForApplyJob(jobID : String , empID : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["job_id"] = jobID
        paramDict["emp_id"] = empID
        
        ServiceHelper.request(paramDict, method: .post, apiName: kApply, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            
                            AlertController.alert(title: "", message: "Applied for job successfully", buttons: ["OK"], tapBlock: { (alert, index) in
                                self.applyBtn.setTitle("Applied", for: .normal)
                                self.applyBtn.isUserInteractionEnabled = false
                                
                                if self.addMyJobs {
                                    self.heartBtn.isHidden = true
                                    
                                }
                              //  self.heartBtn.isHidden = true
                            })
                            
                           // AlertController.alert(title: "Applied for job successfully")
                         
                            
                        }
                        else if status == false {
                            
                        }
                        else {
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
    
    func callApiGetSaveJob(jobID : String)
    {
        var paramDict = Dictionary<String,Any>()
        let userid : String = NSUSERDEFAULT.value(forKey: kUserID) as! String
        paramDict["user_id"] = userid
        paramDict["job_id"] = jobID
        
        ServiceHelper.request(paramDict, method: .post, apiName: kSaveJobs, hudType: .smoothProgress) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)

                        }
                        else if status == false {
                            
                            // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                            
                        }
                        else {
                            
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
}
    
    
    
    func callApiForDeleteMyJob(jobID: String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["job_id"] = jobID
        
        
        ServiceHelper.request(paramDict, method: .post, apiName: kDeleteMyJob, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            
                            AlertController.alert(title: "", message:response.validatedValue("message", expected: "" as AnyObject)as! String, buttons: ["OK"], tapBlock: { (alert, index) in
                                self.heartBtn.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                            })
                            
                            
                        }

                    }
                    
                }
            }
        }
    }
}
