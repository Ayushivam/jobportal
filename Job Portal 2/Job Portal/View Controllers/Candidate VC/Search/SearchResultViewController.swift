//
//  SearchResultViewController.swift
//  Job Portal
//
//  Created by nile on 30/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class SearchResultViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,GetListDelegate,getJobPassValueDelegate{
    func passGetJobDataData(keyword: String, jobId: String, subJobId: String, countryId: String, stateId: String, cityId: String) {
        callApiGetJob(jobkeyWord : keyword ,jobId : jobId , subJobId : subJobId, countyId : countryId, stateId : stateId, cityId : cityId)
    }
    
    func passSaveJobData() {
    callApiGetJob( jobkeyWord : jobKeyValue, jobId: jobTypeID, subJobId: subJobTypeID, countyId: countryID, stateId: stateID, cityId: cityID)
    }
    

    
    
    @IBOutlet weak var noDataFoundLabel: UILabel!
    @IBOutlet weak var searchResultTableViewOutlet: UITableView!

    @IBOutlet weak var mainView: UIView!
     var  jobKeyValue = ""
     var jobTypeID = ""
     var  subJobTypeID = ""
     var  countryID = ""
     var stateID = ""
     var  cityID = ""
     var jobKeyValuepopOP = ""
     var getJobListlistArray = [UserInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        self.navigationController?.navigationBar.isHidden = true
        self.jobKeyValue = "\(jobKeyValue)"
        self.jobTypeID = "\(jobTypeID)"
        self.subJobTypeID = "\(subJobTypeID)"
        self.countryID = "\(countryID)"
        self.stateID = "\(stateID)"
        self.cityID = "\(cityID)"
        
        callApiGetJob( jobkeyWord : jobKeyValue, jobId: jobTypeID, subJobId: subJobTypeID, countyId: countryID, stateId: stateID, cityId: cityID)
        self.noDataFoundLabel.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Delegate Method
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 187.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getJobListlistArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell", for: indexPath) as! SearchResultTableViewCell
        let obj = getJobListlistArray[indexPath.row]
        let _ = (obj.myJob == 1) ? (cell.heartButton.setImage(#imageLiteral(resourceName: "fillHeart"), for: .normal)) : (cell.heartButton.setImage(#imageLiteral(resourceName: "heart"), for: .normal))
        cell.companyName.text = obj.companyName
        cell.jobTittleLabel.text = obj.jobTitle
        cell.stateLabel.text = obj.sName 
        cell.viewJobButton.tag = indexPath.row+1000
        cell.urlButton.tag = indexPath.row+1500
        cell.heartButton.tag = indexPath.row+2100
        cell.urlButton.addTarget(self, action: #selector(urlButtonAction), for: .touchUpInside)
        cell.viewJobButton.addTarget(self, action: #selector(viewJobButtonAction), for: .touchUpInside)
        cell.heartButton.addTarget(self, action: #selector(saveJobButtonAction), for: .touchUpInside)
        cell.companyLogoImageView.sd_setImage(with: URL(string: obj.companyLogo), placeholderImage: UIImage(named:"no-image"), options: SDWebImageOptions(rawValue: 0))
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: obj.startDate)
        
        if date != nil{
            let diffInDays : Int = Calendar.current.dateComponents([.day], from: date!, to: Date()).day!
            cell.postedJobLablel.text = "Posted " + "\(diffInDays)" + " days ago"
        }
        else {
            cell.postedJobLablel.text = ""
        }
        
        return cell
    }

    @objc func urlButtonAction(_sender:UIButton) {
        
            let index = _sender.tag - 1500
           let URLobj = self.getJobListlistArray[index]
           let url = URLobj.url
        if let url = URL(string: "\(url)")
        {
            print(url)
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    @objc func viewJobButtonAction(_sender:UIButton) {
       
        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            let index  = _sender.tag-1000
            let viewObj = self.getJobListlistArray[index]
            let jobId = viewObj.jobID
            let empId = viewObj.eID
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "ViewJobViewController") as! ViewJobViewController
            newViewController.delegate = self
            newViewController.jobID = jobId
            newViewController.empID = empId
            navigationController?.pushViewController(newViewController, animated: true)
        }else {
            AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                
                if index == 1{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    @objc func saveJobButtonAction(_sender:UIButton) {
        if let _ = NSUSERDEFAULT.value(forKey: kUserID)
        {
            view.endEditing(true)
            let index  = _sender.tag-2100
            let saveJobObj = self.getJobListlistArray[index]
            let jobId = saveJobObj.jobID
            if saveJobObj.myJob == 0
            {
                saveJobObj.myJob = 1
                self.getJobListlistArray[index] = saveJobObj
                let heartButton : UIButton = view.viewWithTag(_sender.tag) as! UIButton
                heartButton.setImage(#imageLiteral(resourceName: "fillHeart"), for: .normal)
                callApiGetSaveJob(jobID: jobId )
            }
            else {
            }
        }else {
            AlertController.alert(title: kLoginFirst, message: "", buttons: ["Cancel","OK"]) { (alert, index) in
                
                if index == 1{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    //Mark : - Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func filterBtnAction(_ sender: UIButton) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        popOverVC.providesPresentationContextTransitionStyle = true
       popOverVC.delegate = self
        popOverVC.definesPresentationContext = true
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        popOverVC.view.backgroundColor = UIColor.init(white : 0.10, alpha: 0.5)
        self.present(popOverVC, animated: true, completion: nil)

    }
    
    //MARK :- WEB API
    func callApiGetJob(jobkeyWord : String ,jobId : String , subJobId : String, countyId : String, stateId : String, cityId : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        paramDict["jobkeyword"] = jobkeyWord
        paramDict["job_type"] = jobId
        paramDict["sub_job"] = subJobId
        paramDict["country"] = countyId
        paramDict["state"] = stateId
        paramDict["city"] = cityId
        ServiceHelper.request(paramDict, method: .get, apiName:kGetJobs , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }

            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == false {
                            self.getJobListlistArray.removeAll()
                            self.searchResultTableViewOutlet.reloadData()

                                self.noDataFoundLabel.isHidden = false
                        }
                        else if status == true
                        {
                            self.getJobListlistArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.companyName = dict.validatedValue("company_name", expected: "" as AnyObject) as! String
                                userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String
                                userObj.sName = dict.validatedValue("s_name", expected: "" as AnyObject) as! String + ","+(dict.validatedValue("c_name" , expected: "" as AnyObject) as! String)
                                userObj.activePlan = dict.validatedValue("active_plan", expected: "" as AnyObject) as! String
                                userObj.planType = dict.validatedValue("plan_type", expected: "" as AnyObject) as! String
                                userObj.viewLimit =  dict.validatedValue("view_limit", expected: "" as AnyObject) as! String
                                userObj.jobID = dict.validatedValue("j_id", expected: "" as AnyObject) as! String
                                userObj.jobType =  dict.validatedValue("job_type", expected: "" as AnyObject) as! String
                                userObj.eID = dict.validatedValue("e_id", expected: "" as AnyObject) as! String
                                userObj.companyLogo = dict.validatedValue("company_logo", expected: "" as AnyObject) as! String
                                userObj.startDate = dict.validatedValue("start_date", expected: "" as AnyObject) as! String
                                userObj.createdDate = dict.validatedValue("created_date", expected: "" as AnyObject) as! String
                                userObj.myJob = dict.validatedValue("my_job", expected: false as AnyObject) as! Int
//                                let _ = (userObj.companyLogo  == "") ? (userObj.companyLogo = "") : (userObj.companyLogo = "https://www.niletechinnovations.com/projects/jobportal/uploads/company_logo/" + userObj.companyLogo )

                                userObj.url = dict.validatedValue("url", expected: "" as AnyObject) as! String
                                let arr = userObj.url.components(separatedBy: ":")
                                if arr.count == 1
                                {
                                    userObj.url = "https://" + userObj.url
                                }
                                
                                self.getJobListlistArray.append(userObj)
                            }
                             self.searchResultTableViewOutlet.reloadData()
                            
                            if self.getJobListlistArray.count == 0 {
                                self.noDataFoundLabel.isHidden = true
                            }
                            else {
                                self.noDataFoundLabel.isHidden = true
                            }
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetSaveJob(jobID : String)
    {
        var paramDict = Dictionary<String,Any>()
        let userid : String = NSUSERDEFAULT.value(forKey: kUserID) as! String
        paramDict["user_id"] = userid
        paramDict["job_id"] = jobID
        
        ServiceHelper.request(paramDict, method: .post, apiName: kSaveJobs, hudType: .smoothProgress) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            
                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
                            self.view.makeToast(MSG)
                        }
                        else if status == false {
                            
                           // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)

                        }
                        else {
                            
                            //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
//    func callApiForDeleteMyJob(jobID: String,tag:Int)
//    {
//        var paramDict = Dictionary<String, Any>()
//        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
//        paramDict["job_id"] = jobID
//
//
//        ServiceHelper.request(paramDict, method: .post, apiName: kDeleteMyJob, hudType: .default) { (result, error, code) in
//            if let error = error {
//                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
//            }
//            else {
//                if let response = result as? Dictionary<String, AnyObject> {
//                    let responseCode = code
//                    if (Int(responseCode) == 200) {
//                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
//                        if status == true{
//                            let MSG : String = response.validatedValue("message", expected: "" as AnyObject) as! String
//                            self.view.makeToast(MSG)
//                            let heartButton : UIButton = self.view.viewWithTag(tag) as! UIButton
//                            heartButton.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
//
//                        }
//
//
//                    }
//
//                }
//            }
//        }
//    }
}
