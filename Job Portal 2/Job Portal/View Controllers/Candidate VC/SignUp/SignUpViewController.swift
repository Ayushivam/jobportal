//
//  SignUpViewController.swift
//  Job Portal
//
//  Created by nile on 08/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var jobTypeView: UIView!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var phoneNoTextField: PaddedTextField!
    @IBOutlet weak var selectCountryBtn: UIButton!
    @IBOutlet weak var selectJobTypeBn: UIButton!
    @IBOutlet weak var passwordTextField: PaddedTextField!
    @IBOutlet weak var emailAddressTextField: PaddedTextField!
    @IBOutlet weak var lastNameTextField: PaddedTextField!
    @IBOutlet weak var firstNameTextField: PaddedTextField!
    @IBOutlet weak var bottomView: UIView!
    var selectJobArray = Array<String>()
    var selectCountryArray = Array<String>()
    var selectIDArray = Array<String>()
    var selectJobIDArray = Array<String>()
    var otpValue = String()
    var emailValue = String()
    var jobIdString = ""
    var countryIdString = ""
    @IBOutlet weak var eyeBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- INITIAL SETUP
    func initialSetUp() {
        self.navigationController?.navigationBar.isHidden = true
        bottomView.center = self.view.center
        bottomView.layer.shadowRadius = 4
        firstNameTextField.layer.borderWidth = 0.50
        firstNameTextField.layer.borderColor = UIColor.black.cgColor
        firstNameTextField.layer.cornerRadius = 2.0
        lastNameTextField.layer.borderWidth = 0.50
        lastNameTextField.layer.borderColor = UIColor.black.cgColor
        lastNameTextField.layer.cornerRadius = 2.0
        emailAddressTextField.layer.borderWidth = 0.50
        emailAddressTextField.layer.borderColor = UIColor.black.cgColor
        emailAddressTextField.layer.cornerRadius = 2.0
        emailAddressTextField.keyboardType = .emailAddress
        passwordTextField.layer.borderWidth = 0.50
        passwordTextField.layer.borderColor = UIColor.black.cgColor
        passwordTextField.layer.cornerRadius = 2.0
        passwordTextField.isSecureTextEntry = true
        jobTypeView.layer.borderWidth = 0.50
        jobTypeView.layer.borderColor = UIColor.black.cgColor
        jobTypeView.layer.cornerRadius = 2.0
        countryView.layer.borderWidth = 0.50
        countryView.layer.borderColor = UIColor.black.cgColor
        countryView.layer.cornerRadius = 2.0
        phoneNoTextField.layer.borderWidth = 0.50
        phoneNoTextField.layer.borderColor = UIColor.black.cgColor
        phoneNoTextField.layer.cornerRadius = 2.0
        phoneNoTextField.keyboardType = .numberPad
        signUpBtn.layer.cornerRadius = 2.0
        self.hideKeyboardWhenTappedAround()
        callApiGetSelectJobType()
        callApiGetSelectCountry()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Action Button
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func commonBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        switch sender.tag {
        case 11:
            commonOptionPicker(selectArray: selectJobArray,button: selectJobTypeBn)
            break
            
        case 12:
            commonOptionPicker(selectArray: selectCountryArray,button: selectCountryBtn)
            
            break
        default:
            break
        }
    }
    
    @IBAction func signUpBtnAction(_ sender: UIButton) {
        
        if firstNameTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter first name.")
        }
            
        else if lastNameTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter last name.")
        }
            
        else if emailAddressTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter email address.")
        }
            
        else if !(emailAddressTextField.text?.isEmail)!{
            AlertController.alert(title: "Please enter valid email address.")
        }
            
        else if passwordTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter password.")
        }
            
        else if selectJobTypeBn.currentTitle == "Select Job Type" {
            AlertController.alert(title: "Please select job type.")
        }
            
        else if selectCountryBtn.currentTitle == "Select Country" {
            AlertController.alert(title: "Please select country.")
        }
            
        else {
            callApiForRegistration()
        }
    }
    
    @IBAction func eyeBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            passwordTextField.isSecureTextEntry = false
        }
        else {
            passwordTextField.isSecureTextEntry = true
        }
    }

    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){
        if selectArray.count == 0
        {
            
        }else
        {
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
            
            if button == self.selectCountryBtn{
                button.setTitle(string, for: .normal)
                let id = self.selectIDArray[i]
                self.countryIdString = id
            }
            
            
            else if button == self.selectJobTypeBn{
                button.setTitle(string, for: .normal)
                let id = self.selectJobIDArray[i]
                self.jobIdString = id
            }
            
            
            else {
                button.setTitle(string, for: .normal)
            }
        }
        }
    }
        
    
    
    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 20
            {
                return false
            }
            break
        case 101:
            if str.length > 20
            {
                return false
            }
            
            
        case 102:
            if str.length > 60
            {
                return false
            }
            
            
        case 103:
            if str.length > 15
            {
                return false
            }
            
            
        case 104:
            if str.length > 12
            {
                return false
            }
        default:
            break
        }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    //MARK :- WEB API
    func callApiGetSelectJobType()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetSelectJobType, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                
                if let jobArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in jobArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let jobName : String = dict.validatedValue("job_type", expected: "" as AnyObject) as! String
                            self.selectJobArray.append(jobName)
                            let jobid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.selectJobIDArray.append(jobid)
                        }
                        
                    }
                    else
                    {
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetSelectCountry()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetCountry, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                
                
                if let countryArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in countryArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let countryName : String = dict.validatedValue("country_name", expected: "" as AnyObject) as! String
                            let countryid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.selectCountryArray.append(countryName)
                            self.selectIDArray.append(countryid)
                            
                        }
                        
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiForRegistration()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["first_name"] = firstNameTextField.text
        paramDict["last_name"] = lastNameTextField.text
        paramDict["email"] = emailAddressTextField.text
        paramDict["password"] = passwordTextField.text
        paramDict["job_type"] = jobIdString
        paramDict["country"] = countryIdString
        paramDict["contact"] = phoneNoTextField.text
        
        
        ServiceHelper.request(paramDict, method: .post, apiName: kCandidateSignUp, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            self.otpValue = response.validatedValue("verified_code", expected: "" as AnyObject)  as! String
                            print(self.otpValue)
                            self.emailValue = response.validatedValue("email", expected: "" as AnyObject)  as! String
                            print(self.emailValue)
                            AlertController.alert(title: "", message:result?.value(forKey: "message") as! String , buttons: ["OK"], tapBlock: { (alert, index) in
                                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                let newViewController = storyboard.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
                                newViewController.VerifyOTP = verifyOTPCheckEnmType.CandidateOTP
                                newViewController.email = self.emailValue
                                NSUSERDEFAULT.set(self.emailAddressTextField.text, forKey: kUserEmail)
                                newViewController.otp = self.otpValue
                                self.navigationController?.pushViewController(newViewController, animated: true)
                            })
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
}
