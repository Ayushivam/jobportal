//
//  NotificationDetailsViewController.swift
//  Job Portal
//
//  Created by nile on 05/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class NotificationDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var getNotificationDetailsArray = [UserInfo]()
    var getStatus = ""
    @IBOutlet weak var noDataFoundLbl: UILabel!
    @IBOutlet weak var notificationTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        noDataFoundLbl.isHidden = true
        callApiGetNotificationDetails()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- TableView Delegate Datasorce
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getNotificationDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notificationCell = tableView.dequeueReusableCell(withIdentifier: "notificationDetailsTableViewCell") as! notificationDetailsTableViewCell
        let notifyObj = getNotificationDetailsArray[indexPath.row]
        notificationCell.jobTitleLbl.text = notifyObj.jobTitle
        notificationCell.companyNameLbl.text = notifyObj.companyName
        notificationCell.statusLbl.text = "Status:" + " " + self.getStatus
        notificationCell.companyLogoImageView.sd_setImage(with: URL(string: notifyObj.companyLogo), placeholderImage: UIImage(named:"no-image"), options: SDWebImageOptions(rawValue: 0))
        notificationCell.urlBtn.tag = indexPath.row+1500
        
        notificationCell.urlBtn.addTarget(self, action: #selector(urlButtonAction), for: .touchUpInside)
        
        return notificationCell
    }
    
    @objc func urlButtonAction(_sender:UIButton) {
        
        let index = _sender.tag - 1500
        let URLobj = self.getNotificationDetailsArray[index]
        let url = URLobj.url
        if let url = URL(string: "\(url)")
        {
            print(url)
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    //MARK :- WEB API Functions
    func callApiGetNotificationDetails()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["user_id"] = NSUSERDEFAULT.value(forKey: kUserID)
        
        ServiceHelper.request(paramDict, method: .post, apiName:kNotificationDetails , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == false {
                            self.getNotificationDetailsArray.removeAll()
                            self.notificationTableView.reloadData()
                            
                            self.noDataFoundLbl.isHidden = false
                        }
                        else if status == true
                        {
                            self.getNotificationDetailsArray.removeAll()
                            let data : Array = response.validatedValue("message", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.companyName = dict.validatedValue("company_name", expected: "" as AnyObject) as! String
                                
                                userObj.companyLogo = dict.validatedValue("company_logo", expected: "" as AnyObject) as! String
                                userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String
                                userObj.status = dict.validatedValue("status", expected: "" as AnyObject) as! String
                                
                                if   userObj.status == "1"{
                                    self.getStatus = "Accepted"
                                }
                                    
                                else {
                                    self.getStatus = "Rejected"
                                }
                                
                                let _ = (userObj.companyLogo  == "") ? (userObj.companyLogo = "") : (userObj.companyLogo = userObj.companyLogo )
                                userObj.url = dict.validatedValue("company_website", expected: "" as AnyObject) as! String
                                let arr = userObj.url.components(separatedBy: ":")
                                if arr.count == 1
                                {
                                    userObj.url = "https://" + userObj.url
                                }
                                self.getNotificationDetailsArray.append(userObj)
                            }
                            self.notificationTableView.reloadData()
                            
                            if self.getNotificationDetailsArray.count == 0 {
                                self.noDataFoundLbl.isHidden = true
                            }
                            else {
                                self.noDataFoundLbl.isHidden = true
                            }
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
}
