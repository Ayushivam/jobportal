//
//  ForgotpasswordViewController.swift
//  Job Portal
//
//  Created by nile on 15/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class ForgotpasswordViewController: UIViewController,UITextFieldDelegate {

    var isFromEmployer = false
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var emailAddressTextField: PaddedTextField!
    var apiKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        emailAddressTextField.layer.borderWidth = 0.50
        emailAddressTextField.layer.cornerRadius = 2.0
        emailAddressTextField.layer.borderColor = UIColor.black.cgColor
        emailAddressTextField.keyboardType = .emailAddress
        emailAddressTextField.tag = 100
        submitBtn.layer.cornerRadius = 5.0
        emailAddressTextField.returnKeyType = .done
         let _ = (isFromEmployer) ? (apiKey = kEmplyerForgotPassord) : (apiKey = kForgotPassword)
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {

        navigationController?.popViewController(animated: true)

    }
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        if emailAddressTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter email ID.")
        }
        else if !(emailAddressTextField.text?.isEmail)!{
            AlertController.alert(title: "Please enter valid email ID.")
        }
        else {
            self.callApiForForgotPassword()
        }
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 50
            {
                return false
            }
            break
            
        default:
            break
        }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
    }

    //MARK:- Web Api Method
    func callApiForForgotPassword()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["email"] = emailAddressTextField.text
        
        ServiceHelper.request(paramDict, method: .post, apiName: apiKey, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            AlertController.alert(title: "", message:result?.value(forKey: "message") as! String , buttons: ["OK"], tapBlock: { (alert, index) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                        else {
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
}

