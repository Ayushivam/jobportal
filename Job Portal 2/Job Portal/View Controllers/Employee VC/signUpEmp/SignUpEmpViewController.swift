//
//  SignUpEmpViewController.swift
//  Job Portal
//
//  Created by nile on 26/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class SignUpEmpViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var eyeBtn: UIButton!
    @IBOutlet weak var signUPBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var comapnyNameTextField: PaddedTextField!
    @IBOutlet weak var comapnyWebsiteTextField: PaddedTextField!
    @IBOutlet weak var addressTextField: PaddedTextField!
    @IBOutlet weak var firstNameTextField: PaddedTextField!
    @IBOutlet weak var emailAdressTextField: PaddedTextField!
    @IBOutlet weak var passwordTextField: PaddedTextField!
    @IBOutlet weak var phoneNoTextField: PaddedTextField!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    var countryArray = Array<String>()
    var countryIDArray = Array<String>()
    var stateArray = Array<String>()
    var stateIDArray = Array<String>()
    var cityArray = Array<String>()
    var cityIDArray = Array<String>()
    @IBOutlet weak var stateBtn: UIButton!
    var otpValue = String()
    var emailValue = String()

    override func viewDidLoad() {
        super.viewDidLoad()
         initialSetUp()
        callApiGetSelectCountry()
        // Do any additional setup after loading the view.
    }
    
    func initialSetUp() {
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
      
        firstNameTextField.layer.borderWidth = 0.50
        firstNameTextField.layer.borderColor = UIColor.black.cgColor
        firstNameTextField.layer.cornerRadius = 2.0
        comapnyNameTextField.layer.borderWidth = 0.50
        comapnyNameTextField.layer.borderColor = UIColor.black.cgColor
        comapnyNameTextField.layer.cornerRadius = 2.0
        comapnyWebsiteTextField.layer.borderWidth = 0.50
        comapnyWebsiteTextField.layer.borderColor = UIColor.black.cgColor
        comapnyWebsiteTextField.layer.cornerRadius = 2.0
        addressTextField.layer.borderWidth = 0.50
        addressTextField.layer.borderColor = UIColor.black.cgColor
        addressTextField.layer.cornerRadius = 2.0
        countryView.layer.borderWidth = 0.50
        countryView.layer.borderColor = UIColor.black.cgColor
        countryView.layer.cornerRadius = 2.0
        stateView.layer.borderWidth = 0.50
        stateView.layer.borderColor = UIColor.black.cgColor
        stateView.layer.cornerRadius = 2.0
        cityView.layer.borderWidth = 0.50
        cityView.layer.borderColor = UIColor.black.cgColor
        cityView.layer.cornerRadius = 2.0
        emailAdressTextField.layer.borderWidth = 0.50
        emailAdressTextField.layer.borderColor = UIColor.black.cgColor
        emailAdressTextField.layer.cornerRadius = 2.0
        passwordTextField.layer.borderWidth = 0.50
        passwordTextField.layer.borderColor = UIColor.black.cgColor
        passwordTextField.layer.cornerRadius = 2.0
        phoneNoTextField.layer.borderWidth = 0.50
        phoneNoTextField.layer.borderColor = UIColor.black.cgColor
        phoneNoTextField.layer.cornerRadius = 2.0
        emailAdressTextField.keyboardType = .emailAddress
        passwordTextField.isSecureTextEntry = true
        phoneNoTextField.keyboardType = .phonePad
        signUPBtn.layer.cornerRadius = 2.0

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func eyyeBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected ==  true
        {
            passwordTextField.isSecureTextEntry = false
        }
        
        else {
            
            passwordTextField.isSecureTextEntry = true
        }
    }
    
    
    @IBAction func signUpBtnAction(_ sender: UIButton) {
        
        if comapnyNameTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter company name.")
        }
            
        else if comapnyWebsiteTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter website.")
        }
            
        else if addressTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter address.")
        }
           
        else if countryBtn.currentTitle == "Select Country" {
            AlertController.alert(title: "Please select country.")
        }
            
            
        else if stateBtn.currentTitle == "Select State" {
            AlertController.alert(title: "Please select state.")
        }
            
        else if cityBtn.currentTitle == "Select City" {
            AlertController.alert(title: "Please select city.")
        }
            
        else if firstNameTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter first name.")
        }
            
            
        else if emailAdressTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter email address.")
        }
        else if !(emailAdressTextField.text?.isEmail)!{
            AlertController.alert(title: "Please enter valid email address.")
        }
            
        else if passwordTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter password.")
        }
            
         
        else if phoneNoTextField.text?.count == 0 {
            AlertController.alert(title: "Please enter phone number.")
        }
        else {
            callApiForRegistration()
        }
    }
    
    @IBAction func commonBtnActionForDesJob(_ sender: UIButton) {
        
        view.endEditing(true)
        switch sender.tag {
            
        case 11:
            commonOptionPicker(selectArray: countryArray,button: countryBtn)
            
            break
        case 12:
            
            if countryBtn.currentTitle == "Select Country" {
                AlertController.alert(title: "Please select country.")
            }
            else {
             commonOptionPicker(selectArray: stateArray,button: stateBtn)
            }
            break
        case 13:
            
            if stateBtn.currentTitle == "Select State" {
                AlertController.alert(title: "Please select state.")
            }
            else { commonOptionPicker(selectArray: cityArray,button: cityBtn)
            }
            break
        default:
            break
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
            
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    
    //MARK:- FOR  DATA PICKER
    func commonOptionPicker(selectArray : Array<Any>,button:UIButton){
        if selectArray.count == 0
        {
            
        }else
        {
        RPicker.sharedInstance.selectOption(dataArray: selectArray as? Array<String>, selectedIndex: 0) { (string, i) in
          
            if button == self.countryBtn{
            button.setTitle(string, for: .normal)
            let id = self.countryIDArray[i]
            self.callApiGetState(countryid: id)
            }

            else if button == self.stateBtn
            {
                button.setTitle(string, for: .normal)
                let id = self.stateIDArray[i]
                self.callApiGetCity(cityId: id)
            }else {
                 button.setTitle(string, for: .normal)
            }
        }
        }
    }

    //MARK:- WEB API
    func callApiGetSelectCountry()
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetCountry, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                
                
                if let countryArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in countryArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let countryName : String = dict.validatedValue("country_name", expected: "" as AnyObject) as! String
                            self.countryArray.append(countryName)
                            let countryid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.countryIDArray.append(countryid)
                            
                        }
                        
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
        
    }
    
    func callApiGetState(countryid : String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["country_id"] = countryid
        
        ServiceHelper.request(paramDict, method: .post, apiName: KGetState, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let stateArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        self.stateBtn.setTitle("Select State", for: .normal)
                        self.cityBtn.setTitle("Select City", for: .normal)
                        
                        self.stateArray.removeAll()
                        self.stateIDArray.removeAll()
                        for i in stateArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let stateName : String = dict.validatedValue("state_name", expected: "" as AnyObject) as! String
                            self.stateArray.append(stateName)
                            let stateId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.stateIDArray.append(stateId)
                            
                        }
                        
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetCity(cityId:String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["state_id"] = cityId
        
        ServiceHelper.request(paramDict, method: .post, apiName: KGetCity, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let cityArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        self.cityBtn.setTitle("Select City", for: .normal)
                        
                        self.cityArray.removeAll()
                        self.cityIDArray.removeAll()
                        for i in cityArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let cityName : String = dict.validatedValue("city_name", expected: "" as AnyObject) as! String
                            self.cityArray.append(cityName)
                            let cityId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.cityIDArray.append(cityId)
                        }
                        
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiForRegistration()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["company_name"] = comapnyNameTextField.text
        paramDict["url"] = comapnyWebsiteTextField.text
        paramDict["address"] = addressTextField.text
        paramDict["country"] = countryBtn.currentTitle
        paramDict["state"] = stateBtn.currentTitle
        paramDict["city"] = cityBtn.currentTitle
        paramDict["name"] = firstNameTextField.text
        paramDict["email"] = emailAdressTextField.text
        paramDict["password"] = passwordTextField.text
        paramDict["contact_no"] = phoneNoTextField.text

        
        ServiceHelper.request(paramDict, method: .post, apiName: kEmpSignUp, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            self.otpValue = response.validatedValue("verified_code", expected: "" as AnyObject)  as! String
                            print(self.otpValue)
                            self.emailValue = response.validatedValue("email", expected: "" as AnyObject)  as! String
                            print(self.emailValue)
                            AlertController.alert(title: "", message:result?.value(forKey: "message") as! String , buttons: ["OK"], tapBlock: { (alert, index) in
                                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                let newViewController = storyboard.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
                                newViewController.VerifyOTP = verifyOTPCheckEnmType.EmployeeOTP

                                newViewController.email = self.emailValue
                                newViewController.otp = self.otpValue
                                self.navigationController?.pushViewController(newViewController, animated: true)
                            })
                        }
                        else {
                            
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }

}
