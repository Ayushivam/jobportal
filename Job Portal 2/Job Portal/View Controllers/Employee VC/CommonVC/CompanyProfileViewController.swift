//
//  CompanyProfileViewController.swift
//  Job Portal
//
//  Created by nile on 29/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class CompanyProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var picker = UIImagePickerController()

    //@IBOutlet weak var removePhotoBtn: UIButton!
    @IBOutlet weak var nameTextField: PaddedTextField!
    @IBOutlet weak var phoneNumberTextField: PaddedTextField!
    @IBOutlet weak var emailTextField: PaddedTextField!
    @IBOutlet weak var designationTextField: PaddedTextField!
    @IBOutlet weak var linkedInView: UIView!
    @IBOutlet weak var instagramView: UIView!
    @IBOutlet weak var twitterView: UIView!
    @IBOutlet weak var fbView: UIView!
    @IBOutlet weak var linkedInTextField: PaddedTextField!
    @IBOutlet weak var instagramTextField: PaddedTextField!
    @IBOutlet weak var twitterTextField: PaddedTextField!
    @IBOutlet weak var faceBookTextField: PaddedTextField!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var yearView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var yearBtn: UIButton!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var websiteTextField: PaddedTextField!
    @IBOutlet weak var officeAddressTextField: PaddedTextField!
    @IBOutlet weak var companyNameTextField: PaddedTextField!
//    @IBOutlet weak var uploadBtn: UIButton!
//    @IBOutlet weak var chooseFileBtn: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    var countryArray = Array<String>()
    var countryIDArray = Array<String>()
    var stateArray = Array<String>()
    var stateIDArray = Array<String>()
    var cityArray = Array<String>()
    var cityIDArray = Array<String>()

    var countryIdString = ""
    var stateIdString = ""
    var cityIDString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
          initialSetUp()
        
        // Do any additional setup after loading the view.
    }

    func initialSetUp()
    {
        callApiGetEmpProfile()
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
//        uploadBtn.layer.cornerRadius = 2.0
//        chooseFileBtn.layer.cornerRadius = 2.0
        companyNameTextField.layer.cornerRadius = 2.0
        companyNameTextField.layer.borderWidth = 0.50
        companyNameTextField.layer.borderColor = UIColor.black.cgColor
        websiteTextField.layer.cornerRadius = 2.0
        websiteTextField.layer.borderWidth = 0.50
        websiteTextField.layer.borderColor = UIColor.black.cgColor
        officeAddressTextField.layer.cornerRadius = 2.0
        officeAddressTextField.layer.borderWidth = 0.50
        officeAddressTextField.layer.borderColor = UIColor.black.cgColor
        yearView.layer.cornerRadius = 2.0
        yearView.layer.borderWidth = 0.50
        yearView.layer.borderColor = UIColor.black.cgColor
        countryView.layer.cornerRadius = 2.0
        countryView.layer.borderWidth = 0.50
        countryView.layer.borderColor = UIColor.black.cgColor
        stateView.layer.cornerRadius = 2.0
        stateView.layer.borderWidth = 0.50
        stateView.layer.borderColor = UIColor.black.cgColor
        cityView.layer.cornerRadius = 2.0
        cityView.layer.borderWidth = 0.50
        cityView.layer.borderColor = UIColor.black.cgColor
        descriptionTextView.layer.cornerRadius = 2.0
        descriptionTextView.layer.borderWidth = 0.50
        descriptionTextView.layer.borderColor = UIColor.black.cgColor
        fbView.layer.cornerRadius = 2.0
        fbView.layer.borderWidth = 0.50
        fbView.layer.borderColor = UIColor.black.cgColor
        twitterView.layer.cornerRadius = 2.0
        twitterView.layer.borderWidth = 0.50
        twitterView.layer.borderColor = UIColor.black.cgColor
        linkedInView.layer.cornerRadius = 2.0
        linkedInView.layer.borderWidth = 0.50
        linkedInView.layer.borderColor = UIColor.black.cgColor
        instagramView.layer.cornerRadius = 2.0
        instagramView.layer.borderWidth = 0.50
        instagramView.layer.borderColor = UIColor.black.cgColor
        nameTextField.layer.cornerRadius = 2.0
        nameTextField.layer.borderWidth = 0.50
        nameTextField.layer.borderColor = UIColor.black.cgColor
        emailTextField.layer.cornerRadius = 2.0
        emailTextField.layer.borderWidth = 0.50
        emailTextField.layer.borderColor = UIColor.black.cgColor
        phoneNumberTextField.layer.cornerRadius = 2.0
        phoneNumberTextField.layer.borderWidth = 0.50
        phoneNumberTextField.layer.borderColor = UIColor.black.cgColor
        designationTextField.layer.cornerRadius = 2.0
        designationTextField.layer.borderWidth = 0.50
        designationTextField.layer.borderColor = UIColor.black.cgColor
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
        self.profileImageView.clipsToBounds = true
        self.profileImageView.layer.borderWidth = 1.0
        self.profileImageView.layer.borderColor = UIColor.black.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    

    func formattedDateFromStringForDate(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        
        return nil
    }

    
    func callApiGetEmpProfile()
    {
        var paramDict = Dictionary<String,Any>()
        let empID : String = NSUSERDEFAULT.value(forKey: kEmpID) as! String
        paramDict["emp_id"] = empID
        
        ServiceHelper.request(paramDict, method: .post, apiName: kGetEmpProfile, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == true {
                            let data : Dictionary = response.validatedValue("data", expected: "" as AnyObject) as!  Dictionary<String, AnyObject>
                            self.companyNameTextField.text! = data.validatedValue("company_name", expected: "" as AnyObject) as! String
                            self.websiteTextField.text! = data.validatedValue("company_website", expected: "" as AnyObject) as! String
                            self.officeAddressTextField.text! = data.validatedValue("address", expected: "" as AnyObject) as! String
                            self.phoneNumberTextField.text! = data.validatedValue("phone_no", expected: "" as AnyObject) as! String
                            self.nameTextField.text! = data.validatedValue("conatact_name", expected: "" as AnyObject) as! String
                            self.emailTextField.text! = data.validatedValue("email", expected: "" as AnyObject) as! String
                            self.designationTextField.text! = data.validatedValue("designation", expected: "" as AnyObject) as! String
                            self.descriptionTextView.text! = data.validatedValue("description", expected: "" as AnyObject) as! String
                            self.faceBookTextField.text! = data.validatedValue("facebook", expected: "" as AnyObject) as! String
                            self.twitterTextField.text! = data.validatedValue("twitter", expected: "" as AnyObject) as! String
                            self.instagramTextField.text! = data.validatedValue("google", expected: "" as AnyObject) as! String
                            self.linkedInTextField.text! = data.validatedValue("linkedin", expected: "" as AnyObject) as! String
                            var userImage : String = data.validatedValue("company_logo", expected: "" as AnyObject) as! String
                            let _ = (userImage  == "") ? (userImage = "") : (userImage = userImage)
                            self.profileImageView.sd_setImage(with: URL(string: userImage), placeholderImage: UIImage(named:"user"), options: SDWebImageOptions(rawValue: 0))
                            self.yearBtn.setTitle(data.validatedValue("since", expected: "" as AnyObject ) as? String, for: .normal)

                            
                            let countryId : String = data.validatedValue("country_id", expected: "" as AnyObject) as! String
                            self.countryIdString = countryId
                            
                            self.callApiGetSelectCountry(countryID:countryId)
                            let stateId : String = data.validatedValue("state_id", expected: "" as AnyObject) as! String
                            self.stateIdString = stateId
                            
                            let cityID : String = data.validatedValue("city_id", expected: "" as AnyObject) as! String
                            self.cityIDString = cityID
                            self.callApiGetState(countryid: countryId, isgetProfile: true, stateId: stateId,cityId: cityID)
                            
                            
                        }
                        else if status == false {
                            
                        }
                        
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    func callApiGetSelectCountry(countryID:String)
    {
        let paramDict = Dictionary<String, Any>()
        
        ServiceHelper.request(paramDict, method: .get, apiName: kGetCountry, hudType: .default) { (result, error, code) in
            if let error = error {
                
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let countryArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        for i in countryArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let countryName : String = dict.validatedValue("country_name", expected: "" as AnyObject) as! String
                            self.countryArray.append(countryName)
                            let countryid : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.countryIDArray.append(countryid)
                        }
                        for j in (0..<self.countryIDArray.count) {
                            let obj = self.countryIDArray[j]
                            if countryID == obj {
                                self.countryBtn.setTitle(self.countryArray[j], for: .normal)
                            }
                        }
                    } else
                    {
                        // AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetState(countryid : String,isgetProfile:Bool,stateId:String,cityId:String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["country_id"] = countryid
        
        ServiceHelper.request(paramDict, method: .post, apiName: KGetState, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let stateArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        self.stateArray.removeAll()
                        self.stateIDArray.removeAll()
                        for i in stateArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let stateName : String = dict.validatedValue("state_name", expected: "" as AnyObject) as! String
                            self.stateArray.append(stateName)
                            let stateId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.stateIDArray.append(stateId)
                        }
                        
                        if isgetProfile{
                            
                            for j in (0..<self.stateIDArray.count) {
                                let obj = self.stateIDArray[j]
                                if  stateId == obj {
                                    self.stateBtn.setTitle(self.stateArray[j], for: .normal)
                                    self.callApiGetCity(stateId: stateId, isFromGetProfile: true,cityId:cityId)
                                }
                            }
                        }
                        else {
                            self.stateBtn.setTitle("Select State", for: .normal)
                            self.stateIdString = ""
                            self.cityBtn.setTitle("Select City", for: .normal)
                            self.cityIDString = ""
                        }
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiGetCity(stateId:String,isFromGetProfile:Bool,cityId:String)
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["state_id"] = stateId

        ServiceHelper.request(paramDict, method: .post, apiName: KGetCity, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let cityArray : Array = result as? Array<AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        self.cityArray.removeAll()
                        self.cityIDArray.removeAll()
                        for i in cityArray
                        {
                            let dict : Dictionary = i as! Dictionary<String,AnyObject>
                            let cityName : String = dict.validatedValue("city_name", expected: "" as AnyObject) as! String
                            self.cityArray.append(cityName)
                            let cityId : String = dict.validatedValue("id", expected: "" as AnyObject) as! String
                            self.cityIDArray.append(cityId)
                        }
                        if isFromGetProfile
                        {
                            for j in (0..<self.cityIDArray.count) {
                                let obj = self.cityIDArray[j]
                                if  cityId == obj {
                                    self.cityBtn.setTitle(self.cityArray[j], for: .normal)
                                }
                            }
                        }else {
                            self.cityBtn.setTitle("Select City", for: .normal)
                            self.cityIDString = ""
                        }
                    }else{
                        //AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
}
