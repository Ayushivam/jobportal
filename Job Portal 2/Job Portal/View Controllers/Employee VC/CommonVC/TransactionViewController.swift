//
//  TransactionViewController.swift
//  Job Portal
//
//  Created by nile on 11/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class TransactionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var tarnsactionListArray = [UserInfo]()
    @IBOutlet weak var transactionTableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        callApiGetTransaction()
        self.noDataLabel.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tarnsactionListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
        
        let obj = tarnsactionListArray[indexPath.row]
        cell.jobTittleLbl.text = obj.jobTitle
        cell.paymentDateLbl.text = obj.date
        cell.amountLbl.text = obj.amount
        cell.packageLbl.text = obj.package
        cell.statusLbl.text = obj.status
        cell.numberingLabel.text = String(format: "%li", indexPath.row + 1)

        return cell
    }
   
    
    //MARK :- WEB API
    func callApiGetTransaction()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["emp_id"] = NSUSERDEFAULT.value(forKey: kEmpID)
        
        ServiceHelper.request(paramDict, method: .post, apiName:kTransaction , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == false {
                            self.tarnsactionListArray.removeAll()
                            self.transactionTableView.reloadData()
                                self.noDataLabel.isHidden = false
                        }
                        else if status == true
                        {
                            self.tarnsactionListArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                             
                                
                                  userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String
                                 userObj.date = dict.validatedValue("payment_date", expected: "" as AnyObject) as! String
                                
                                userObj.amount = dict.validatedValue("amount", expected: "" as AnyObject) as! String
                                userObj.status = dict.validatedValue("plan_status", expected: "" as AnyObject) as! String
                                
                                    userObj.package = dict.validatedValue("package", expected: "" as AnyObject) as! String
                                
                                self.tarnsactionListArray.append(userObj)
                            }
                            self.transactionTableView.reloadData()
                            
                                                        if self.tarnsactionListArray.count == 0 {
                                                            self.noDataLabel.isHidden = true
                                                        }
                                                        else {
                                                            self.noDataLabel.isHidden = true
                                                        }
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
}
