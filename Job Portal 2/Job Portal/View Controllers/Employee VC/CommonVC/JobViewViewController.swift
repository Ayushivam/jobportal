//
//  JobViewViewController.swift
//  Job Portal
//
//  Created by nile on 11/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class JobViewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var noDataLabel: UILabel!

    var jobViewListArray = [UserInfo]()

    @IBOutlet weak var jobViewTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
      callApiGetMyJobView()
        self.noDataLabel.isHidden = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- BUTTON ACTION
  
    @IBAction func bckBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TABLEVIEW DELEGATE
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return jobViewListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobViewTableViewCell", for: indexPath) as! jobViewTableViewCell
        
        let obj = jobViewListArray[indexPath.row]
        cell.jobTitleLbl.text = obj.jobTitle
        cell.totalViewLbl.text = obj.totalView
        cell.leftBlnceLbl.text = obj.leftBlnce
        cell.numberingLabel.text = String(format: "%li", indexPath.row + 1)

        if obj.leftBlnce == "" {
            cell.leftBlnceLbl.text = "0"
        }
        
        cell.useBlnceLbl.text =  obj.totalUsedblnce
        cell.planExpLbl.text = obj.planExp
        cell.planStatusLbl.text = obj.planType
        return cell
    }
    
    //MARK :- WEB API
    func callApiGetMyJobView()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["emp_id"] = NSUSERDEFAULT.value(forKey: kEmpID)
        
        ServiceHelper.request(paramDict, method: .post, apiName:kJobView , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == false {
                            self.jobViewListArray.removeAll()
                            self.jobViewTableView.reloadData()
                            self.noDataLabel.isHidden = false
                        }
                        else if status == true
                        {
                            self.jobViewListArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String

                                userObj.totalView = dict.validatedValue("total_view", expected: "" as AnyObject) as! String + " View"
                                userObj.leftBlnce = dict.validatedValue("left_balance", expected: "" as AnyObject) as! String
                                if userObj.leftBlnce != ""
                                {
                                    var amount : Float = Float(userObj.leftBlnce)!
                                    amount = (amount*100).rounded()/100
                                    userObj.leftBlnce = "\(amount)"
                                }
                                userObj.usedBlnce  = dict.validatedValue("payment_value", expected: "" as AnyObject) as! String
                                if userObj.usedBlnce != ""
                                {
                                    var amount : Float = Float(userObj.usedBlnce)!
                                    amount = (amount*100).rounded()/100
                                    userObj.usedBlnce = "\(amount)"
                                }
                                userObj.planExp = dict.validatedValue("expired_date", expected: "" as AnyObject) as! String
                                userObj.planType = dict.validatedValue("status", expected: "" as AnyObject) as! String
                                userObj.totalUsedblnce = "\((userObj.usedBlnce as NSString).floatValue - (userObj.leftBlnce as NSString).floatValue)"
                                if userObj.totalUsedblnce != ""
                                {
                                    var amount : Float = Float(userObj.totalUsedblnce)!
                                    amount = (amount*100).rounded()/100
                                    userObj.totalUsedblnce = "\(amount)"
                                }
                                self.jobViewListArray.append(userObj)
                            }
                            self.jobViewTableView.reloadData()
                            if self.jobViewListArray.count == 0 {
                                self.noDataLabel.isHidden = true
                            }
                            else {
                                self.noDataLabel.isHidden = true
                            }
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
}
