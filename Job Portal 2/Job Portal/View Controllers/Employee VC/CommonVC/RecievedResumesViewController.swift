//
//  RecievedResumesViewController.swift
//  Job Portal
//
//  Created by nile on 11/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class RecievedResumesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var recievedResumesListArray = [UserInfo]()
    @IBOutlet weak var receivedResumeTableView: UITableView!
    
    @IBOutlet weak var noDataLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        callApiGetRecievedResumes()
        self.noDataLabel.isHidden = true
        receivedResumeTableView.estimatedRowHeight = 132
        receivedResumeTableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- BUTTON ACTION
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- TABLEVIEW DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recievedResumesListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "RecievedResumeTableViewCell", for: indexPath) as! RecievedResumeTableViewCell
        let obj = recievedResumesListArray[indexPath.row]
        cell.nameLbl.text = obj.cName
        cell.profileImageView.sd_setImage(with: URL(string: obj.profilePic), placeholderImage: UIImage(named:"no-image"), options: SDWebImageOptions(rawValue: 0))
        cell.companyNameLbl.text = obj.jobTitle
        cell.locationLbl.text = obj.state + "/" + obj.city
        if obj.status == "1"
        {
            cell.statusLbl.text = "Status: Accepted"
        }
        else if obj.status == "0" {
            cell.statusLbl.text = "Status: Rejected"
        }
        else {
            cell.statusLbl.text = "Status: In Progress"
        }

        return cell
    }
    
    //MARK :- WEB API
    func callApiGetRecievedResumes()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["emp_id"] = NSUSERDEFAULT.value(forKey: kEmpID)
        
        ServiceHelper.request(paramDict, method: .post, apiName:kReceivedResumes , hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        
                        if status == false {
                            self.recievedResumesListArray.removeAll()
                            self.receivedResumeTableView.reloadData()
                            
                                self.noDataLabel.isHidden = false
                        }
                        else if status == true
                        {
                            self.recievedResumesListArray.removeAll()
                            let data : Array = response.validatedValue("data", expected: "" as AnyObject) as! Array<Any>
                            for i in data
                            {
                                let userObj = UserInfo()
                                let dict :Dictionary = i as! Dictionary<String,AnyObject>
                                userObj.companyName = dict.validatedValue("company_name", expected: "" as AnyObject) as! String
                                userObj.cName = dict.validatedValue("c_name", expected: "" as AnyObject) as! String
                                userObj.profilePic = dict.validatedValue("profile_pic", expected: "" as AnyObject) as! String
                                let _ = (userObj.profilePic == "") ? (userObj.profilePic = "") : (userObj.profilePic = userObj.profilePic)
                                userObj.state = dict.validatedValue("state_name", expected: "" as AnyObject) as! String
                                  userObj.city = dict.validatedValue("city_name", expected: "" as AnyObject) as! String
                                  userObj.jobTitle = dict.validatedValue("job_title", expected: "" as AnyObject) as! String
                                  userObj.status =  (dict.validatedValue("status", expected: "" as AnyObject) as! String)
                              
                                self.recievedResumesListArray.append(userObj)
                            }
                            self.receivedResumeTableView.reloadData()
                            
                                                        if self.recievedResumesListArray.count == 0 {
                                                            self.noDataLabel.isHidden = true
                                                        }
                                                        else {
                                                            self.noDataLabel.isHidden = true
                                                        }
                        }
                        
                    } else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    
}
