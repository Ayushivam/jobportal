//
//  EmployeeMenuViewController.swift
//  Job Portal
//
//  Created by Vibhuti Sharma on 15/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class EmployeeMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var imageArray = [#imageLiteral(resourceName: "policy"),#imageLiteral(resourceName: "protection"),#imageLiteral(resourceName: "question"),#imageLiteral(resourceName: "cpasswrd")]
    var nameArray =  ["TERMS & CONDITIONS","PRIVACY POLICY","FAQ","CHANGE PASSWORD"]
    
    @IBOutlet weak var menuTableViewOutlet: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.selectionStyle = .none
        cell.commonNameLbl.text = nameArray[indexPath.row]
        cell.commonImageView.image = imageArray[indexPath.row]
        cell.commonBtn.tag = indexPath.row+1001
        cell.commonBtn.addTarget(self, action: #selector(clickButtonAction), for: .touchUpInside)
        return cell
    }

    @objc func clickButtonAction(_sender : UIButton)
    {
        let tag = _sender.tag-1000

        if tag == 0 {
            
           // UIApplication.shared.openURL(NSURL(string: "https://www.niletechinnovations.com/projects/jobportal/training")! as URL)
        }

        else if tag == 1 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            staticVC.contentType = methodType.terms
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
            
        else if tag == 2 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            staticVC.contentType = methodType.privacy
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
            
        else if tag == 3 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let staticVC = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            staticVC.contentType = methodType.faq
            APPDELEGATE.navController?.pushViewController(staticVC, animated: true)
        }
            
        else if tag == 4 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let interviewVC = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            interviewVC.isfromEmployee = true
            APPDELEGATE.navController?.pushViewController(interviewVC, animated: true)
        }
    }
}
