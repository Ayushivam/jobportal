//
//  EmpDashboardViewController.swift
//  Job Portal
//
//  Created by nile on 29/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class EmpDashboardViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    @IBOutlet weak var dashboardCollectionViewOutlet: UICollectionView!

    var titleItem = ["Company Profile","Received Resumes","Transactions","Job Views"]
    var updatedItem = ["View Profile","Jobs","","Views"]
    var imageIcon = [#imageLiteral(resourceName: "company"),#imageLiteral(resourceName: "recievedresume"),#imageLiteral(resourceName: "trans"),#imageLiteral(resourceName: "docs")]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEmpCount(_:)), name: NSNotification.Name(rawValue: "updateEmpCount"), object: nil)

        self.hideKeyboardWhenTappedAround()
        if let _ = NSUSERDEFAULT.value(forKey: kEmpID)
        {
            callApiForDashboardData()
            
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func updateEmpCount(_ notification: NSNotification) {
        callApiForDashboardData()
    }
    
    
    
    //MARK :- Action Button
    @IBAction func logOUTBTNACTION(_ sender: UIButton) {
        
        let refreshAlert = UIAlertController(title: "Log Out", message: "Are you sure, you want to log out? ", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert .dismiss(animated: true, completion: nil)
            
        }))
        
            refreshAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in
                NSUSERDEFAULT.removeObject(forKey: kEmpID)
                self.navigationController?.popToRootViewController(animated: true)
            
        }))
        present(refreshAlert, animated: true, completion: nil)
    }

    //MARK:- Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellsAcross: CGFloat = 2
        var widthRemainingForCellContent = collectionView.bounds.width
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            let borderSize: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
            widthRemainingForCellContent -= borderSize + ((cellsAcross - 1) * flowLayout.minimumInteritemSpacing)
        }
        let cellWidth = widthRemainingForCellContent / cellsAcross
        return CGSize(width: cellWidth, height: cellWidth)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmpDashboardCollectionViewCell", for: indexPath) as! EmpDashboardCollectionViewCell
        cell.tittleLabel.text = titleItem[indexPath.row]
        cell.upddatedTitleLabel.text = updatedItem[indexPath.row]
        cell.commonImageView.image = imageIcon[indexPath.row]
        cell.commonButton.tag = indexPath.row+100
        cell.commonButton.addTarget(self, action: #selector(centerAction), for: .touchUpInside)


        return cell
    }
    
    //MARK :- Button Action For Collection View
    @objc func centerAction(_sender:UIButton) {
        
        let tag = _sender.tag-100
        if tag == 0{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newCompanyProfileVC = storyBoard.instantiateViewController(withIdentifier: "CompanyProfileViewController") as! CompanyProfileViewController
            APPDELEGATE.navController?.pushViewController(newCompanyProfileVC, animated: true)
        }

            
        else if tag == 1{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let receivedResumeVC = storyBoard.instantiateViewController(withIdentifier: "RecievedResumesViewController") as! RecievedResumesViewController
            APPDELEGATE.navController?.pushViewController(receivedResumeVC, animated: true)
        }
            
            
            
        else if tag == 2{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let transactionVC = storyBoard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            APPDELEGATE.navController?.pushViewController(transactionVC, animated: true)
        }
            
        
       else if tag == 3{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let jobViewVC = storyBoard.instantiateViewController(withIdentifier: "JobViewViewController") as! JobViewViewController
            APPDELEGATE.navController?.pushViewController(jobViewVC, animated: true)
        }
        
    }

    func callApiForDashboardData()
    {
        var paramDict = Dictionary<String, Any>()
        
        paramDict["emp_id"] = NSUSERDEFAULT.value(forKey: kEmpID)

        ServiceHelper.request(paramDict, method: .post, apiName: kEmpDashboard, hudType: .smoothProgress) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status  = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true{
                            let dataDict : Dictionary = response.validatedValue("data", expected: "" as AnyObject) as! Dictionary<String,AnyObject>
                            let receivedResumeCount = dataDict.validatedValue("recieved_resume", expected: "" as AnyObject) as! String
                            let total_jobs = dataDict.validatedValue("total_jobs", expected: "" as AnyObject) as! String
                            let total_view : String = dataDict["toatal_view"]!["total_view"] as! String
                            let total_Transaction : String = dataDict["total_txn"]!["total_transaction"] as! String
                            self.updatedItem.removeAll()
                            self.updatedItem = ["View Profile" , receivedResumeCount + " Jobs", total_Transaction, total_view + " Views"]
                            self.dashboardCollectionViewOutlet.reloadData()
                        }
                        else {
                        }
                    }
                }
            }
        }
    }
    

}
