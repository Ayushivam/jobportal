//
//  EmployerHomeViewController.swift
//  Job Portal
//
//  Created by Vibhuti Sharma on 15/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class EmployerHomeViewController: UIViewController {

    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var dashboardBtn: UIButton!
    @IBOutlet weak var heightContraints: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!

    var currentVC: UIViewController?
    var dashBoardVC: EmpDashboardViewController?
    var menuVC: EmployeeMenuViewController?
    
    var  baseNavController: UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        configureHeight()
    }

    func initialSetUp() {
        
        self.navigationController?.navigationBar.isHidden = true
        bottomView.center = self.view.center
        bottomView.backgroundColor = UIColor.white
        bottomView.layer.shadowColor = UIColor.lightGray.cgColor
        bottomView.layer.shadowOpacity = 5
        bottomView.layer.shadowOffset = CGSize.zero
        bottomView.layer.shadowRadius = 4
        
        baseNavController = UINavigationController()
        baseNavController?.isNavigationBarHidden = true
        let baseStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        dashBoardVC = baseStoryboard.instantiateViewController(withIdentifier: "EmpDashboardViewController") as? EmpDashboardViewController
        menuVC = baseStoryboard.instantiateViewController(withIdentifier: "EmployeeMenuViewController") as? EmployeeMenuViewController

        
        dashboardBtn.setImage(UIImage(named: "dashboardB"), for: .normal)
        topLbl.text = "DASHBOARD"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateEmpCount"), object: nil)
        self.display(dashBoardVC!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func replaceCurrentVC(withVC newVC: UIViewController) {
        currentVC?.willMove(toParentViewController: nil)
        currentVC?.view.removeFromSuperview()
        currentVC?.removeFromParentViewController()
        currentVC = newVC
        addChildViewController(currentVC!)
        containerView?.addSubview((currentVC?.view)!)
        currentVC?.view.frame = (containerView?.bounds)!
        currentVC?.didMove(toParentViewController: self)
    }
    
    func display(_ viewController: UIViewController) {
        baseNavController?.viewControllers = [viewController]
        baseNavController?.isNavigationBarHidden = true
        replaceCurrentVC(withVC: baseNavController!)
    }
    
    func configureHeight()
    {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                heightContraints.constant = 60
            //  print("iPhone 5 or 5S or 5C")
            case 1334:
                heightContraints.constant = 60
            //  print("iPhone 6/6S/7/8")
            case 1920, 2208:
                heightContraints.constant = 60
            //   print("iPhone 6+/6S+/7+/8+")
            case 2436:
                heightContraints.constant = 80
            //   print("iPhone X, Xs")
            case 2688:
                heightContraints.constant = 80
            //   print("iPhone Xs Max")
            case 1792:
                heightContraints.constant = 80
                print("iPhone Xr")
            default:
                break
                //   print("unknown")
            }
        }
    }
    
    @IBAction func logOUTBTNACTION(_ sender: UIButton) {
        let refreshAlert = UIAlertController(title: "Log Out", message: "Are you sure, you want to log out? ", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert .dismiss(animated: true, completion: nil)
            
        }))
        
    refreshAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in
    NSUSERDEFAULT.removeObject(forKey: kEmpID)
    self.navigationController?.popToRootViewController(animated: true)
        
        }))
        present(refreshAlert, animated: true, completion: nil)

    }
    
    @IBAction func commonActionBtn(_ sender: UIButton) {
        switch sender.tag {
        case 11:
            
            initialImage()
            dashboardBtn.setImage(UIImage(named: "dashboardB"), for: .normal)
            topLbl.text = "DASHBOARD"
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateEmpCount"), object: nil)

            self.display(dashBoardVC!)
            break
            
        case 12:
            initialImage()
            menuBtn.setImage(UIImage(named: "menuB"), for: .normal)
            topLbl.text = "MENU"
            self.display(menuVC!)
            break
        default:
            break
        }
    }
    
    func initialImage()
    {
        dashboardBtn.setImage(UIImage(named: "dashboard"), for: .normal)
        menuBtn.setImage(UIImage(named: "menu"), for: .normal)
    }
    
}
