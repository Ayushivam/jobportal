//
//  VerifyOTPViewController.swift
//  Job Portal
//
//  Created by nile on 23/10/18.
//  Copyright © 2018 nile. All rights reserved.
//


enum verifyOTPCheckEnmType {
    case CandidateOTP,EmployeeOTP
}
import UIKit
class VerifyOTPViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var verifyCodeTextField: PaddedTextField!
    @IBOutlet weak var topView: UIView!
    var email = ""
    var otp = ""
    var VerifyOTP : verifyOTPCheckEnmType?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        topView.center = self.view.center
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOpacity = 5
        topView.layer.shadowOffset = CGSize.zero
        topView.layer.shadowRadius = 4
        verifyCodeTextField.layer.borderWidth = 0.50
        verifyCodeTextField.layer.cornerRadius = 2.0
        verifyCodeTextField.layer.borderColor = UIColor.black.cgColor
        verifyCodeTextField.tag = 100
        verifyCodeTextField.delegate = self
        verifyCodeTextField.returnKeyType = .done
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func submitBtnAction(_ sender: UIButton) {
       
        
        if(VerifyOTP == verifyOTPCheckEnmType.EmployeeOTP)
        {
        if verifyCodeTextField.text?.count == 0{
            AlertController.alert(title: "Please enter your OTP")
        }
        else if   verifyCodeTextField.text == "\(otp)"
        {
            callApiForVerifyEMPOTP()
        }
            else
        {
            AlertController.alert(title: "Invalid OTP")
        }
        }
        
        
        else {
            if verifyCodeTextField.text?.count == 0{
                AlertController.alert(title: "Please enter your OTP.")
            }
            else if   verifyCodeTextField.text == "\(otp)"
            {
                callApiForVerifyOTP()
            }
            else
            {
                AlertController.alert(title: "Invalid OTP.")
            }
        }
    }
    
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 6
            {
                return false
            }
            break
       
        default:
            break
        }
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    //MARK:- Web Api Method
    func callApiForVerifyOTP()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["email"] = email

        ServiceHelper.request(paramDict, method: .post, apiName: kVerifyOTP, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }

            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            let userID : String = response.validatedValue("user_id", expected: "" as AnyObject) as! String
                            NSUSERDEFAULT.set(userID, forKey: kUserID)
                             NSUSERDEFAULT.set("0", forKey: kcheckLoginType)
                            AlertController.alert(title: "", message:result?.value(forKey: "message") as! String , buttons: ["OK"], tapBlock: { (alert, index) in
                                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                let newViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.navigationController?.pushViewController(newViewController, animated: true)
                            })
                        }
                        else {
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
    
    func callApiForVerifyEMPOTP()
    {
        var paramDict = Dictionary<String, Any>()
        paramDict["email"] = email
        
        ServiceHelper.request(paramDict, method: .post, apiName: kVerifyEMPOTP, hudType: .default) { (result, error, code) in
            if let error = error {
                AlertController.presentAlert("Error!", msgStr: error.localizedDescription, controller: self)
            }
                
            else {
                if let response = result as? Dictionary<String, AnyObject> {
                    let responseCode = code
                    if (Int(responseCode) == 200) {
                        
                        let status = response.validatedValue("status", expected: false as AnyObject) as! Bool
                        if status == true {
                            
                            let empID : String = response.validatedValue("emp_id", expected: "" as AnyObject) as! String
                             NSUSERDEFAULT.set(empID, forKey: kEmpID)
                             NSUSERDEFAULT.set("0", forKey: kcheckLoginType)
                            AlertController.alert(title: "", message:result?.value(forKey: "message") as! String , buttons: ["OK"], tapBlock: { (alert, index) in
                                let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                let newViewController = storyboard.instantiateViewController(withIdentifier: "EmpDashboardViewController") as! EmpDashboardViewController
                                self.navigationController?.pushViewController(newViewController, animated: true)
                            })
                        }
                        else {
                            AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                        }
                    }else{
                        AlertController.alert(title: response.validatedValue("message", expected: "" as AnyObject) as! String)
                    }
                }
            }
        }
    }
}
