
import UIKit

//API Names
let NSUSERDEFAULT = UserDefaults.standard
let kMSG = "message"
let kMSGEMP = "message"
let kUserID = "userID"
let kEmpID = "empID"
let kTrue = "success"
let kUserEmail = "userEmail"
let kcheckLoginType = "socialType"
let kRegisteruser = "register_user"
let kLogin = "login_user"
//Candidate Sign up
let kGetSelectJobType = "get_job_type"
let kGetCountry = "get_country"
let kCandidateSignUp = "sign_up"
let kEmpSignUp = "employer_signup"
//Verify Otp
let kVerifyOTP = "verify"
let kVerifyEMPOTP = "employer_verify"

//Resend OTP
let kResendOTP = "resent_code"
let kEMPResendOTP = "resent_code_employer"

//LOGIN
let kLoginForCandidate = "login"
let kLoginForEmployee = "employer_login"

//
let kGetLang = "get_language"
let KGetState =  "get_state_post"
let KGetCity =  "get_city_post"
let kGetSkill = "get_skill"
let kGetEducation = "get_education"
let kSelectSubJobType = "get_job_sub_type_post"
let kGetJobs = "get_jobs"
let kViewJobs = "view_job"
let kSharedJobs = "shareJob"
let kSaveJobs = "save_job"
let kMyJobList = "get_myjob_list"
let kApplyJobList = "get_apply_job_list"
let kApply = "apply_job"
let kDeleteMyJob = "delete_myjob"
let kDeleteAppliedJobs = "delete_applied_job"
let kGetListJobAlert = "jobalert"
let kForgotPassword = "candidate_forgot_password"
let kEmplyerForgotPassord = "emp_forgot_password"
let kViewProfile = "viewprofile"
let kFeautureEmployer = "featured_employers"
let kFeautureJobs = "featured_jobs"
let kGetCandidateProfile = "get_candidate_profile"
let kUploadProfilePhoto = "update_profile_pic"
let kUpdateCandidateProfile = "update_candidate_profile"
let kGetSpeciliztion = "get_cource_list"
let kAddEducation = "candidate_education"
let kAddProfessional = "save_pro_skill"
let kAddWorknExp = "add_work_exp"
let kGetCandidateEducation = "get_candidate_education"
let kDeleteEducation = "delete_education"
let kDeleteProfessional = "delete_professional"
let kDeleteExp = "delete_experience"
let kEducationSkill = "get_education_skill"
let kResumeService = "resume_services"
let kDeleteResume = "delete_resume"
let kGetCurrency = "get_currency"
let kConvertCurrency = "convert_currency"
let kResources  = "resources"
let kResumeOrder = "resume_order"
let kChangePassword = "candidatechangepassword"
let kInterviewService = "interview_prepration"
let kInterviewOrder = "interview_order"
let kNotificationDetails = "notification_details"
let kPrivacy = "privacy"
let kTerms = "terms"
let kFaqs = "faqs"
let kDashboard = "dashbord"
let kFacebookLogin = "facebook_login"
let kEmployeeChangePassword = "employer_change_password"

//MARK:- ALL EMPLOYEE API's

let kGetEmpProfile = "get_emp_profile"
let kUploadEmpProfilePhoto = "update_emp_profile_pic"
let kDeleteProfileImage = "delete_logo"
let kJobView = "jobs_view"
let kReceivedResumes = "received_resume"
let kTransaction = "jobtransactions"
let kEmpDashboard = "employer_dashboard"

// Paypal Id
// For development
//let kPaypalId = ""AXbmR8xoRvBbH0B-JVId1kRAgGtWBqPzTqiHw7ZG7jwwQrK9QrMzrsm1OmlH-voA2jwdr5a49ETtyQld""

// For Live
let kPaypalId = "ARpgZbV0_VtfAiKnxGuQyhFf-mQ_RRwBRZoEyXlv5mSIPVfGOoTD8srAPkRPgK7CH6APqQvEi3zcAdJ7"


class ApiConstants: NSObject {
}
