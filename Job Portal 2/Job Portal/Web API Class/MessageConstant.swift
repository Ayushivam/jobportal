//
//  MessageConstant.swift
//  Job Portal
//
//  Created by nile on 17/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit
let kWriteEmail  = "Please enter email address."
let kValidEmail = "Please enter valid email address."
let kEmptyPassword = "Please enter password."
let kLoginFirst = "Please login first."
