//
//  ProfessionalResumeTableViewCell.swift
//  Job Portal
//
//  Created by nile on 27/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class ProfessionalResumeTableViewCell: UITableViewCell {
    @IBOutlet weak var deleteBtn: UIButton!
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var skillLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
deleteBtn.layer.cornerRadius = 2.0
        editBtn.layer.cornerRadius = 2.0
        // Configure the view for the selected state
    }

}
