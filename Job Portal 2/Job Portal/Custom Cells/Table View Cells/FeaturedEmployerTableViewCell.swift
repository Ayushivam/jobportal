//
//  FeaturedEmployerTableViewCell.swift
//  Job Portal
//
//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class FeaturedEmployerTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var companyBtn: UIButton!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 4
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 3
        // Configure the view for the selected state
    }

}
