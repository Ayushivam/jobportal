

//
//  notificationDetailsTableViewCell.swift
//  Job Portal
//
//  Created by nile on 05/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class notificationDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var urlBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!

    @IBOutlet weak var jobTitleLbl: UILabel!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 4
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 3
        // Configure the view for the selected state
    }

}
