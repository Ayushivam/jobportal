//
//  professionalSkillListTableViewCell.swift
//  Job Portal
//
//  Created by nile on 16/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class professionalSkillListTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var skillLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
