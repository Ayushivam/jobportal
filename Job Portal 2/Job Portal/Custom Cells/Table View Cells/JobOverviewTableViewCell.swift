//
//  JobOverviewTableViewCell.swift
//  Job Portal
//
//  Created by nile on 16/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class JobOverviewTableViewCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var getDataLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      
        // Configure the view for the selected state
    }

}
