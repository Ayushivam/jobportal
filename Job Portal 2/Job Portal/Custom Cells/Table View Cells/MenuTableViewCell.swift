//
//  MenuTableViewCell.swift
//  Job Portal
//
//  Created by nile on 01/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var commonBtn: UIButton!
    @IBOutlet weak var commonNameLbl: UILabel!
    @IBOutlet weak var commonImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
