//
//  EducationListTableViewCell.swift
//  Job Portal
//
//  Created by nile on 16/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class EducationListTableViewCell: UITableViewCell {

    @IBOutlet weak var despLabel: UILabel!
    @IBOutlet weak var univsityLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var eduDegreeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
