//
//  MyJobsTableViewCell.swift
//  Job Portal
//
//  Created by nile on 31/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class MyJobsTableViewCell: UITableViewCell {

    @IBOutlet weak var heartButton: UIButton!
    @IBOutlet weak var urlButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var applyNowLAbel: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var viewJobBtn: UIButton!
    @IBOutlet weak var compnyNameLabel: UILabel!
    @IBOutlet weak var jobPostLabel: UILabel!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 4
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 3
        viewJobBtn.layer.cornerRadius = 5.0
        removeBtn.layer.cornerRadius = 5.0
        
        
        // Configure the view for the selected state
    }

}
