//
//  workandexpResumeTableViewCell.swift
//  Job Portal
//
//  Created by nile on 27/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class workandexpResumeTableViewCell: UITableViewCell {
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var monthnTillLabell: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var organization: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        deleteBtn.layer.cornerRadius = 2.0
        editBtn.layer.cornerRadius = 2.0
        // Configure the view for the selected state
    }

}
