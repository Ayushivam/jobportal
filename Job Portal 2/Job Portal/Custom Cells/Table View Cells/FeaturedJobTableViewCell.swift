//
//  FeaturedJobTableViewCell.swift
//  Job Portal
//
//  Created by nile on 02/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class FeaturedJobTableViewCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var   viewJobBtn: UIButton!
    @IBOutlet weak var dateLBL: UILabel!

    @IBOutlet weak var jobTitleLBL: UILabel!
    @IBOutlet weak var locationLBL: UILabel!
    @IBOutlet weak var contentLBL: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 4
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 3
        viewJobBtn.layer.cornerRadius = 5.0
        // Configure the view for the selected state
    }

}
