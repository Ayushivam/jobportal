//
//  ResumeServiceTableViewCell.swift
//  Job Portal
//
//  Created by nile on 15/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class ResumeServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var orderNowConstrints: NSLayoutConstraint!
    @IBOutlet weak var subTittleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var orderNowBtn: UIButton!
    @IBOutlet weak var dollarMoneyLabel: UILabel!
    @IBOutlet weak var resumeWritingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        mainView.layer.shadowColor = UIColor.lightGray.cgColor
//        mainView.layer.shadowOpacity = 4
//        mainView.layer.shadowOffset = CGSize.zero
//        mainView.layer.shadowRadius = 3

        // Configure the view for the selected state
    }

}
