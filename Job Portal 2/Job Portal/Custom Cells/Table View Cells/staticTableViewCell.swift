//
//  staticTableViewCell.swift
//  Job Portal
//
//  Created by nile on 06/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class staticTableViewCell: UITableViewCell {

    @IBOutlet weak var answerTitle: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
