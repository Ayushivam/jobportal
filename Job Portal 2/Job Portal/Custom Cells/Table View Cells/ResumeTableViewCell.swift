//
//  ResumeTableViewCell.swift
//  Job Portal
//
//  Created by nile on 22/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class ResumeTableViewCell: UITableViewCell {
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var universityNameLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var MainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        editButton.layer.cornerRadius = 2.0

        deleteButton.layer.cornerRadius = 2.0

        // Configure the view for the selected state
    }

}
