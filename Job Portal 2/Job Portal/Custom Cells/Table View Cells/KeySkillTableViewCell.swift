//
//  KeySkillTableViewCell.swift
//  Job Portal
//
//  Created by nile on 29/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class KeySkillTableViewCell: UITableViewCell {

    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var keyskillLbl: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
