//
//  SearchResultTableViewCell.swift
//  Job Portal
//
//  Created by nile on 30/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {
    @IBOutlet weak var companyName: UILabel!
    
    @IBOutlet weak var urlButton: UIButton!
    @IBOutlet weak var postedJobLablel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewJobButton: UIButton!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var gpsButton: UIButton!
    @IBOutlet weak var heartButton: UIButton!
    @IBOutlet weak var jobTittleLabel: UILabel!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 3
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 2
        mainView.layer.cornerRadius = 2.0
        viewJobButton.layer.borderWidth = 0.50
        viewJobButton.layer.cornerRadius = 5.0
        viewJobButton.layer.borderColor = (UIColor.init(red: 45.0/255, green: 85.0/255, blue: 151.0/255, alpha: 1.0).cgColor)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
//        mainView.layer.shadowColor = UIColor.lightGray.cgColor
//        mainView.layer.shadowOpacity = 3
//        mainView.layer.shadowOffset = CGSize.zero
//        mainView.layer.shadowRadius = 2
//        mainView.layer.cornerRadius = 2.0
//        viewJobButton.layer.borderWidth = 0.50
//        viewJobButton.layer.cornerRadius = 5.0
//        viewJobButton.layer.borderColor = (UIColor.init(red: 45.0/255, green: 85.0/255, blue: 151.0/255, alpha: 1.0).cgColor)

        // Configure the view for the selected state
    }

}
