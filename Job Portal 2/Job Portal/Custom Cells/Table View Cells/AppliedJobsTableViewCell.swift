//
//  AppliedJobsTableViewCell.swift
//  Job Portal
//
//  Created by nile on 01/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class AppliedJobsTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var jobPostTitle: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var viewJobBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 4
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 3
        viewJobBtn.layer.cornerRadius = 5.0
        removeBtn.layer.cornerRadius = 5.0
        // Configure the view for the selected state
    }

}
