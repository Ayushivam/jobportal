//
//  jobViewTableViewCell.swift
//  Job Portal
//
//  Created by nile on 11/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class jobViewTableViewCell: UITableViewCell {

    @IBOutlet weak var numberingLabel: UILabel!
    
    @IBOutlet weak var planStatusLbl: UILabel!
    @IBOutlet weak var planExpLbl: UILabel!
    @IBOutlet weak var leftBlnceLbl: UILabel!
    @IBOutlet weak var useBlnceLbl: UILabel!
    @IBOutlet weak var totalViewLbl: UILabel!
    @IBOutlet weak var jobTitleLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 4
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 3
        // Configure the view for the selected state
    }

}
