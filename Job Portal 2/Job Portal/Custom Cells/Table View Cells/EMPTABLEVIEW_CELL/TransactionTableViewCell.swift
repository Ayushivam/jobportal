
//
//  TransactionTableViewCell.swift
//  Job Portal
//
//  Created by nile on 11/12/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    @IBOutlet weak var paymentDateLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var  statusLbl: UILabel!

    @IBOutlet weak var serialNoLbl: UILabel!
    @IBOutlet weak var numberingLabel: UILabel!
    @IBOutlet weak var jobTittleLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
