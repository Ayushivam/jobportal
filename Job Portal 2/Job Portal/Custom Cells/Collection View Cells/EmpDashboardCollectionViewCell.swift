//
//  EmpDashboardCollectionViewCell.swift
//  Job Portal
//
//  Created by nile on 29/10/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class EmpDashboardCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var upddatedTitleLabel: UILabel!
    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var commonImageView: UIImageView!
    @IBOutlet weak var commonButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        commonButton.layer.cornerRadius = 2.0
        commonButton.backgroundColor = UIColor.white
        commonButton.layer.shadowColor = UIColor.lightGray.cgColor
        commonButton.layer.shadowOpacity = 1.5
        commonButton.layer.shadowOffset = CGSize.zero
        commonButton.layer.shadowRadius = 1.5
    }
}
