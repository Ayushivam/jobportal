//
//  PDFCollectionViewCell.swift
//  Job Portal
//
//  Created by nile on 30/11/18.
//  Copyright © 2018 nile. All rights reserved.
//

import UIKit

class PDFCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var pdfDesc: UILabel!
    @IBOutlet weak var pdfDate: UILabel!
    @IBOutlet weak var pdfName: UILabel!
    @IBOutlet weak var videoThumb: UIImageView!
    
    override func awakeFromNib() {
    
        
        backView.layer.cornerRadius = 2.0
        backView.backgroundColor = UIColor.white
        backView.layer.shadowColor = UIColor.lightGray.cgColor
        backView.layer.shadowOpacity = 1.5
        backView.layer.shadowOffset = CGSize.zero
        backView.layer.shadowRadius = 1.5
        
        
    }
}
